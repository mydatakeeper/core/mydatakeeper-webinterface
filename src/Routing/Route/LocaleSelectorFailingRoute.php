<?php

namespace App\Routing\Route;

use Cake\I18n\I18n;
use Cake\Routing\Route\Route;

class LocaleSelectorFailingRoute extends Route
{
    public function parse(string $url, string $method = ''): ?array
    {
        if (fnmatch($this->template, $url)) {
            $selector = $this->options['selector'];
            $selector->select($this->defaults['locale']);
        }
        return null;
    }
}
