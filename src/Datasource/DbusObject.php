<?php

namespace App\Datasource;

class DbusObject
{
    private const DBUS_TYPES = [
        'DbusArray',
        'DbusDict',
        'DbusVariant',
        'DbusSet',
        'DbusStruct',
        'DbusObjectPath',
    ];

    protected $_config;

    private static $_buses = [];

    private $_proxy;
    private $_properties;
    private $_introspectable;
    private $_introspect;

    public function __construct(array $config = [])
    {
        $this->_config = $config;
    }

    public function config()
    {
        return $this->_config;
    }

    public function configName()
    {
        return $this->config()['name'];
    }

    public function busType()
    {
        $type = \Dbus::BUS_SYSTEM;
        if (!empty($this->config()['busType'])) {
            switch($this->config()['busType']) {
                case 'session':
                    $type = \Dbus::BUS_SESSION;
                    break;
                case 'system':
                    $type = \Dbus::BUS_SYSTEM;
                    break;
                default:
                    throw new \Exception('Unknown Dbus type "'.$this->config()['busType'].'"');
            }
        }
        return $type;
    }

    public static function static_bus($type)
    {
        if (!isset(self::$_buses[$type])) {
            self::$_buses[$type] = new \DBus($type, true);
        }

        return self::$_buses[$type];
    }

    protected function bus()
    {
        return self::static_bus($this->busType());
    }

    public function busName()
    {
        if (empty($this->config()['busName']))
            throw new \Exception("Missing Dbus name");

        return $this->config()['busName'];
    }

    public function busPath()
    {
        if (!empty($this->config()['busPath']))
            $path = $this->config()['busPath'];
        else
            $path = '/'.str_replace(['.'], ['/'], $this->busName());

        return join('', array_map(function($c) {
            if ('A' <= $c && $c <= 'Z') return $c;
            if ('a' <= $c && $c <= 'z') return $c;
            if ('0' <= $c && $c <= '9') return $c;
            if ('/' === $c) return $c;
            return '_x'.ord($c).'_';
        }, str_split($path)));
    }

    public function busInterface()
    {
        if (!empty($this->config()['busInterface']))
            return $this->config()['busInterface'];

        return $this->busName();
    }

    protected function proxy()
    {
        if ($this->_proxy === null) {
            $this->_proxy = $this->bus()->createProxy(
                $this->busName(),
                $this->busPath(),
                $this->busInterface()
            );
        }

        return $this->_proxy;
    }

    protected function introspectable()
    {
        if ($this->_introspectable === null) {
            $this->_introspectable = $this->bus()->createProxy(
                $this->busName(),
                $this->busPath(),
                'org.freedesktop.DBus.Introspectable'
            );
        }

        return $this->_introspectable;
    }

    protected function properties()
    {
        if ($this->_properties === null) {
            $this->_properties = $this->bus()->createProxy(
                $this->busName(),
                $this->busPath(),
                'org.freedesktop.DBus.Properties'
            );
        }

        return $this->_properties;
    }

    public static function extractFromDbusType($value)
    {
        switch (gettype($value)) {
            case 'object':
                if (in_array(get_class($value), self::DBUS_TYPES))
                    return self::extractFromDbusType($value->getData());
            case 'array':
                return array_map('self::extractFromDbusType', $value);
        }

        return $value;
    }

    public static function encapsulateToDBusType($value, $sig)
    {
        assert(self::signatureLen($sig) == strlen($sig), new \DbusException('bad signature'));
        switch($sig[0]) {
            case 'y': return new \DbusByte($value);    // BYTE
            case 'b': return new \DbusBool($value);    // BOOLEAN
            case 'n': return new \DbusInt16($value);   // INT16
            case 'q': return new \DbusUInt16($value);  // UINT16
            case 'i': return new \DbusInt32($value);   // INT32
            case 'u': return new \DbusUInt32($value);  // UINT32
            case 'x': return new \DbusInt64($value);   // INT64
            case 't': return new \DbusUInt64($value);  // UINT64
            case 'd': return new \DbusDouble($value);  // DOUBLE
            case 's': return strval($value);           // STRING
            case 'v': return new \DbusVariant($value); // VARIANT
            case 'a':
                if ($sig[1] == '{') { // DICT
                    $klen = self::signatureLen($sig, 2);
                    $vlen = self::signatureLen($sig, 2 + $klen);
                    $ksig = substr($sig, 2, $klen);
                    $vsig = substr($sig, 2 + $klen, $vlen);
                    assert($ksig == 's', new \DbusException('bad signature'));
                    return new \DbusDict(
                        self::signatureType($sig, 2 + $klen),
                        array_combine(array_map(function ($k) use ($ksig) {
                            return self::encapsulateToDBusType($k, $ksig);
                        }, array_keys($value)), array_map(function ($v) use ($vsig) {
                            return self::encapsulateToDBusType($v, $vsig);
                        }, array_values($value))
                    ), '{' . $ksig . $vsig . '}');
                } else { // ARRAY
                    $len = self::signatureLen($sig, 1);
                    $sig = substr($sig, 1, $len);
                    return new \DbusArray(
                        self::signatureType($sig, 1),
                        array_map(function ($v) use ($sig) {
                            return self::encapsulateToDBusType($v, $sig);
                        }, $value
                    ), $sig);
                }
            case '(': // STRUCT
                $data = [];
                $idx = 1;
                reset($value);
                while ($sig[$idx] != ')') {
                    $len = self::signatureLen($sig, $idx);
                    $s = substr($sig, $idx, $len);
                    $data[] = self::encapsulateToDBusType(current($value), $s);
                    next($value);
                    $idx += $len;
                }
                return new \DbusStruct(substr($sig, 1, $idx - 1), $data);
        }
    }

    private static function signatureLen($sig, $start = 0)
    {
        assert($start < strlen($sig), new \DbusException('bad signature'));
        switch ($sig[$start]) {
            case 'y': // BYTE
            case 'b': // BOOLEAN
            case 'n': // INT16
            case 'q': // UINT16
            case 'i': // INT32
            case 'u': // UINT32
            case 'x': // INT64
            case 't': // UINT64
            case 'd': // DOUBLE
            case 's': // STRING
            case 'v': // VARIANT
                return 1;
            case 'a':
                return self::signatureLen($sig, $start + 1) + 1;
            case '(':
                $len = 1;
                while ($sig[$start + $len] != ')')
                    $len += self::signatureLen($sig, $start + $len);
                return $len+1;
            case '{':
                $klen = self::signatureLen($sig, $start + 1);
                $vlen = self::signatureLen($sig, $start + 1 + $klen);
                assert($sig[$start + 1 + $klen + $vlen] ==  '}', new \DbusException('bad signature'));
                return $start + 1 + $klen + $vlen + 1;
            default:
                throw new \DbusException('bad signature');
        }
    }

    private static function signatureType($sig, $start = 0)
    {
        assert($start < strlen($sig), new \DbusException('bad signature'));
        switch ($sig[$start]) {
            case 'y': return \Dbus::BYTE;
            case 'b': return \Dbus::BOOLEAN;
            case 'n': return \Dbus::INT16;
            case 'q': return \Dbus::UINT16;
            case 'i': return \Dbus::INT32;
            case 'u': return \Dbus::UINT32;
            case 'x': return \Dbus::INT64;
            case 't': return \Dbus::UINT64;
            case 'd': return \Dbus::DOUBLE;
            case 's': return \Dbus::STRING;
            case 'v': return \Dbus::VARIANT;
            case 'a':
                return $sig[$start + 1] == '{'
                    ? \Dbus::DICT
                    : \Dbus::ARRAY;
            case '(': return \DBus::STRUCT;
            default:
                throw new \DbusException('bad signature');
        }
    }

    private function introspect()
    {
        if ($this->_introspect === null) {
            $this->_introspect = simplexml_load_string(
                self::extractFromDbusType(
                    \call_user_func_array(
                        array($this->introspectable(), 'Introspect'), []
                    )
                )
            );
        }

        return $this->_introspect;
    }

    /**
     *  TODO: Add result caching
     */
    public function __call(string $name, array $args)
    {
        $iface = null;
        foreach ($this->introspect()->interface as $node) {
            if ($node->attributes()->name == $this->busInterface()) {
                $iface = $node;
                break;
            }
        }
        if (!$iface)
            throw new \Exception('bad interface');

        $method = null;
        foreach ($iface->method as $node) {
            if ($node->attributes()->name == $name) {
                $method = $node;
                break;
            }
        }
        if (!$method)
            throw new \Exception('bad method');

        $sigs = [];
        foreach ($method->arg as $type => $node) {
            if ($node->attributes()->direction == 'in') {
                $sigs[] = strval($node->attributes()->type);
            }
        }
        if (count($args) != count($sigs))
            throw new \Exception('bad argument number');

        return self::extractFromDbusType(
            \call_user_func_array(
                array($this->proxy(), $name),
                array_map('self::encapsulateToDBusType', $args, $sigs)
            )
        );
    }

    public function toArray()
    {
        return self::extractFromDbusType(
            \call_user_func_array(
                array($this->properties(), 'GetAll'), [$this->busInterface()]
            )
        );
    }

    public function __get(string $name)
    {
        return self::extractFromDbusType(
            \call_user_func_array(
                array($this->properties(), 'Get'), [$this->busInterface(), $name]
            )
        );
    }

    public function __set(string $name, $value)
    {
        if (!is_object($value) || get_class($value) != 'DbusVariant')
            $value = new \DbusVariant($value);

        self::extractFromDbusType(
            \call_user_func_array(
                array($this->properties(), 'Set'), [$this->busInterface(), $name, $value]
            )
        );
    }
}
