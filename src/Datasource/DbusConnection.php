<?php

namespace App\Datasource;

use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;

use Cake\Database\Connection;

class DbusConnection extends Connection
{
    protected $_config;

    private $_objects;

    public function __construct(array $config = [])
    {
        $this->_config = $config;
    }

    public function config(): array
    {
        return $this->_config;
    }

    public function configName(): string
    {
        return $this->config()['name'];
    }

    public function get(string $name, array $config = array())
    {
        if (isset($this->config()[$name])) {
            $config += $this->config()[$name];
        }

        $hash = md5($name . serialize($config));
        if (isset($this->_objects[$hash])) {
            return $this->_objects[$hash];
        }

        try {
            $this->_objects[$hash] = new DbusObject($config);
        } catch (\Exception $e) {
            if (isset($this->config()['cacheError']) && $this->config()['cacheError']) {
                $this->_objects[$hash] = $e;
            } else {
                return null;
            }
        }

        return $this->_objects[$hash];
    }

    public function transactional(callable $transaction)
    {
        return $transaction();
    }

    public function setLogger(LoggerInterface $logger)
    {
        throw new \Exception("Not implemented");
    }

    public function getLogger(): LoggerInterface
    {
        throw new \Exception("Not implemented");
    }

    public function setCacher(CacheInterface $cacher)
    {
        throw new \Exception("Not implemented");
    }

    public function getCacher(): CacheInterface
    {
        throw new \Exception("Not implemented");
    }


    public function disableConstraints(callable $operation)
    {
        throw new \Exception("Not implemented");
    }

    public function enableQueryLogging(bool $value = true)
    {
        throw new \Exception("Not implemented");
    }

    public function disableQueryLogging()
    {
        throw new \Exception("Not implemented");
    }

    public function isQueryLoggingEnabled(): bool
    {
        throw new \Exception("Not implemented");
    }

}
