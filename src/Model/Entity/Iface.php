<?php
namespace App\Model\Entity;

use Cake\ORM\TableRegistry;

use App\Datasource\DBusObject;

class Iface extends DbusEntity
{
    protected $_hidden = ['id'];

    protected $_application;

    protected $_bus;

    public function __construct(array $properties = [], array $options = [])
    {
        if (isset($properties['_application'])) {
            $this->_application = $properties['_application'];
            unset($properties['_application']);
        }

        parent::__construct($properties, $options);
    }

    public function application()
    {
        if (!$this->_application) {
            $this->_application = TableRegistry::get('Applications')->newEntity([
                'id' => $this->id,
                'hasInterface' => true,
                '_interface' => $this,
            ], [
                'individual' => true,
            ]);
        }
        return $this->_application;
    }

    protected function bus() {
        if (!$this->_bus) {
            $this->_bus = new DbusObject([
                'busName' => $this->id,
            ]);
        }
        return $this->_bus;
    }

    public function call_method(string $name, array $arguments)
    {
        return call_user_func_array(array($this->bus(), $name), $arguments);
    }
}
