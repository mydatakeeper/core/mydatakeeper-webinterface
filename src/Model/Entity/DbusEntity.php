<?php
namespace App\Model\Entity;

use Cake\Cache\Cache;
use Cake\ORM\Entity;

class DbusEntity extends Entity
{
    private $_connection;
    private $_cache;
    private $_individual = false;
    private $_fetched = false;

    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
        if (isset($options['connection'])) {
            $this->_connection = $options['connection'];
        }
        if (isset($options['cache'])) {
            $this->_cache = $options['cache'];
        }
        if (isset($options['individual'])) {
            $this->_individual = !!$options['individual'];
        }
        if (isset($options['fetch']) && $options['fetch']) {
            $this->fetch();
        }
    }

    protected function fetch(string $field = null)
    {
        if (!$this->_fetched) {
            if ($field) {
                $this->set([$field => $this->_connection->{$field}], [
                    'setter' => false,
                    'guard' => false,
                ]);
            } else {
                $this->set($this->_connection->toArray(), [
                    'setter' => false,
                    'guard' => false,
                ]);
                $this->_fetched = true;
            }
        }
    }

    public function &get(string $field)
    {
        if ($field[0] !== '_' && parent::get($field) === null)
            $this->fetch($this->_individual ? $field : null);

        return parent::get($field);
    }

    public function has($field): bool
    {
        if ($field[0] !== '_' && !parent::has($field))
            $this->fetch($this->_individual ? $field : null);

        return parent::has($field);
    }

    public function __call($name, $arguments)
    {
        if ($this->_connection)
            return call_user_func_array(array($this->_connection, $name), $arguments);
    }


    public function cache()
    {
        if ($this->_cache && $this->id) {
            $cache = Cache::read($this->_cache, '_dbus_') ?: [];
            Cache::write($this->_cache, [$this->id => $this->toArray()] + $cache, '_dbus_');
        }
    }
}