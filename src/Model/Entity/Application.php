<?php
namespace App\Model\Entity;

use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

class Application extends DbusEntity
{
    private const BBCODE_TAGS = [
        '' => [
            'type' => BBCODE_TYPE_ROOT,
            'childs' => '!i',
        ],
        'abbr' => [
            'type' => BBCODE_TYPE_ARG,
            'open_tag' => '<abbr title="{PARAM}">',
            'close_tag' => '</abbr>',
        ],
        'br' => [
            'type' => BBCODE_TYPE_SINGLE,
            'open_tag' => '<br>',
        ],
        'b' => [
            'type' => BBCODE_TYPE_NOARG,
            'open_tag' => '<b>',
            'close_tag' => '</b>',
        ],
        'i' => [
            'type' => BBCODE_TYPE_NOARG,
            'open_tag' => '<i>',
            'close_tag' => '</i>',
            'childs' => 'b',
        ],
        'url' => [
            'type' => BBCODE_TYPE_OPTARG,
            'open_tag' => '<a href="{PARAM}">',
            'close_tag' => '</a>',
            'default_arg' => '{CONTENT}',
            'childs' => 'b,i',
        ],
        'img' => [
            'type' => BBCODE_TYPE_NOARG,
            'open_tag' => '<img src="',
            'close_tag' => '" />',
            'childs' => '',
        ],
    ];

    protected $_hidden = ['id'];

    protected $_interface;
    protected $_configuration;

    public function __construct(array $properties = [], array $options = [])
    {
        if (isset($properties['_interface'])) {
            $this->_interface = $properties['_interface'];
            unset($properties['_interface']);
        }

        if (isset($properties['_configuration'])) {
            $this->_configuration = $properties['_configuration'];
            unset($properties['_configuration']);
        }

        parent::__construct($properties, $options);
    }

    public function interface()
    {
        if (!$this->hasInterface)
            return false;

        if ($this->_interface === null) {
            $this->_interface = TableRegistry::get('Ifaces')->newEntity([
                'id' => $id,
                '_application' => $this,
            ]);
        }

        return $this->_interface;
    }

    public function interfaceLink()
    {
        return isset($this->_interfaceLink) ?
            $this->_interfaceLink :
            Router::url([
                'action' => 'view',
                'id' => $this->id,
            ]);
    }

    public function config()
    {
        if (!$this->hasConfig)
            return false;

        if ($this->_configuration === null) {
            $this->_configuration = TableRegistry::get('Configurations')->newEntity([
                'id' => $this->id,
                '_application' => $this,
            ]);
        }

        return $this->_configuration;
    }

    public function name()
    {
        return h($this->name);
    }

    public function description()
    {
        return $this->bbcode(h($this->description));
    }

    public function icon(string $size = '*')
    {
        if (!isset($this->icons))
            return false;

        if (isset($this->icons[$size]))
            return $this->icons[$size];
        if (isset($this->icons['*']))
            return $this->icons['*'];
        return false;
    }

    public function iconData(string $size = '*')
    {
        if (isset($this->_iconLink))
            return $this->_iconLink;

        return $this->GetIcon($size);
    }

    protected static function bbcode(string $str)
    {
        return bbcode_parse(\bbcode_create(self::BBCODE_TAGS), $str);
    }
}