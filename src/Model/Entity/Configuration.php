<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use DateTime;
use DateTimeZone;

class Configuration extends DbusEntity
{
    private const DATE_OPTIONS = [
        "monthNames" => false,
        "month" => [
            "leadingZeroValue" => true,
        ],
        "day" => [
            "leadingZeroValue" => true,
        ],
    ];
    private const TIME_OPTIONS = [
        "hour" => [
            "leadingZeroValue" => true,
        ],
        "second" => true,
    ];


    protected $_hidden = ['id'];

    protected $_application;

    protected $_validator;

    public function __construct(array $properties = [], array $options = [])
    {
        if (isset($properties['_application'])) {
            $this->_application = $properties['_application'];
            unset($properties['_application']);
        }

        parent::__construct($properties, $options);
    }

    protected function checkDependencies()
    {
        // Check dependencies
        foreach ($this->visibleProperties() as $field) {
            if (!$this->dependenciesSatisfied($field)) {
                $this->_options[$field]['templateVars']['dependencies'] = ' missing-dependency';
            }
        }
    }

    public function &get($property)
    {
        $value = parent::get($property);
        if ($property[0] === '_' || in_array($property, $this->_hidden))
            return $value;

        switch($this->type($property))
        {
            case 'timestamp':
            case 'datetime':
            case 'date':
            case 'time':
                // Convert timestamp from UTC timezone to locale timezone
                if (is_int($value)) {
                    $value = new DateTime('@' . $value, new DateTimeZone('UTC'));
                    $value->setTimezone(new DateTimeZone(date_default_timezone_get()));
                }
                break;
            default:
                break;
        }
        return $value;
    }

    public function set($property, $value = null, array $options = [])
    {
        if (is_string($property) && $property !== '') {
            $guard = false;
            $property = [$property => $value];
        } else {
            $guard = true;
            $options = (array)$value;
        }

        if (!is_array($property)) {
            throw new InvalidArgumentException('Cannot set an empty property');
        }
        $options += ['setter' => true, 'guard' => $guard];

        if ($options['setter'] == false)
            return parent::set($property, $options);

        foreach ($property as $key => $value) {
            if (in_array($key, $this->_hidden))
                continue;

            $type = $this->type($key);
            // Hash password type value
            if (strncmp($type, 'password', 8) === 0 && strlen($value) > 0) {
                $property[$key] = (new DefaultPasswordHasher)->hash($value);
            }
        }
        return parent::set($property, $options);
    }

    public function application()
    {
        if (!$this->_application) {
            $this->_application = TableRegistry::get('Applications')->newEntity([
                'id' => $this->id,
                'hasConfig' => true,
                '_configuration' => $this,
            ], [
                'individual' => true,
            ]);
        }

        return $this->_application;
    }

    public function options(string $field)
    {
        $property = $this->application()->fields[$field] + [
            'description' => null,
            'placeholder' => null,
            'pattern' => null,
            'maxlength' => null,
            'minlength' => null,
            'advanced' => false,
            'readonly' => false,
        ];
        $options = [
            // 'type' => $this->type($field),
            'label' => $property['label'],
            'nestedInput' => false,
            'disabled' => !!$property['readonly'],
            'templateVars' => [
                'description' => $property['description'],
                'advanced' => $property['advanced'] ? ' advanced' : '',
            ],
            'class' => 'form-control',
        ];

        switch ($this->type($field)) {
            case 'boolean':
                break;
            case 'text':
                $options['placeholder'] = $property['placeholder'];
                $options['maxlength'] = $property['maxlength'];
                $options['minlength'] = $property['minlength'];
                break;
            case 'password':
            case 'password-utf-8':
            case 'password-utf8':
            case 'password-utf-8Extended':
            case 'password-utf8Extended':
            case 'password-ascii':
                $options['value'] = '';
                $options['placeholder'] = $property['placeholder'];
                $options['maxlength'] = $property['maxlength'];
                $options['minlength'] = $property['minlength'];
                break;
            case 'string':
            case 'ascii':
            case 'utf-8':
            case 'utf8':
            case 'utf-8Extended':
            case 'utf8Extended':
                $options['pattern'] = $property['pattern'];
                $options['placeholder'] = $property['placeholder'];
                $options['maxlength'] = $property['maxlength'];
                $options['minlength'] = $property['minlength'];
                break;
            case 'ip':
            case 'ipv4':
            case 'ipv6':
                $options['placeholder'] = $property['placeholder'];
                break;
            case 'datetime':
            case 'timestamp':
                $options += self::DATE_OPTIONS + self::TIME_OPTIONS;
                break;
            case 'date':
                $options += self::DATE_OPTIONS;
                break;
            case 'time':
                $options += self::TIME_OPTIONS;
                break;
            case 'selectMultiple':
                $options['multiple'] = true;
            case 'select':
                $options['options'] = $property['options'];
                $options['empty'] = __("Please select a value");
                break;
            case 'dict':
            case 'metadata':
                throw new Exception("This data type should not be displayed");
            default:
                break;
        }

        return $options;
    }

    public function type(string $field)
    {
        return $this->application()->fields[$field]['type'];
    }

    public function dependencies(string $field)
    {
        $property = $this->application()->fields[$field];
        if (!isset($property['dependencies'])) {
            return [];
        }
        if (is_string($property['dependencies'])) {
            return [$property['dependencies'] => true];
        }
        if (!is_array($property['dependencies'])) {
            return [];
        }
        return $property['dependencies'];
    }

    public function getVisible(): array
    {
        return array_keys(
            array_filter($this->application()->fields, function ($property) {
                return !in_array($property['type'], ['dict', 'metadata']);
            })
        );
    }

    public function writeables() {
        return array_keys(
            array_filter($this->application()->fields, function ($property) {
                return
                    !in_array($property['type'], ['dict', 'metadata']) &&
                    (!isset($property['readonly']) || !$property['readonly']);
            })
        );
    }

    public function dependenciesSatisfied(string $field)
    {
        foreach ($this->dependencies($field) as $id => $value) {
            if (is_array($value)) {
                if (!in_array($this->{$id}, $value))
                    return false;
            } else if ($this->{$id} !== $value) {
                return false;
            }
        }

        return true;
    }

    public function getValidator()
    {
        if ($this->_validator === null)
            $this->_validator = $this->_createValidator();
        return $this->_validator;
    }

    private static function array_recursive_key_exists($key, array $array) {
        foreach ($array as $k => $v) {
            if (is_array($v) && Config::array_recursive_key_exists($key, $v)) {
                return true;
            } else if ($key == $k) {
                return true;
            }
        }
        return false;
    }

    public function _createValidator()
    {
        $validator = new Validator();
        $validator->requirePresence('id', __("Internal error"));
        foreach($this->writeables() as $id) {
            $validator->allowEmptyFor($id);
            $options = $this->options($id);
            $label = isset($options['label']) ? $options['label'] : $id;

            if (isset($options['readonly']) && $options['readonly']) {
                continue;
            }

            if (!empty($options['maxlength'])) {
                $validator->maxLength($id, $options['maxlength'], __(
                    "Value of ''{0}' is too long",
                    $label
                ));
            }
            if (!empty($options['minlength'])) {
                $validator->minLength($id, $options['minlength'], __(
                    "Value of ''{0}' is too short",
                    $label
                ));
            }
            if (!empty($options['pattern'])) {
                $validator->regex($id, '/^'.$options['pattern'].'$/sm', __(
                    "Value of ''{0}' is invalid",
                    $label
                ));
            }
            switch($this->type($id)) {
                case 'text':
                    $validator->allowEmptyString($id);
                case 'password':
                case 'string':
                    $validator->utf8($id, __(
                        "Value of ''{0}' is not a valid string",
                        $label
                    ));
                    break;
                case 'password-utf-8':
                case 'password-utf8':
                case 'utf-8':
                case 'utf8':
                    $validator->utf8($id, __(
                        "Value of ''{0}' is not a valid UTF-8 string",
                        $label
                    ));
                    break;
                case 'password-utf-8Extended':
                case 'password-utf8Extended':
                case 'utf-8Extended':
                case 'utf8Extended':
                    $validator->utf8Extended($id, __(
                        "Value of ''{0}' is not a valid extended UTF-8 string",
                        $label
                    ));
                    break;
                case 'ascii':
                case 'password-ascii':
                    $validator->ascii($id, __(
                        "Value of ''{0}' is not a valid ASCII string",
                        $label
                    ));
                    break;
                case 'ip':
                    $validator->ip($id, __(
                        "Value of ''{0}' is not a valid IP address",
                        $label
                    ));
                    break;
                case 'ipv4':
                    $validator->ipv4($id, __(
                        "Value of ''{0}' is not a valid IPv4 address",
                        $label
                    ));
                    break;
                case 'ipv6':
                    $validator->ipv6($id, __(
                        "Value of ''{0}' is not a valid IPv6 address",
                        $label
                    ));
                    break;
                case 'select':
                    $validator->add($id, 'custom', [
                        'rule' => function ($value, $context) use ($options) {
                            return Config::array_recursive_key_exists($value, $options['options']);
                        },
                        'message' => __(
                            "Value of ''{0}' is not one of the options",
                            $label
                        )
                    ]);
                    break;
                case 'selectMultiple':
                    $validator->isArray($id);
                    $validator->add($id, 'custom', [
                        'rule' => function (array $values, $context) use ($options) {
                            foreach ($values as $value) {
                                if (!Config::array_recursive_key_exists($value, $options['options']))
                                    return false;
                            }
                            return true;
                        },
                        'message' => __(
                            "At least one value of ''{0}' is not one of the options",
                            $label
                        )
                    ]);
                    break;
                case 'dict':
                    user_error("Dict field should always be readonly." . PHP_EOL, E_USER_WARNING);
                    break;
                case 'boolean':
                    $validator->boolean($id, __(
                        "Value of ''{0}' is not valid",
                        $label
                    ));
                    break;
                default:
                    $fullId = $this->id.'.'.$id;
                    $type = $this->type($id);
                    user_error("Unknown field '$fullId' type '$type'." . PHP_EOL, E_USER_WARNING);
                    break;
            }
        }

        return $validator;
    }
}