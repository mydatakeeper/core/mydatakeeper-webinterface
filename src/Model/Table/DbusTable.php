<?php
namespace App\Model\Table;

use Cake\Cache\Cache;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Table;

use App\Model\Entity\DbusEntity;

class DbusTable extends Table
{
    protected function getPath(string $id): string {
        throw new \Exception("Unknown path");
    }

    private $_cache;
    protected function getCache(string $name) {
        if ($this->_cache === null) {
            $this->_cache = Cache::read($name, '_dbus_') ?: [];
        }
        return $this->_cache;
    }

    protected function getData($data): array
    {
        if (is_string($data)) $data = ['id' => $data];
        return $data;
    }

    protected function getList(): array
    {
        $plural = strtolower(substr(static::class, strrpos(static::class, '\\') + 1, -5));
        $cache = $this->getCache($plural);
        if ($cache) return $cache;

        return $this->getConnection()->get($plural)->List();
    }

    public function newEntity(array $data, array $options = []): EntityInterface
    {
        if (isset($data['id'])) {
            $id = $data['id'];
            $options += [
                'cache' => strtolower(substr(static::class, strrpos(static::class, '\\') + 1, -5)),
            ];
            $cache = $this->getCache($options['cache']);
            if ($cache && isset($cache[$id])) {
                $data += $cache[$id];
            }
        }

        $options += [
            'name' => strtolower(substr(static::class, strrpos(static::class, '\\') + 1, -6)),
        ];
        $config = $this->getConnection()->config()[$options['name']];
        $busPath =
            isset($options['busPath']) ? $options['busPath'] : (
                isset($config['busPath']) ? $config['busPath'] : (
                    isset($data['id']) ? $this->getPath($id) : null));

        if ($busPath !== null) {
            $options += [
                'useSetters' => false,
                'validate' => false,
                'connection' => $this->getConnection()->get(
                    $options['name'], compact('busPath')
                ),
            ];
        }

        $class = static::class === DbusTable::class ? DbusEntity::class : $this->getEntityClass();
        return new $class($data, $options);
    }

    public function getById(string $id, array $options = [])
    {
        return $this->newEntity($this->getData($id), $options);
    }

    public function getAll(array $options = [])
    {
        return array_map(function ($data) use ($options) {
            return $this->newEntity($this->getData($data), $options);
        }, $this->getList());
    }
}
