<?php
namespace App\Model\Table;

use Cake\Datasource\EntityInterface;

class ConfigurationsTable extends DbusTable
{
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->setPrimaryKey('id');
    }

    protected function getPath(string $id): string
    {
        return '/'.str_replace(['.'], ['/'], $id);
    }

    public function save(EntityInterface $entity, $options = [])
    {
        if ($entity->hasErrors())
            return false;

        $config = $this->getConnection()->get('configuration', [
            'busPath' => $this->getPath($entity->id),
        ]);
        $data = $config->toArray();
        foreach ($entity->writeables() as $field) {
            $new = $entity->{$field};
            $old = $data[$field];
            if ($new == $old)
                continue;
            switch($entity->type($field)) {
                case 'selectMultiple':
                    $config->{$field} = new \DbusVariant($new, 'as');
                    break;
                case 'boolean':
                    $config->{$field} = !!$new;
                    break;
                case 'password':
                case 'password-utf-8':
                case 'password-utf8':
                case 'password-utf-8Extended':
                case 'password-utf8Extended':
                case 'password-ascii':
                    if (empty($new))
                        break;
                default:
                    $config->{$field} = $new;
                    break;
            }
        }

        return true;
    }
}
