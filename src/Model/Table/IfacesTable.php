<?php
namespace App\Model\Table;

class IfacesTable extends DbusTable
{
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->setPrimaryKey('id');
    }

    protected function getPath(string $id): string
    {
        return '/'.str_replace(['.'], ['/'], $id);
    }
}
