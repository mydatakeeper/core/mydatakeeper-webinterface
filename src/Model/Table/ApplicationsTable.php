<?php
namespace App\Model\Table;

use Cake\Validation\Validator;

class ApplicationsTable extends DbusTable
{
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->setPrimaryKey('id');
    }

    public function validationDefault(Validator $validator): Validator
    {
        return $validator
            ->requirePresence('version')
            ->regex('version', '/^((0|[1-9][0-9]*)?[a-z]*(0|[1-9][0-9]*)?[a-z]*)(\.(0|[1-9][0-9]*)?[a-z]*(0|[1-9][0-9]*)?[a-z]*)*$/')
            ->requirePresence('name')
            ->utf8('name')
            ->email('contact')
            ->url('website')
            ->utf8('description')
            ->isArray('tags')
            ->add('tags', 'custom', [
                'rule' => function(array $tags, $context) {
                    foreach ($tags as $tag) {
                        if (!preg_match('/^[0-9a-z-_.]+$/', $tag))
                            return false;
                    }
                    return true;
                },
            ])
            ->isArray('categories')
            ->add('categories', 'custom', [
                'rule' => function(array $categories, $context) {
                    foreach ($categories as $category) {
                        if (!preg_match('/^[a-z]+$/', $category))
                            return false;
                    }
                    return true;
                },
            ])
            ->isArray('icons')
            ->add('icons', 'custom', [
                'rule' => function(array $filenames, $context) {
                    $installDirectory = $context['data']['installDirectory'];
                    foreach ($filenames as $size => $filename) {
                        if (!in_array($size, self::ICON_SIZES)) {
                            user_error("Icon '$filename' size '$size' is not supported." . PHP_EOL, E_USER_WARNING);
                            return false;
                        }

                        $path = realpath($installDirectory . DS . $filename);
                        if ($path === false || strncmp($path, $installDirectory, strlen($installDirectory)) !== 0) {
                            user_error("Icon '$filename' is not in the install directory." . PHP_EOL, E_USER_WARNING);
                            return false;
                        }
                        if (!is_readable($path)) {
                            user_error("Icon '$filename' is not a readable file." . PHP_EOL, E_USER_WARNING);
                            return false;
                        }
                        $type = pathinfo($path, PATHINFO_EXTENSION);
                        if (!in_array($type, self::ICON_TYPES)) {
                            user_error("Icon '$filename' type '$type' is not supported." . PHP_EOL, E_USER_WARNING);
                            return false;
                        }
                    }
                    return true;
                },
            ]);
    }

    protected function getPath(string $id): string
    {
        return '/'.str_replace(['.'], ['/'], $id);
    }

    protected function getList(): array
    {
        $cache = $this->getCache('applications');
        if ($cache) {
            $list = [];
            foreach ($cache as $id => $data) {
                $list[] = ['id' => $id] + $data;
            }
            return $list;
        }

        $list = [];
        foreach ($this->getConnection()->get('applications')->List() as $id => $data) {
            $list[] = [
                'id' => $id,
                'hasConfig' => $data[0],
                'hasInterface' => $data[1],
            ];
        }
        return $list;
    }
}
