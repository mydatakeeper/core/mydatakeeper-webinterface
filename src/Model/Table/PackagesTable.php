<?php
namespace App\Model\Table;

class PackagesTable extends DbusTable
{
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->setPrimaryKey('id');
    }

    protected function getPath(string $id): string
    {
        return '/fr/mydatakeeper/Package/' . $id;
    }

    protected function getData($data): array
    {
        $data = parent::getData($data);
        if (!isset($data['Name'])) $data['Name'] = $data['id'];
        return $data;
    }

    protected function getList(): array
    {
        $cache = $this->getCache('packages');
        if ($cache) return $cache;

        $list= [];
        foreach ($this->getConnection()->get('packages')->List as $id => $data) {
            $list[] = [
                'id' => $id,
                'Name' => $id,
                'Version' => $data[0],
                'Licenses' => $data[1],
            ];
        }
        return $list;
    }
}
