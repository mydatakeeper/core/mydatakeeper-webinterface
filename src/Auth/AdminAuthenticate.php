<?php
namespace App\Auth;

use Cake\Auth\BaseAuthenticate;
use Cake\Http\ServerRequest;
use Cake\Http\Response;

use App\Datasource\DbusConnection as Dbus;

class AdminAuthenticate extends BaseAuthenticate
{
    public function authenticate(ServerRequest $request, Response $response)
    {
        $table = $this->getTableLocator()->get($this->_config['userModel']);
        $systemConfig = $table->newEntity(['id' => 'fr.mydatakeeper.system']);
        $password = $request->getData('password');
        $hash = $systemConfig->hash;
        return $this->passwordHasher()->check($password, $hash);
    }
}