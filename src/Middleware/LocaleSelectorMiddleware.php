<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         3.3.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Middleware;

use Cake\Http\Exception\BadRequestException;
use Cake\I18n\I18n;
use Cake\I18n\Middleware\LocaleSelectorMiddleware as OldLocaleSelectorMiddleware;
use Locale;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Sets the runtime default locale for the request based on a
 * 'locale' route parameter and the Accept-Language header.
 * The default will only be set if it matches the list of
 * passed valid locales.
 */
class LocaleSelectorMiddleware extends OldLocaleSelectorMiddleware
{
    protected static $_matched;

    public function select(string $locale)
    {
        if (static::$_matched)
            return;

        if ($this->locales !== ['*']) {
            $locale = Locale::lookup($this->locales, $locale, true);
        }
        if ($locale || $this->locales === ['*']) {
            I18n::setLocale($locale);
        } else {
            throw new BadRequestException(__("Requested locale is not supported yet"));
        }
        static::$_matched = true;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $locale = $request->getParam('locale');
        if (!$locale) {
            return parent::__invoke($request, $response, $next);
        }

        $this->select($locale);

        return $handler->handle($request);
    }
}
