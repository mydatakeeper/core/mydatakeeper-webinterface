<?php

namespace App\View\Form;

use Cake\Validation\Validator;
use Cake\View\Form\EntityContext;

class DbusEntityContext extends EntityContext
{
    public function fieldNames(): array
    {
        return $this->entity()->visibleProperties();
    }

    public function isPrimaryKey(string $field): bool
    {
        return $field === 'id';
    }

    public function getRequiredMessage(string $field): ?string
    {
        return null;
    }

    public function type(string $field): ?string
    {
        return $this->entity()->type($field);
    }

    public function attributes(string $field): array
    {
        return $this->entity()->options($field);
    }

    protected function _getValidator(array $parts): Validator
    {
        return $this->entity()->getValidator();
    }
}
