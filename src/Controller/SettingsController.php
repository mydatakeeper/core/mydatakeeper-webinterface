<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\EventInterface;
use Cake\I18n\I18n;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class SettingsController extends AppController
{
    /**
     * Allow unauthenticated user to access some pages
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Auth->deny();
    }

    public function index()
    {
        if ($this->request->is('post')) {
            $this->loadModel('Configurations');

            $config = $this->Configurations->newEntity($this->request->getData());
            $errors = $config->getErrors();
            if (!empty($errors)) {
                foreach ($errors as $key => $error) {
                    foreach ($error as $rule => $message) {
                        if ($key === 'id')
                            $this->Flash->set($message, ['element' => 'error']);
                        else
                            $this->Flash->set($message, ['key' => $config->id, 'element' => 'error']);
                    }
                }
            } else if (!$this->Configurations->save($config)) {
                $this->Flash->set(__("Internal error"), ['key' => $config->id, 'element' => 'error']);
            }
            return $this->redirect([]);
        }

        $this->loadModel('Applications');

        $applications = [];
        foreach ($this->Applications->getAll() as $application) {
            $errors = $application->getErrors();
            if ($errors) {
                $this->Flash()->set($errors, ['element' => 'error']);
            } else {
                $applications[] = $application;
            }
        }

        $this->set(compact('applications'));
    }
}
