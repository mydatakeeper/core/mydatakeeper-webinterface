<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Http\Exception\NotFoundException;
use Cake\I18n\I18n;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginAction' => [
                'controller' => 'App',
                'action' => 'login',
                'locale' => I18n::getLocale(),
            ],
            'authenticate' => ['Admin' => ['userModel' => 'Configurations']],
            'authError' => __("Please connect in order to reach this page"),
            'unauthorizedRedirect' => false,
            'loginRedirect' => [
                'controller' => 'App',
                'action' => 'index',
                'locale' => I18n::getLocale(),
            ],
            'logoutRedirect' => [
                'controller' => 'App',
                'action' => 'index',
                'locale' => I18n::getLocale(),
            ]
        ]);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');

        $allowed_origins = [
            "http://mydatakeeper.local",
            "http://local.mydatakeeper.fr",
            "http://home.mydatakeeper.fr",
            "https://local.mydatakeeper.fr",
            "https://home.mydatakeeper.fr",
        ];
        if (Configure::read('debug')) {
            $allowed_origins += [
                "http://local.debug.mydatakeeper.fr",
                "https://local.debug.mydatakeeper.fr",
            ];
        }

        $this->response = $this->response->cors($this->request)
            ->allowOrigin($allowed_origins)
            ->allowMethods(['GET', 'POST'])
            ->allowHeaders(['X-CSRF-Token'])
            ->allowCredentials()
            ->exposeHeaders(['Link'])
            ->maxAge(300)
            ->build();
    }

    /**
     * Allow unauthenticated user to access some pages
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'activate']);
    }

    protected function activated($config = null)
    {
        if (!$config) {
            $this->loadModel('Configurations');
            $config = $this->Configurations->getById('fr.mydatakeeper.account');
        }
        return $config->status === 'enabled';
    }

    public function index()
    {
        if (!$this->activated()) {
            return $this->redirect([
                'action' => 'activate',
                'redirect' => $this->request->getQuery('redirect', null),
            ]);
        } else if (!$this->Auth->user()) {
            return $this->redirect([
                'action' => 'login',
                'redirect' => $this->request->getQuery('redirect', null),
            ]);
        } else {
            return $this->redirect([
                'controller' => 'Application',
                'action' => 'index',
            ]);
        }
    }

    public function activate()
    {
        $this->loadModel('Configurations');
        $config = $this->Configurations->getById('fr.mydatakeeper.account');
        if ($this->activated($config)) {
            return $this->redirect([
                'action' => 'login',
                '?' => [
                    'redirect' => $this->request->getQuery('redirect', null),
                ],
            ]);
        }


        if ($this->request->is('post')) {
            $config = $this->Configurations->patchEntity($config, $this->request->getData());
            $errors = $config->getErrors();
            if (!empty($errors)) {
                foreach ($errors as $key => $error) {
                    foreach ($error as $rule => $message) {
                        $this->Flash->set($message, ['element' => 'error']);
                    }
                }
            } else if (!$this->Configurations->save($config)) {
                $this->Flash->set(__("Internal error"), ['element' => 'error']);
            }


            return $this->redirect([
                '?' => [
                    'waiting_status' => $config['associate'] ?
                        $config['enable'] ?
                            'enabled' :
                            'associated' :
                        'registered',
                    'timeout' => 0,
                    'redirect' => $this->request->getQuery('redirect', null),
                ],
            ]);
        }

        // Check if activation timed out
        $timeout = $this->request->getQuery('timeout', 0);
        if ($timeout > 5) {
            $this->Flash->set(
                __("Unable to activate your box at the moment. ".
                   "Please try again in a few minutes or ".
                   "contact our support."
                ),
                ['element' => 'error']
            );
            return $this->redirect([
                '?' => [
                    'waiting_status' => null,
                    'timeout' => null,
                    'redirect' => $this->request->getQuery('redirect', null),
                ],
            ]);
        }

        $this->set([
            'application' => $config->application(),
            'waiting_status' => $this->request->getQuery('waiting_status', false),
            'timeout' => $timeout + 1,
            'advanced' => $this->request->getQuery('advanced', false),
            'redirect' => $this->request->getQuery('redirect', null),
        ]);
    }

    public function login()
    {
        if (!$this->activated()) {
            return $this->redirect([
                'action' => 'activate',
                '?' => [
                    'redirect' => $this->request->getQuery('redirect', null),
                ],
            ]);
        }

        if ($this->Auth->user()) {
            return $this->redirect($this->Auth->redirectUrl());
        }

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->error(__("Wrong password"));
            }
        }
    }
}
