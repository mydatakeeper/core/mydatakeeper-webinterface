<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\EventInterface;
use Cake\Http\Exception\NotFoundException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class LicenseController extends AppController
{
    /**
     * Allow unauthenticated user to access some pages
     */
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow();
    }

    public function index()
    {
        $this->loadModel('Packages');

        $packages = $this->Packages->getAll();
        $this->set(compact('packages'));
    }

    public function package(string $package)
    {
        $this->loadModel('Packages');

        $package = $this->Packages->getById($package);
        if (!$package)
            throw new NotFoundException();

        $this->set(compact('package'));
    }

    public function license(string $package, string $license)
    {
        $this->loadModel('Packages');

        $package = $this->Packages->getById($package);

        try {
            return $this->response->withStringBody(
                $package->License($license),
                [ 'download' => false]
            )->withType('text');
        } catch (\DbusException $e) {
            throw new NotFoundException();
        }
    }

    public function custom(string $package, string $file)
    {
        $this->loadModel('Packages');

        $package = $this->Packages->getById($package);

        try {
            return $this->response->withStringBody(
                $package->CustomFile($file),
                [ 'download' => false]
            )->withType('text');
        } catch (\DbusException $e) {
            throw new NotFoundException();
        }
    }
}
