<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class CertificateController extends AppController
{
    public $mimeTypes = [
        'cer' => 'application/pkix-cert',
        'crl' => ['application/pkix-crl', 'application/x-pkcs7-crl'],
        'crt' => ['application/x-x509-user-cert', 'application/x-x509-ca-cert'],
        'csr' => 'application/pkcs10',
        'der' => 'application/x-x509-ca-cert',
        'key' => 'application/pkcs8',
        'p10' => 'application/pkcs10',
        'p12' => 'application/x-pkcs12',
        'p7b' => 'application/x-pkcs7-certificates',
        'p7c' => 'application/pkcs7-mime',
        'p7r' => 'application/x-pkcs7-certreqresp',
        'p8' => 'application/pkcs8',
        'pem' => 'application/x-pem-file',
        'pfx' => 'application/x-pkcs12',
        'spc' => 'application/x-pkcs7-certificates',
    ];

    public function initialize(): void
    {
        parent::initialize();

        // Add certificate mime types
        foreach ($this->mimeTypes as $type => $mimeType) {
            $this->response->setTypeMap($type, $mimeType);
        }
    }

    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow();
    }

    protected function getCertificates(string $basename = 'ca')
    {
        $certs = array();
        $dir = scandir(CERTS);
        foreach ($dir as $filename) {
            if ($filename === '.' || $filename === '..')
                continue;
            $path = CERTS . $filename;
            if (!is_file($path) || !is_readable($path))
                continue;

            $info = pathinfo($path);
            if ($info['filename'] !== $basename)
                continue;
            if (!isset($info['extension']) || !array_key_exists($info['extension'], $this->mimeTypes))
                continue;
            $certs[$info['extension']] = $path;
        }
        return $certs;
    }

    protected function getPlatforms()
    {
        return $platforms = [
            "android" => ["Android", 'pem'],
            "linux"   => ["Linux", 'pem'],

            "ios"     => ["iOS", 'pem'],
            "ipados"  => ["iPadOS", 'pem'],
            "mac"     => ["Macintosh", 'pem'], // alias
            "macosx"  => ["MacOSX", 'pem'],

            "windows"     => ["Windows", 'p12'], // alias
            "win10"       => ["Windows 10", 'p12'],
            "win32"       => ["Windows 32bit", 'p12'],
            "win64"       => ["Windows 64bit", 'p12'],
            "win7"        => ["Windows 7", 'p12'],
            "win8"        => ["Windows 8", 'p12'],
            "win8.1"      => ["Windows 8.1", 'p12'],
            "winphone10"  => ["Windows Phone 10", 'p12'],
            "winphone8.1" => ["Windows Phone 8.1", 'p12'],
            "winvista"    => ["Windows Vista", 'p12'],

            "xbox_360"               => ["Xbox 360", null],
            "xbox_os"                => ["Xbox OS", null],
            "xbox_os_10"             => ["Xbox OS 10", null],
            "xbox_os_10_mobile_view" => ["Xbox OS 10 (Mobile View)", null],

            "unknown" => [__("Unknown platform"), null],
        ];
    }

    protected function getPlatform()
    {
        $browser = get_browser();
        if ($browser === false) {
            return 'unknown';
        } else {
            return strtolower(str_replace(
                [' ', '(', ')'],
                ['_', '',  ''],
                $browser->platform
            ));
        }
    }


    public function index(string $platform = null)
    {
        if (isset($this->request->getQueryParams()['url'])) {
            if ($this->request->getUri()->getScheme() === "https") {
                $url = filter_var(
                    urldecode($this->request->getQueryParams()['url']),
                    FILTER_VALIDATE_URL
                );
                if ($url === false)
                    $url = 'https://mydatakeeper.co/';
                return $this->redirect($url);
            }
        }

        $certs = $this->getCertificates();
        $platform = $platform ?: $this->getPlatform();
        $platforms = $this->getPlatforms();
        if (!array_key_exists($platform, $platforms)) {
            throw new NotFoundException();
        }

        $this->set(compact('certs', 'platforms', 'platform'));
    }

    public function download(string $format = null)
    {
        $browser = get_browser();
        $certs = $this->getCertificates();
        $format = $format ?: $this->getPlatforms()[$this->getPlatform()][1];
        if (!array_key_exists($format, $certs))
        {
            throw new NotFoundException();
        }

        return $this->response->withFile($certs[$format], [
            'name' => "mydatakeeper.$format",
            'download' => true,
        ]);
    }
}
