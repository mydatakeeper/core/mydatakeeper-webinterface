<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\EventInterface;
use Cake\Http\Exception\NotFoundException;
use Cake\Routing\Asset;
use Cake\Routing\Router;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ApplicationController extends AppController
{
    /**
     * Allow unauthenticated user to access some pages
     */
    public function beforeFilter(EventInterface $event): void
    {
        parent::beforeFilter($event);
        $this->Auth->allow('file');
    }

    public function index()
    {
        $this->loadModel('Applications');

        $applications = [];
        foreach ($this->Applications->getAll(['individual' => true]) as $application) {
            $errors = $application->getErrors();
            if ($errors) {
                $this->Flash()->set($errors, ['element' => 'error']);
            } else {
                $applications[] = $application;
            }
        }

        $applications[] = $this->Applications->newEntity([
            'name' => __("Settings"),
            'hasInterface' => true,
            '_interfaceLink' => Router::url([
                'controller' => 'settings',
                'action' => 'index',
            ]),
            '_iconLink' => Asset::imageUrl('settings.svg'),
        ]);

        $this->set(compact('applications'));
    }

    public function view($id)
    {
        $this->viewBuilder()->setClassName('Application');

        $this->loadModel('Ifaces');

        $interface = $this->Ifaces->getById($id);
        if (!$interface)
            throw new NotFoundException();

        $this->set(compact('interface'));
    }

    public function file($id, $name)
    {
        $this->loadModel('Ifaces');

        $interface = $this->Ifaces->getById($id);
        if (!$interface)
            throw new NotFoundException();

        if (!in_array($name, array_merge([$interface->index], $interface->files)))
            throw new NotFoundException();

        $root = realpath($interface->root) . '/';
        $path = realpath($root . $name);
        if (strncmp($root, $path, strlen($root)) !== 0)
            throw new \Exception("File is not inside valid root folder");

        return $this->response->withFile($path);
    }

    public function method($id)
    {
        $this->loadModel('Ifaces');

        $interface = $this->Ifaces->getById($id);
        if (!$interface)
            throw new NotFoundException();

        $data = $this->request->input('json_decode', true);

        // Check 'method' value
        if (!isset($data['method']) || empty($data['method'])) {
            return $this->response->withStatus(400)->withStringBody(
                json_encode("Missing method name")
            );
        }
        $method = $data['method'];
        $methods = $interface->methods;
        if (!array_key_exists($method, $methods)) {
            return $this->response->withStatus(400)->withStringBody(
                json_encode("Bad method name")
            );
        }

        // Check 'args' value
        if (!isset($data['args'])) {
            return $this->response->withStatus(400)->withStringBody(
                json_encode("Missing arguments")
            );
        }
        $args = $data['args'];
        $defs = $methods[$method];

        // Call method
        try {
            $data = $interface->call_method($method, $args);
        } catch (\Exception $e) {
            return $this->response->withStatus(500)->withStringBody(
                json_encode($e->getMessage())
            );
        }

        return $this->response->withStringBody(json_encode($data));
    }
}
