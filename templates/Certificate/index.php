<?php

$this->assign('title', __("Install your security certificate"));

$pretty_platform = $platforms[$platform][0];
$format = $platforms[$platform][1];

// Trigger a redirect on page loading which will trigger certificate download
$params = $this->request->getQueryParams();
$auto_download = isset($params["auto_download"]) ? !!$params["auto_download"] : false;
if ($auto_download && array_key_exists($format, $certs)) {
    $this->Html->meta([
        'http-equiv' => 'refresh',
        'content' => '0;URL=' . $this->Url->build(['action' => 'download', $format]),
    ], null, [
        'block' => true,
    ]);
}

?>

<div class="container-fluid p-4" id="installation-instructions">
    <div class="row justify-content-center pb-3">
        <h3 class="text-center">
            <?= __("Certificate installation instructions for {0}", $pretty_platform); ?>
        </h3>
    </div>
    <?php if ($format) { ?>
        <div class="row justify-content-center pb-5">
            <div class="col-12 col-md-9 col-xl-6">
                <?= $this->Html->link(
                    __("Download your certificate ({0} version)", $pretty_platform),
                    ['action' => 'download', $format],
                    ['class' => 'btn btn-primary btn-lg w-100 text-wrap']
                ); ?>
            </div>
        </div>
    <?php } ?>
    <div class="row justify-content-center">
        <?= $this->element('Certificate/' . $platform); ?>
    </div>
</div>
<div class="container-fluid p-4" id="why-install-certificate">
    <div class="row justify-content-center">
        <div class="col-12 col-md-9 col-xl-6">
            <h3 class="text-center"><?= __("Why install this certificate?") ?></h3>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-12 col-md-9 col-xl-6">
            <p class="text-justify">
                <?= __("When browsing the internet, there are (mainly) two kind of traffic : <a href=\"https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol\">HTTP</a> and <a href=\"https://en.wikipedia.org/wiki/HTTPS\">HTTPS</a>. Your {0} box intercept this traffic to verify what is going out <b>and</b> what is coming in. It then proceed to a sanitization of your traffic.", '<span class="timeburner">my<b>data</b>keeper</span>', ) ?>
            </p>
            <p class="text-justify">
                <?= __("When working on HTTP traffic, your {0} box can intercept and sanitize your traffic transparently (as detailed in the following diagram). You don't need to do anything.", '<span class="timeburner">my<b>data</b>keeper</span>') ?>
            </p>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-12 col-md-9 col-xl-6">
            <?php
                $url = $this->Url->build(['controller' => 'Page', 'action' => 'image', 'http-proxy.svg']);
                echo $this->Html->image($url , [
                    'alt' => __("HTTP proxy"),
                    'url' => $url,
                    'style' => "width:100%;height:auto",
                ]);
            ?>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-12 col-md-9 col-xl-6">
            <p class="text-justify">
                <?= __("The \"S\" in HTTPS stands for \"secure\". You can check wether your connection is secured by looking at the address bar of the page you are currently visiting. A secured connection is encrypting all the traffic between you and the website (end-to-end encryption). To make sure you are receiving traffic from the real website, without anyone listening in the middle, your computer will check wether the website certificate is known and is valid.") ?>
            </p>
            <p class="text-justify">
                <?= __("In this case, your {0} box cannot intercept yout traffic transparently. You need to tell your computer that you trust {0} to intercept and (potentially) alter your traffic. To do so, you need to <a href=\"{1}\">install your box certificate</a>. Without this certificate, your computer will show you an error since it does not trust the origin of the traffic.", '<span class="timeburner">my<b>data</b>keeper</span>', $this->Url->build([
                'controller' => 'Certificate',
                'action' => 'index',
                'auto_download' => true,
            ])) ?>
            </p>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-12 col-md-9 col-xl-6">
            <?php
                $url = $this->Url->build(['controller' => 'Page', 'action' => 'image', 'https-proxy.svg']);
                echo $this->Html->image($url , [
                    'alt' => __("HTTPS proxy"),
                    'url' => $url,
                    'style' => "width:100%;height:auto",
                ]);
            ?>
        </div>
    </div>
</div>
<div class="container-fluid p-4" id="other-platform-instructions">
    <div class="row justify-content-center">
        <div class="col-12 pb-2">
            <h3 class="text-center"><?= __("Certificate installation instructions for every platform") ?></h3>
        </div>
        <div class="col-12 col-md-9 col-xl-6">
            <div class="card mt-3">
                <h5 class="card-header"><?= __("All platforms") ?></h5>
                <ul class="list-group  list-group-flush">
                <?php foreach ($platforms as $platform => $info) { ?>
                    <li class="list-group-item">
                        <?= $this->Html->link(__("Certificate installation instructions for {0}", $info[0]), [$platform]); ?>
                    </li>
                <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid p-4" id="other-certificate-formats">
    <div class="row justify-content-center">
        <div class="col-12 pb-2">
            <h3 class="text-center"><?= __("All the certificate formats available") ?></h3>
        </div>
        <div class="col-12 col-md-9 col-xl-6">
            <div class="card mt-3">
                <h5 class="card-header"><?= __("All formats") ?></h5>
                <ul class="list-group  list-group-flush">
                <?php foreach ($certs as $format => $path) { ?>
                    <li class="list-group-item">
                        <?= $this->Html->link(__("Certificate in ''{0}' format", $format), ['action' => 'download', $format]); ?>
                    </li>
                <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>
