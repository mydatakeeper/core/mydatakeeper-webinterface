<?php
use Cake\I18n\I18n;

$this->assign('title', __("''{0}' license(s)", $package->Name));
?>
<div class="container-fluid">
    <div class="row justify-content-center font-weight-bold">
        <div class="col-12 col-sm-10 col-md-8 col-lg-6 trans">
            <div class="card my-2">
                <div class="card-header font-weight-bold">
                    <?= __("Package ''{0}' license(s)", $package->Name) ?>
                </div>
                <div class="card-body font-weight-normal">
                    <div class="my-2">
                        <span class="font-weight-bold"><?= __("License(s):") ?></span>
                        <?php foreach ($package->Licenses as $license => $filename) { ?>
                            <?= empty($filename) ? $license : $this->Html->link($license, [
                                'action' => 'license',
                                urlencode($package->id),
                                urlencode($license),
                            ]); ?>
                        <?php } ?>
                    </div>
                    <?php if (!empty($package->CustomFiles)) { ?>
                    <div class="my-2">
                        <span class="font-weight-bold"><?= __("Custom license(s) file(s):") ?></span>
                        <?php foreach ($package->CustomFiles as $index => $filename) { ?>
                            <?= $this->Html->link(__($filename, $index), [
                                'action' => 'custom',
                                urlencode($package->id),
                                urlencode($filename)
                            ]); ?>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </div>
                <div class="card-footer text-right">
                    <small class="text-muted">
                        <?= __("Version:") ?> <?= $package->Version ?>
                    </small>
                </div>
            </div>
        </div>
    </div>
</div>
