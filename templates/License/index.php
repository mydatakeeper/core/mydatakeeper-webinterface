<?php
use Cake\I18n\I18n;

$this->assign('title', __("Licenses"));
?>
<div class="container-fluid">
    <div class="row justify-content-center">
        <p class="col-12 px-5">
            <?= __("The Mydatakeeper brand, as well as its logo displayed on this website are trademarks. Any complete or partial reproduction of these brands without the explicit consent of  Mydatakeeper S.A.S. is forbidden, with regards to the L.713-2 and following articles of the French Code de la propriété intellectuelle.") ?>
        </p>
    </div>
    <div class="row justify-content-center">
        <p class="col-12 px-5">
            <?= __("At Mydatakeeper, we believe in the <a href=\"https://en.wikipedia.org/wiki/Open-source_model\">Open-source model</a>. We are using open-source software whose list (and licenses) is provided below. The code used in our boxes is also under open-source licenses and you can retrieve it from our <a href=\"https://gitlab.com/mydatakeeper\">GitLab</a>.") ?>
        </p>
    </div>
</div>
<div class="container-fluid py-5">
    <div class="row justify-content-center font-weight-bold">
        <div class="col-6 col-md-4 col-lg-3 trans"><?= __("Package") ?></div>
        <div class="d-none d-md-block col-md-4 col-lg-2 trans"><?= __("Version") ?></div>
        <div class="col-6 col-md-4 col-lg-3 trans"><?= __("License(s) kind(s)") ?></div>
    </div>
    <?php foreach ($packages as $package) { ?>
        <div class="row justify-content-center">
            <div class="col-6 col-md-4 col-lg-3 trans">
                <?= $this->Html->link($package->Name, [
                    'action' => 'package',
                    urlencode($package->id),
                ]); ?>
            </div>
            <div class="d-none d-md-block col-md-4 col-lg-2 trans">
                <small class="text-muted"><?= $package->Version ?></small>
            </div>
            <div class="col-6 col-md-4 col-lg-3 trans">
                <?php foreach ($package->Licenses as $license => $filename) { ?>
                    <?= empty($filename) ? $license : $this->Html->link($license, [
                        'action' => 'license',
                        urlencode($package->id),
                        urlencode($license),
                    ]); ?>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
</div>
