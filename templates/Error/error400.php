<?php
use Cake\Core\Configure;
use Cake\Error\Debugger;

if (Configure::read('debug')) :
    $this->layout = 'dev_error';

    $this->assign('title', $message);
    $this->assign('templateName', 'error400.ctp');

    $this->start('file');
?>
<?php if (!empty($error->queryString)) : ?>
    <p class="notice">
        <strong>SQL Query: </strong>
        <?= h($error->queryString) ?>
    </p>
<?php endif; ?>
<?php if (!empty($error->params)) : ?>
        <strong>SQL Query Params: </strong>
        <?php Debugger::dump($error->params) ?>
<?php endif; ?>
<?= $this->element('auto_table_warning') ?>
<?php
if (extension_loaded('xdebug')) :
    xdebug_print_function_stack();
endif;

$this->end();
endif;
?>
<div class="row justify-content-center">
    <div class="col-12 col-md-9 col-xl-6">
        <h4 class="text-center px-5"><?= __("Error") ?></h4>
        <div class="form px-5">
            <p class="text-center">
                <?= __("The requested address {0} was not found on this server.", "<strong>'{$url}'</strong>");?>
            </p>
        </div>
    </div>
</div>
