<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
use Cake\I18n\I18n;

$title = $this->fetch('title', __("Safety first"));
$description = 'Mydatakeeper - ' . $title;
$locale = I18n::getLocale();
$locales = Configure::read('App.availableLocales');

?>
<!DOCTYPE html>
<html lang="<?= $locale ?>">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $description ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->fetch('meta') ?>

    <?= $this->Html->css('vendor/bootstrap.min') ?>
    <?= $this->Html->css('mydatakeeper.min') ?>
    <?= $this->fetch('css') ?>

    <link rel="preload" href="/font/timeburnernormal.ttf" as="font" crossorigin>
    <link rel="preload" href="/font/timeburnerbold.ttf" as="font" crossorigin>
</head>
<body>
<div class="float-right m-1">
    <div class="header-lang dropdown">
        <button class="btn btn-light border dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?= $locales[$locale]['flag'] ?>
        </button>
        <div class="dropdown-menu dropdown-menu-right p-0" aria-labelledby="dropdownMenuLink">
            <?php foreach ($locales as $locale => $info) {
                $content = $info['flag'].'<span class="d-none d-md-inline ml-md-1">'.$info['label'].'</span>';
                echo $this->Html->link($content,
                    $this->request->getParam('pass', []) + compact('locale'),
                    ['escape' => false, 'class' => 'dropdown-item']
                );
            }
            ?>
        </div>
    </div>
</div>
<header class="justify-content-start p-2">
    <div class="header-image d-inline-block align-middle p-1" style="width: 40px">
        <?= $this->Html->image('mydatakeeper-logo.svg', [
            'alt' => __("Home"),
            'url' => ['controller' => 'App', 'action' => 'index'],
        ]); ?>
    </div>
    <div class="header-title d-none d-sm-inline-block align-middle">
        <h1 class="timeburner">
            <?= $this->Html->link(
                'my<b>data</b>keeper',
                ['controller' => 'App', 'action' => 'index'],
                ['escapeTitle' => false]
            ); ?>
        </h1>
    </div>
</header>
<main>
    <?= $this->fetch('content') ?>
</main>
<footer class="border-top text-center p-3">
    <div class="align-middle">
        <span>
            <?= __("Copyright") ?> &copy;
            <span class="timeburner">my<b>data</b>keeper</span>
            2018 - <?= date('Y') ?>
        </span>
        <span><?= $this->Html->link(__("Licenses"), ['controller' => 'License', 'action' => 'index']); ?></span>
        <span><?= $this->Html->link(__("Thanks"), ['controller' => 'Page', 'action' => 'display', 'thanks']); ?></span>
        <span><?= $this->Html->link(__("Website"), 'https://mydatakeeper.co/'); ?></span>
    </div>
    <div class="align-middle">
        <?= __("Icons made by <a href='https://www.flaticon.com/authors/freepik' title='Freepik'>Freepik</a> from <a href='https://www.flaticon.com/' title='Flaticon'>www.flaticon.com</a>"); ?>
    </div>
</footer>

<?= $this->Html->script($this->Url->build([
    'controller' => 'Page',
    'action' => 'javascript',
    'lang.js'
] + compact('locale'))) ?>
<?= $this->Html->script('vendor/jquery.min') ?>
<?= $this->Html->script('vendor/bootstrap.bundle.min') ?>
<?= $this->Html->script('vendor/cleave.min') ?>
<?= $this->Html->script('mydatakeeper.min') ?>
<?= $this->fetch('script') ?>

</body>
</html>
