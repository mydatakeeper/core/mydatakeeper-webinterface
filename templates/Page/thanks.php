<?php
$this->assign('title', __("Thanks"));
$supporter_count = 89;
$super_supporters = [
    'Trespy',
];
$supporters = [
    'Amélie Chatelain',
    'Anissa Bara',
    'Audrey Renouf',
    'Bene.chat',
    'Céline Hermenier Desaintjean',
    'Charlotte Dourneau',
    'D&S',
    'Fabrice Dulamon',
    'Félicie',
    'Félix Meysen',
    'Floriane Brossaud',
    'Gauthier Caulier',
    'Gilbert Mastroianni',
    'Guiben',
    'Hugo Malagutti',
    'Johan.B',
    'Julia Couturier',
    'Julie Penin',
    'Kenitt',
    'Kévin Bondiguel',
    'Kiki',
    'Marie-Laure Renaudie',
    'Marion Raes',
    'Matthieu Freiche',
    'Maxime Oliveri',
    'Mélinda Brizard',
    'Michel Hermenier',
    'Mireille Jardeaux',
    'Philippe Dubot',
    'Pierre-Antoine Colonna',
    'Robin Dassonville',
    'Sylvain Condemi',
    'Thierry Goudout',
    'Thomas Mitard',
    'Victor Le Henaff',
    'Viviane Toussaint',
    'Y. Sarazin',
];
?>

<div class="container-fluid px-5 pb-5">
    <div class="row">
        <div class="col-12 mb-3">
            <p class="px-5 text-justify">
                <?= __("At the beginning, Mydatakeeper launched a crowdfunding campaign on <a href=\"https://www.ulule.com/mydatakeeper/\">Ulule</a> in order to raise enough money to boot its activity. No less than <b>{0}</b> people answered our call and helped us to try and make our dream come true. Here is a complete list of the people :", $supporter_count) ?>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-12 mb-5">
            <p class="text-center font-weight-bold"><?= __("BIGGEST SUPPORTERS:") ?></p>
        <?php foreach ($super_supporters as $supporter) { ?>
            <p class="px-5 mb-0 text-center"><?= h($supporter) ?></p>
        <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-12 mb-5">
            <p class="text-center font-weight-bold"><?= __("SUPPORTER:") ?></p>
        <?php foreach ($supporters as $supporter) { ?>
            <p class="px-5 mb-0 text-center"><?= h($supporter) ?></p>
        <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <p class="px-5 text-justify">
                <?= __("As well as {0} other anonymous supporters that funded our adventure in July 2018.", $supporter_count - count($supporters) - count($super_supporters)) ?>
            </p>
        </div>
        <div class="col-12">
            <p class="px-5 text-justify">
                <?= __("We are extremely thankful to these people as well as to all the others that support and believe in us daily. From the Mydatakeeper team : <b>a big thank you!</b>") ?>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <p class="px-5 text-right">
                <b>Guillaume &amp; Théo</b>
            </p>
        </div>
    </div>
</div>
