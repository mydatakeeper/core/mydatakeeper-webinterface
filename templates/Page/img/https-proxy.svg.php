<?php
function escape(int $x, string $str)
{
    return str_replace("\n", '</tspan><tspan x="' . $x . '" dx="21" dy="20">', $str);
}

$this->response = $this->response->withType('svg');

?>
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:xlink="http://www.w3.org/1999/xlink"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   width="120mm"
   height="30mm"
   viewBox="0 0 210 297"
   version="1.1"
   id="svg8"
   inkscape:export-filename="/home/theo/bitmap.png"
   inkscape:export-xdpi="96"
   inkscape:export-ydpi="96"
   sodipodi:docname="https-proxy.svg"
   inkscape:version="0.92.3 (2405546, 2018-03-11)">
  <defs
     id="defs2">
    <marker
       inkscape:stockid="Arrow2Lend"
       orient="auto"
       refY="0"
       refX="0"
       id="marker6646"
       style="overflow:visible"
       inkscape:isstock="true">
      <path
         id="path6644"
         style="fill:#000000;fill-opacity:1;fill-rule:evenodd;stroke:#000000;stroke-width:0.625;stroke-linejoin:round;stroke-opacity:1"
         d="M 8.7185878,4.0337352 -2.2072895,0.01601326 8.7185884,-4.0017078 c -1.7454984,2.3720609 -1.7354408,5.6174519 -6e-7,8.035443 z"
         transform="matrix(-1.1,0,0,-1.1,-1.1,0)"
         inkscape:connector-curvature="0" />
    </marker>
    <marker
       inkscape:stockid="Arrow2Lend"
       orient="auto"
       refY="0"
       refX="0"
       id="marker6638"
       style="overflow:visible"
       inkscape:isstock="true">
      <path
         id="path6636"
         style="fill:#000000;fill-opacity:1;fill-rule:evenodd;stroke:#000000;stroke-width:0.625;stroke-linejoin:round;stroke-opacity:1"
         d="M 8.7185878,4.0337352 -2.2072895,0.01601326 8.7185884,-4.0017078 c -1.7454984,2.3720609 -1.7354408,5.6174519 -6e-7,8.035443 z"
         transform="matrix(-1.1,0,0,-1.1,-1.1,0)"
         inkscape:connector-curvature="0" />
    </marker>
    <marker
       inkscape:stockid="Arrow2Lend"
       orient="auto"
       refY="0"
       refX="0"
       id="marker6630"
       style="overflow:visible"
       inkscape:isstock="true">
      <path
         id="path6628"
         style="fill:#000000;fill-opacity:1;fill-rule:evenodd;stroke:#000000;stroke-width:0.625;stroke-linejoin:round;stroke-opacity:1"
         d="M 8.7185878,4.0337352 -2.2072895,0.01601326 8.7185884,-4.0017078 c -1.7454984,2.3720609 -1.7354408,5.6174519 -6e-7,8.035443 z"
         transform="matrix(-1.1,0,0,-1.1,-1.1,0)"
         inkscape:connector-curvature="0" />
    </marker>
    <marker
       inkscape:stockid="Arrow2Lend"
       orient="auto"
       refY="0"
       refX="0"
       id="marker6622"
       style="overflow:visible"
       inkscape:isstock="true">
      <path
         id="path6620"
         style="fill:#000000;fill-opacity:1;fill-rule:evenodd;stroke:#000000;stroke-width:0.625;stroke-linejoin:round;stroke-opacity:1"
         d="M 8.7185878,4.0337352 -2.2072895,0.01601326 8.7185884,-4.0017078 c -1.7454984,2.3720609 -1.7354408,5.6174519 -6e-7,8.035443 z"
         transform="matrix(-1.1,0,0,-1.1,-1.1,0)"
         inkscape:connector-curvature="0" />
    </marker>
    <marker
       inkscape:stockid="Arrow2Lend"
       orient="auto"
       refY="0"
       refX="0"
       id="marker6614"
       style="overflow:visible"
       inkscape:isstock="true">
      <path
         id="path6612"
         style="fill:#000000;fill-opacity:1;fill-rule:evenodd;stroke:#000000;stroke-width:0.625;stroke-linejoin:round;stroke-opacity:1"
         d="M 8.7185878,4.0337352 -2.2072895,0.01601326 8.7185884,-4.0017078 c -1.7454984,2.3720609 -1.7354408,5.6174519 -6e-7,8.035443 z"
         transform="matrix(-1.1,0,0,-1.1,-1.1,0)"
         inkscape:connector-curvature="0" />
    </marker>
    <marker
       inkscape:stockid="Arrow2Lend"
       orient="auto"
       refY="0"
       refX="0"
       id="marker6606"
       style="overflow:visible"
       inkscape:isstock="true">
      <path
         id="path6364"
         style="fill:#000000;fill-opacity:1;fill-rule:evenodd;stroke:#000000;stroke-width:0.625;stroke-linejoin:round;stroke-opacity:1"
         d="M 8.7185878,4.0337352 -2.2072895,0.01601326 8.7185884,-4.0017078 c -1.7454984,2.3720609 -1.7354408,5.6174519 -6e-7,8.035443 z"
         transform="matrix(-1.1,0,0,-1.1,-1.1,0)"
         inkscape:connector-curvature="0" />
    </marker>
    <marker
       inkscape:stockid="Arrow2Lend"
       orient="auto"
       refY="0"
       refX="0"
       id="marker6297"
       style="overflow:visible"
       inkscape:isstock="true">
      <path
         inkscape:connector-curvature="0"
         id="path6295"
         style="fill:#000000;fill-opacity:1;fill-rule:evenodd;stroke:#000000;stroke-width:0.625;stroke-linejoin:round;stroke-opacity:1"
         d="M 8.7185878,4.0337352 -2.2072895,0.01601326 8.7185884,-4.0017078 c -1.7454984,2.3720609 -1.7354408,5.6174519 -6e-7,8.035443 z"
         transform="matrix(-1.1,0,0,-1.1,-1.1,0)" />
    </marker>
    <marker
       inkscape:stockid="Arrow2Lend"
       orient="auto"
       refY="0"
       refX="0"
       id="marker6911"
       style="overflow:visible"
       inkscape:isstock="true">
      <path
         id="path6909"
         style="fill:#000000;fill-opacity:1;fill-rule:evenodd;stroke:#000000;stroke-width:0.625;stroke-linejoin:round;stroke-opacity:1"
         d="M 8.7185878,4.0337352 -2.2072895,0.01601326 8.7185884,-4.0017078 c -1.7454984,2.3720609 -1.7354408,5.6174519 -6e-7,8.035443 z"
         transform="matrix(-1.1,0,0,-1.1,-1.1,0)"
         inkscape:connector-curvature="0" />
    </marker>
    <marker
       inkscape:stockid="Arrow2Lend"
       orient="auto"
       refY="0"
       refX="0"
       id="marker6833"
       style="overflow:visible"
       inkscape:isstock="true">
      <path
         id="path6831"
         style="fill:#000000;fill-opacity:1;fill-rule:evenodd;stroke:#000000;stroke-width:0.625;stroke-linejoin:round;stroke-opacity:1"
         d="M 8.7185878,4.0337352 -2.2072895,0.01601326 8.7185884,-4.0017078 c -1.7454984,2.3720609 -1.7354408,5.6174519 -6e-7,8.035443 z"
         transform="matrix(-1.1,0,0,-1.1,-1.1,0)"
         inkscape:connector-curvature="0" />
    </marker>
    <marker
       inkscape:stockid="Arrow2Lend"
       orient="auto"
       refY="0"
       refX="0"
       id="marker6787"
       style="overflow:visible"
       inkscape:isstock="true">
      <path
         id="path6785"
         style="fill:#000000;fill-opacity:1;fill-rule:evenodd;stroke:#000000;stroke-width:0.625;stroke-linejoin:round;stroke-opacity:1"
         d="M 8.7185878,4.0337352 -2.2072895,0.01601326 8.7185884,-4.0017078 c -1.7454984,2.3720609 -1.7354408,5.6174519 -6e-7,8.035443 z"
         transform="matrix(-1.1,0,0,-1.1,-1.1,0)"
         inkscape:connector-curvature="0" />
    </marker>
    <marker
       inkscape:stockid="Arrow2Lstart"
       orient="auto"
       refY="0"
       refX="0"
       id="Arrow2Lstart"
       style="overflow:visible"
       inkscape:isstock="true">
      <path
         id="path6423"
         style="fill:#000000;fill-opacity:1;fill-rule:evenodd;stroke:#000000;stroke-width:0.625;stroke-linejoin:round;stroke-opacity:1"
         d="M 8.7185878,4.0337352 -2.2072895,0.01601326 8.7185884,-4.0017078 c -1.7454984,2.3720609 -1.7354408,5.6174519 -6e-7,8.035443 z"
         transform="matrix(1.1,0,0,1.1,1.1,0)"
         inkscape:connector-curvature="0" />
    </marker>
    <marker
       inkscape:stockid="Arrow2Lend"
       orient="auto"
       refY="0"
       refX="0"
       id="marker6715"
       style="overflow:visible"
       inkscape:isstock="true">
      <path
         id="path6713"
         style="fill:#000000;fill-opacity:1;fill-rule:evenodd;stroke:#000000;stroke-width:0.625;stroke-linejoin:round;stroke-opacity:1"
         d="M 8.7185878,4.0337352 -2.2072895,0.01601326 8.7185884,-4.0017078 c -1.7454984,2.3720609 -1.7354408,5.6174519 -6e-7,8.035443 z"
         transform="matrix(-1.1,0,0,-1.1,-1.1,0)"
         inkscape:connector-curvature="0" />
    </marker>
    <marker
       inkscape:stockid="Arrow2Lend"
       orient="auto"
       refY="0"
       refX="0"
       id="Arrow2Lend"
       style="overflow:visible"
       inkscape:isstock="true">
      <path
         id="path6426"
         style="fill:#000000;fill-opacity:1;fill-rule:evenodd;stroke:#000000;stroke-width:0.625;stroke-linejoin:round;stroke-opacity:1"
         d="M 8.7185878,4.0337352 -2.2072895,0.01601326 8.7185884,-4.0017078 c -1.7454984,2.3720609 -1.7354408,5.6174519 -6e-7,8.035443 z"
         transform="matrix(-1.1,0,0,-1.1,-1.1,0)"
         inkscape:connector-curvature="0" />
    </marker>
    <marker
       inkscape:stockid="Arrow1Lend"
       orient="auto"
       refY="0"
       refX="0"
       id="Arrow1Lend"
       style="overflow:visible"
       inkscape:isstock="true">
      <path
         id="path6408"
         d="M 0,0 5,-5 -12.5,0 5,5 Z"
         style="fill:#000000;fill-opacity:1;fill-rule:evenodd;stroke:#000000;stroke-width:1.00000003pt;stroke-opacity:1"
         transform="matrix(-0.8,0,0,-0.8,-10,0)"
         inkscape:connector-curvature="0" />
    </marker>
    <linearGradient
       id="linearGradient5635">
      <stop
         offset="0"
         style="stop-color:#ffffff;stop-opacity:1"
         id="stop5637" />
      <stop
         offset="1"
         style="stop-color:#ededed;stop-opacity:1"
         id="stop5639" />
    </linearGradient>
    <linearGradient
       id="linearGradient5584">
      <stop
         offset="0"
         style="stop-color:#ffffff;stop-opacity:1"
         id="stop5586" />
      <stop
         offset="1"
         style="stop-color:#ffffff;stop-opacity:0"
         id="stop5588" />
    </linearGradient>
    <linearGradient
       id="linearGradient9590">
      <stop
         offset="0"
         style="stop-color:#bfbfbf;stop-opacity:1"
         id="stop9592" />
      <stop
         offset="1"
         style="stop-color:#f8f8f8;stop-opacity:1"
         id="stop9594" />
    </linearGradient>
    <linearGradient
       gradientTransform="matrix(0.0913,0,0,0.0937,69.69,218.6)"
       gradientUnits="userSpaceOnUse"
       xlink:href="#linearGradient9590"
       id="linearGradient2944"
       y2="-1918"
       x2="-500.70001"
       y1="-2279"
       x1="-543.29999" />
    <linearGradient
       gradientUnits="userSpaceOnUse"
       xlink:href="#linearGradient5635"
       id="linearGradient2946"
       y2="3.5"
       x2="20.75"
       y1="23"
       x1="22.5" />
    <linearGradient
       gradientUnits="userSpaceOnUse"
       xlink:href="#linearGradient5584"
       id="linearGradient2948"
       y2="42.610001"
       x2="26.09"
       y1="2.4860001"
       x1="25.98" />
    <linearGradient
       gradientTransform="translate(0.002,252)"
       gradientUnits="userSpaceOnUse"
       xlink:href="#linearGradient5584"
       id="linearGradient2880"
       y2="42.610001"
       x2="26.09"
       y1="2.4860001"
       x1="25.98" />
    <linearGradient
       gradientTransform="translate(0.002,252)"
       gradientUnits="userSpaceOnUse"
       xlink:href="#linearGradient5635"
       id="linearGradient2894"
       y2="3.5"
       x2="20.75"
       y1="23"
       x1="22.5" />
    <linearGradient
       gradientTransform="matrix(0.0913,0,0,0.0937,69.69,470.6)"
       gradientUnits="userSpaceOnUse"
       xlink:href="#linearGradient9590"
       id="linearGradient2898"
       y2="-1918"
       x2="-500.70001"
       y1="-2279"
       x1="-543.29999" />
    <linearGradient
       id="linearGradient4196">
      <stop
         id="stop4198"
         offset="0"
         style="stop-color:black;stop-opacity:1;" />
      <stop
         id="stop4200"
         offset="1"
         style="stop-color:black;stop-opacity:0;" />
    </linearGradient>
    <linearGradient
       id="linearGradient3899"
       inkscape:collect="always">
      <stop
         id="stop3901"
         offset="0"
         style="stop-color:#eeeeec" />
      <stop
         id="stop3903"
         offset="1"
         style="stop-color:#d3d7cf" />
    </linearGradient>
    <linearGradient
       id="linearGradient5233">
      <stop
         id="stop5235"
         offset="0"
         style="stop-color:#729fcf;stop-opacity:1;" />
      <stop
         id="stop5237"
         offset="1"
         style="stop-color:#326194;stop-opacity:1;" />
    </linearGradient>
    <linearGradient
       id="linearGradient5137">
      <stop
         id="stop5139"
         offset="0"
         style="stop-color:#eeeeec;stop-opacity:1;" />
      <stop
         id="stop5141"
         offset="1"
         style="stop-color:#e6e6e3;stop-opacity:1;" />
    </linearGradient>
    <linearGradient
       gradientTransform="translate(-0.0235294,-3.039216)"
       gradientUnits="userSpaceOnUse"
       y2="26.039215"
       x2="20.156862"
       y1="5.0996137"
       x1="20.156862"
       id="linearGradient6246"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <linearGradient
       gradientTransform="matrix(0.999303,0,0,0.998527,0.00306125,-2.971316)"
       y2="38.876041"
       x2="39.904388"
       y1="6.3760414"
       x1="17.247635"
       gradientUnits="userSpaceOnUse"
       id="linearGradient5147"
       xlink:href="#linearGradient5137"
       inkscape:collect="always" />
    <radialGradient
       gradientUnits="userSpaceOnUse"
       gradientTransform="matrix(1.232634,0,0,0.778392,-5.590582,-0.847446)"
       r="19.00016"
       fy="32.997028"
       fx="24.006104"
       cy="32.997028"
       cx="24.006104"
       id="radialGradient5239"
       xlink:href="#linearGradient5233"
       inkscape:collect="always" />
    <linearGradient
       y2="43.82579"
       x2="31.86105"
       y1="37.842293"
       x1="31.743324"
       gradientTransform="matrix(1,0,0,0.992781,0,-2.718035)"
       gradientUnits="userSpaceOnUse"
       id="linearGradient2308"
       xlink:href="#linearGradient5137"
       inkscape:collect="always" />
    <linearGradient
       y2="40.219608"
       x2="23.529411"
       y1="34.572548"
       x1="23.154902"
       gradientTransform="matrix(1.004187,0,0,1,-0.12454,-3.011765)"
       gradientUnits="userSpaceOnUse"
       id="linearGradient2310"
       xlink:href="#linearGradient4196"
       inkscape:collect="always" />
    <linearGradient
       inkscape:collect="always"
       id="linearGradient1558">
      <stop
         style="stop-color:#babdb6"
         offset="0"
         id="stop1560" />
      <stop
         style="stop-color:#eeeeec"
         offset="1"
         id="stop1562" />
    </linearGradient>
    <linearGradient
       id="linearGradient3042">
      <stop
         style="stop-color:black;stop-opacity:0;"
         offset="0"
         id="stop3044" />
      <stop
         id="stop3050"
         offset="0.5"
         style="stop-color:black;stop-opacity:1;" />
      <stop
         style="stop-color:black;stop-opacity:0;"
         offset="1"
         id="stop3046" />
    </linearGradient>
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient2433"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="3"
       fy="33.214287"
       fx="2.9999995"
       cy="33.214287"
       cx="2.9999995"
       gradientTransform="matrix(0,-0.750002,1.333334,0,-40.28573,35.00001)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient2517"
       xlink:href="#linearGradient4196"
       inkscape:collect="always" />
    <radialGradient
       r="3"
       fy="33.214287"
       fx="2.9999995"
       cy="33.214287"
       cx="2.9999995"
       gradientTransform="matrix(0,-0.750002,1.333334,0,-88.28573,-30.49999)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient2519"
       xlink:href="#linearGradient4196"
       inkscape:collect="always" />
    <linearGradient
       y2="29.995127"
       x2="37.03125"
       y1="35.062885"
       x1="37.03125"
       gradientTransform="matrix(1,0,0,0.888889,0,3.833333)"
       gradientUnits="userSpaceOnUse"
       id="linearGradient2521"
       xlink:href="#linearGradient3042"
       inkscape:collect="always" />
    <linearGradient
       y2="32.448051"
       x2="16.396038"
       y1="19.659277"
       x1="16.396038"
       gradientTransform="matrix(1,0,0,0.833169,-48,19.08245)"
       gradientUnits="userSpaceOnUse"
       id="linearGradient3320"
       xlink:href="#linearGradient1558"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3322"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3324"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3326"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3328"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3330"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3332"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3334"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3336"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3338"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3340"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3342"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3344"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3346"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3348"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3350"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3352"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3354"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3356"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3358"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3360"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3362"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3364"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3366"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3368"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3370"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3372"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3374"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3376"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3378"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3380"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3382"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3384"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3386"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3388"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3390"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3392"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3394"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3396"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3398"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3400"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3402"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3404"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3406"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3408"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3410"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3412"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3414"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3416"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3418"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3420"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3422"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3424"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3426"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3428"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3430"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3432"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3434"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3436"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3438"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <radialGradient
       r="19.5"
       fy="47.045319"
       fx="20.913568"
       cy="47.045319"
       cx="20.913568"
       gradientTransform="matrix(1.382603,3.581398e-5,0,0.171503,-6.729684,34.0972)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient3440"
       xlink:href="#linearGradient5584"
       inkscape:collect="always" />
    <linearGradient
       y2="35.75"
       x2="26.5"
       y1="43.249905"
       x1="26.5"
       gradientTransform="matrix(1,0,0,0.753044,-48,12.25251)"
       gradientUnits="userSpaceOnUse"
       id="linearGradient3442"
       xlink:href="#linearGradient3899"
       inkscape:collect="always" />
    <radialGradient
       r="23.75956"
       fy="42.6875"
       fx="23.9375"
       cy="42.6875"
       cx="23.9375"
       gradientTransform="matrix(1,0,0,0.24763,0,32.1168)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient4337"
       xlink:href="#linearGradient4196"
       inkscape:collect="always" />
    <linearGradient
       y2="33.637787"
       x2="37.295498"
       y1="38.267769"
       x1="37.484837"
       gradientTransform="translate(24)"
       gradientUnits="userSpaceOnUse"
       id="linearGradient4343"
       xlink:href="#linearGradient4196"
       inkscape:collect="always" />
    <linearGradient
       inkscape:collect="always"
       id="linearGradient4391">
      <stop
         style="stop-color:#729fcf;stop-opacity:1"
         offset="0"
         id="stop4393" />
      <stop
         style="stop-color:#729fcf;stop-opacity:0"
         offset="1"
         id="stop4395" />
    </linearGradient>
    <linearGradient
       id="linearGradient4233">
      <stop
         style="stop-color:#d7d7d7;stop-opacity:1;"
         offset="0"
         id="stop4235" />
      <stop
         style="stop-color:#eeeeec;stop-opacity:1"
         offset="1"
         id="stop4237" />
    </linearGradient>
    <linearGradient
       id="linearGradient4175"
       inkscape:collect="always">
      <stop
         id="stop4177"
         offset="0"
         style="stop-color:#d3d7cf;stop-opacity:1" />
      <stop
         id="stop4179"
         offset="1"
         style="stop-color:#eeeeec;stop-opacity:1" />
    </linearGradient>
    <linearGradient
       id="linearGradient2833">
      <stop
         style="stop-color:#959595;stop-opacity:1;"
         offset="0"
         id="stop2835" />
      <stop
         style="stop-color:white;stop-opacity:0.85576922;"
         offset="1"
         id="stop2837" />
    </linearGradient>
    <linearGradient
       id="linearGradient3859">
      <stop
         id="stop3861"
         offset="0"
         style="stop-color:#d3d7cf;stop-opacity:1" />
      <stop
         id="stop3037"
         offset="0.30973485"
         style="stop-color:#eeeeec;stop-opacity:1;" />
      <stop
         style="stop-color:white;stop-opacity:1;"
         offset="0.88648254"
         id="stop3039" />
      <stop
         id="stop3863"
         offset="1"
         style="stop-color:#d3d7cf;stop-opacity:1" />
    </linearGradient>
    <linearGradient
       id="linearGradient3942">
      <stop
         id="stop3944"
         offset="0"
         style="stop-color:#888a85;stop-opacity:1" />
      <stop
         id="stop3946"
         offset="1"
         style="stop-color:#eeeeec;stop-opacity:1" />
    </linearGradient>
    <linearGradient
       id="linearGradient2809">
      <stop
         style="stop-color:#d3d7cf;stop-opacity:1;"
         offset="0"
         id="stop2811" />
      <stop
         style="stop-color:#eeeeec;stop-opacity:0;"
         offset="1"
         id="stop2813" />
    </linearGradient>
    <radialGradient
       r="117.14286"
       fy="486.64789"
       fx="605.71429"
       cy="486.64789"
       cx="605.71429"
       gradientTransform="matrix(-2.774389,0,0,1.969706,112.7623,-872.8854)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient6719-0"
       xlink:href="#linearGradient4196"
       inkscape:collect="always" />
    <radialGradient
       r="117.14286"
       fy="486.64789"
       fx="605.71429"
       cy="486.64789"
       cx="605.71429"
       gradientTransform="matrix(2.774389,0,0,1.969706,-1891.633,-872.8854)"
       gradientUnits="userSpaceOnUse"
       id="radialGradient6717-6"
       xlink:href="#linearGradient4196"
       inkscape:collect="always" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient3042"
       id="linearGradient5270"
       gradientUnits="userSpaceOnUse"
       gradientTransform="matrix(2.774389,0,0,1.969706,-1892.179,-872.8854)"
       x1="302.85715"
       y1="366.64789"
       x2="302.85715"
       y2="609.50507" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient2809"
       id="linearGradient5272"
       gradientUnits="userSpaceOnUse"
       gradientTransform="matrix(-0.909091,0,0,1,46.95455,-0.999999)"
       x1="7.5030818"
       y1="27.499998"
       x2="1.0550151"
       y2="27.499998" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient4391"
       id="linearGradient5274"
       gradientUnits="userSpaceOnUse"
       gradientTransform="matrix(-0.909091,0,0,1,46.04546,1.999997)"
       x1="6.5030808"
       y1="24.500002"
       x2="0.055013988"
       y2="24.500002" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient2809"
       id="linearGradient5276"
       gradientUnits="userSpaceOnUse"
       gradientTransform="matrix(0.909091,0,0,1,1.045449,-1)"
       x1="7.5030818"
       y1="27.499998"
       x2="1.0550151"
       y2="27.499998" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient4391"
       id="linearGradient5278"
       gradientUnits="userSpaceOnUse"
       gradientTransform="matrix(0.909091,0,0,1,1.954541,1.999997)"
       x1="6.5030808"
       y1="24.500002"
       x2="0.055013988"
       y2="24.500002" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient3859"
       id="linearGradient5280"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(-40,-1)"
       x1="73.05397"
       y1="6.5458651"
       x2="53.61911"
       y2="6.5458651" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient4175"
       id="linearGradient5282"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(-40,-1)"
       x1="60.426777"
       y1="18.520107"
       x2="60.426777"
       y2="72.082878" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient4233"
       id="linearGradient5284"
       gradientUnits="userSpaceOnUse"
       gradientTransform="matrix(0.882353,0,0,1,2.70589,1)"
       x1="24.92893"
       y1="12.001067"
       x2="24.92893"
       y2="10.868282" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient4233"
       id="linearGradient5286"
       gradientUnits="userSpaceOnUse"
       gradientTransform="matrix(0.882353,0,0,1,2.70589,1)"
       x1="24.960155"
       y1="19.00001"
       x2="24.92893"
       y2="17.762573" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient2833"
       id="linearGradient5288"
       gradientUnits="userSpaceOnUse"
       gradientTransform="matrix(0.999998,0,0,1.000003,6.00003,-7.000134)"
       x1="10.249995"
       y1="32.593761"
       x2="12.500004"
       y2="36.374992" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient3942"
       id="linearGradient5290"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(-40,-1)"
       x1="53.5625"
       y1="4.4916701"
       x2="53.5625"
       y2="37.256325" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient4196"
       id="linearGradient5292"
       gradientUnits="userSpaceOnUse"
       x1="57.49662"
       y1="38.277283"
       x2="57.49662"
       y2="31.250401" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient4196"
       id="linearGradient5294"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(2)"
       x1="57.49662"
       y1="38.277283"
       x2="57.49662"
       y2="31.250401" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient4196"
       id="linearGradient5296"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(4)"
       x1="57.49662"
       y1="38.277283"
       x2="57.49662"
       y2="31.250401" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient4196"
       id="linearGradient5298"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(6)"
       x1="57.49662"
       y1="38.277283"
       x2="57.49662"
       y2="31.250401" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient4196"
       id="linearGradient5300"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(8)"
       x1="57.49662"
       y1="38.277283"
       x2="57.49662"
       y2="31.250401" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient4196"
       id="linearGradient5302"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(10)"
       x1="57.49662"
       y1="38.277283"
       x2="57.49662"
       y2="31.250401" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient9590"
       id="linearGradient5451"
       gradientUnits="userSpaceOnUse"
       gradientTransform="matrix(0.0913,0,0,0.0937,69.69,470.6)"
       x1="-543.29999"
       y1="-2279"
       x2="-500.70001"
       y2="-1918" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient5635"
       id="linearGradient5453"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(0.002,252)"
       x1="22.5"
       y1="23"
       x2="20.75"
       y2="3.5" />
    <linearGradient
       inkscape:collect="always"
       xlink:href="#linearGradient5584"
       id="linearGradient5455"
       gradientUnits="userSpaceOnUse"
       gradientTransform="translate(0.002,252)"
       x1="25.98"
       y1="2.4860001"
       x2="26.09"
       y2="42.610001" />
  </defs>
  <sodipodi:namedview
     id="base"
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1.0"
     inkscape:pageopacity="0.0"
     inkscape:pageshadow="2"
     inkscape:zoom="1.4"
     inkscape:cx="272.64635"
     inkscape:cy="123.36384"
     inkscape:document-units="mm"
     inkscape:current-layer="layer2"
     showgrid="true"
     showguides="false"
     inkscape:window-width="1315"
     inkscape:window-height="713"
     inkscape:window-x="51"
     inkscape:window-y="27"
     inkscape:window-maximized="1">
    <inkscape:grid
       type="xygrid"
       id="grid5457" />
  </sodipodi:namedview>
  <metadata
     id="metadata5">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <g
     inkscape:groupmode="layer"
     id="layer2"
     inkscape:label="Layer 2"
     style="display:inline">
    <g
       style="display:inline;enable-background:new"
       id="g4229"
       transform="matrix(1.7452102,0,0,1.8748229,-189.44832,200.66637)">
      <g
         style="display:none"
         id="layer6"
         transform="translate(0,-252)">
        <rect
           style="display:inline;overflow:visible;visibility:visible;fill:#eeeeec;fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:2;marker:none;enable-background:accumulate"
           id="rect6284"
           y="50"
           x="296"
           height="48"
           width="48" />
        <rect
           style="display:inline;overflow:visible;visibility:visible;fill:#eeeeec;fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:2;marker:none;enable-background:accumulate"
           id="rect6592"
           y="126"
           x="303"
           height="32"
           width="32" />
        <rect
           style="display:inline;overflow:visible;visibility:visible;fill:#eeeeec;fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:2;marker:none;enable-background:accumulate"
           id="rect6749"
           y="177"
           x="303"
           height="22"
           width="22" />
        <rect
           style="display:inline;overflow:visible;visibility:visible;fill:#eeeeec;fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:2;marker:none;enable-background:accumulate"
           id="rect6833"
           y="219"
           x="303"
           height="16"
           width="16" />
        <text
           style="font-style:normal;font-weight:normal;font-size:18.30069923px;font-family:'Bitstream Vera Sans';display:inline;fill:#000000;fill-opacity:1;stroke:none;enable-background:new"
           xml:space="preserve"
           id="context"
           y="21.513618"
           x="20.970737"><tspan
             id="tspan2716"
             y="21.513618"
             x="20.970737">mimetypes</tspan></text>
        <text
           style="font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;font-size:18.30069923px;line-height:125%;font-family:'Droid Sans';-inkscape-font-specification:'Droid Sans Bold';text-align:start;writing-mode:lr-tb;text-anchor:start;display:inline;fill:#000000;fill-opacity:1;stroke:none;enable-background:new"
           xml:space="preserve"
           id="icon-name"
           y="21.513618"
           x="191.97073"><tspan
             id="tspan3023"
             y="21.513618"
             x="191.97073">application-certificate</tspan></text>
      </g>
      <g
         style="display:inline"
         id="layer2-3"
         transform="translate(0,-252)">
        <path
           style="opacity:0.03420003;fill-rule:evenodd;stroke-width:3pt"
           id="path1196"
           d="m 4.319,289 c -2.433,0 -4.3376,1.4 -4.3376,3 0,1.7 1.9046,3 4.3376,3 H 43.68 c 2.43,0 4.33,-1.3 4.33,-3 0,-1.6 -1.9,-3 -4.33,-3 z"
           inkscape:connector-curvature="0" />
        <path
           style="opacity:0.03420003;fill-rule:evenodd;stroke-width:3pt"
           id="path1195"
           d="m 4.319,289.2 c -2.222,0 -4.0202,1.3 -4.0202,2.8 0,1.6 1.7982,2.8 4.0202,2.8 H 43.68 c 2.22,0 4.02,-1.2 4.02,-2.8 0,-1.5 -1.8,-2.8 -4.02,-2.8 z"
           inkscape:connector-curvature="0" />
        <path
           style="opacity:0.03420003;fill-rule:evenodd;stroke-width:3pt"
           id="path1194"
           d="m 4.319,289.4 c -2.116,0 -3.7028,1.2 -3.7028,2.6 0,1.5 1.5868,2.6 3.7028,2.6 H 43.68 c 2.11,0 3.7,-1.1 3.7,-2.6 0,-1.4 -1.59,-2.6 -3.7,-2.6 z"
           inkscape:connector-curvature="0" />
        <path
           style="opacity:0.03420003;fill-rule:evenodd;stroke-width:3pt"
           id="path1193"
           d="m 4.319,289.6 c -1.904,0 -3.4912,1.1 -3.4912,2.4 0,1.4 1.5872,2.4 3.4912,2.4 H 43.68 c 1.9,0 3.49,-1 3.49,-2.4 0,-1.3 -1.59,-2.4 -3.49,-2.4 z"
           inkscape:connector-curvature="0" />
        <path
           style="opacity:0.03420003;fill-rule:evenodd;stroke-width:3pt"
           id="path1192"
           d="m 4.319,289.8 c -1.798,0 -3.174,1 -3.174,2.2 0,1.3 1.376,2.2 3.174,2.2 H 43.68 c 1.8,0 3.17,-0.9 3.17,-2.2 0,-1.2 -1.37,-2.2 -3.17,-2.2 z"
           inkscape:connector-curvature="0" />
        <path
           style="opacity:0.03420003;fill-rule:evenodd;stroke-width:3pt"
           id="path1191"
           d="m 4.319,290 c -1.587,0 -2.856,0.9 -2.856,2 0,1.1 1.269,2 2.856,2 H 43.68 c 1.58,0 2.85,-0.9 2.85,-2 0,-1.1 -1.27,-2 -2.85,-2 z"
           inkscape:connector-curvature="0" />
        <path
           style="opacity:0.03420003;fill-rule:evenodd;stroke-width:3pt"
           id="path1190"
           d="m 4.319,290.2 c -1.481,0 -2.645,0.8 -2.645,1.8 0,1 1.164,1.8 2.645,1.8 H 43.68 c 1.48,0 2.64,-0.8 2.64,-1.8 0,-1 -1.16,-1.8 -2.64,-1.8 z"
           inkscape:connector-curvature="0" />
        <path
           style="opacity:0.03420003;fill-rule:evenodd;stroke-width:3pt"
           id="path1189"
           d="m 4.319,290.4 c -1.269,0 -2.327,0.7 -2.327,1.6 0,0.9 1.058,1.6 2.327,1.6 H 43.68 c 1.27,0 2.32,-0.7 2.32,-1.6 0,-0.9 -1.05,-1.6 -2.32,-1.6 z"
           inkscape:connector-curvature="0" />
        <rect
           style="color:#000000;display:inline;overflow:visible;visibility:visible;fill:url(#linearGradient2898);fill-opacity:1;fill-rule:nonzero;stroke:#888a85;stroke-width:1;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none"
           id="rect1324"
           y="257.5"
           x="2.507"
           ry="0.77630001"
           rx="0.77630001"
           height="35.009998"
           width="42.990002" />
        <path
           style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:0.30290001;fill:#555753;fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:1;marker:none;enable-background:accumulate"
           id="path5613"
           d="m 6.471,259.1 c -1.366,0 -2.469,1.1 -2.469,2.4 0,0.8 0.403,1.6 1,2 -0.612,0.5 -1,1.2 -1,2 0,0.8 0.388,1.5 1,2 -0.612,0.5 -1,1.2 -1,2 0,0.8 0.388,1.5 1,2 -0.612,0.5 -1,1.2 -1,2 0,0.8 0.388,1.5 1,2 -0.612,0.5 -1,1.2 -1,2 0,0.8 0.388,1.5 1,2 -0.612,0.5 -1,1.2 -1,2 0,0.8 0.388,1.5 1,2 -0.612,0.5 -1,1.2 -1,2 0,0.6 0.19,1.1 0.5,1.5 -0.32,0.4 -0.5,0.9 -0.5,1.5 0,1.4 1.103,2.5 2.469,2.5 0.799,0 1.516,-0.4 1.969,-1 0.453,0.6 1.145,1 1.94,1 0.79,0 1.48,-0.4 1.93,-1 0.46,0.6 1.17,1 1.97,1 0.8,0 1.52,-0.4 1.97,-1 0.45,0.6 1.15,1 1.94,1 0.79,0 1.48,-0.4 1.93,-1 0.46,0.6 1.17,1 1.97,1 0.8,0 1.49,-0.4 1.94,-1 0.45,0.6 1.14,1 1.94,1 0.8,0 1.51,-0.4 1.97,-1 0.45,0.6 1.14,1 1.93,1 0.8,0 1.52,-0.4 1.97,-1 0.46,0.6 1.14,1 1.94,1 0.8,0 1.52,-0.4 1.97,-1 0.45,0.6 1.15,1 1.94,1 0.79,0 1.48,-0.4 1.94,-1 0.45,0.6 1.16,1 1.96,1 1.37,0 2.47,-1.1 2.47,-2.5 0,-0.6 -0.19,-1.1 -0.53,-1.5 0.3,-0.4 0.47,-1 0.47,-1.5 0,-0.8 -0.39,-1.5 -1,-2 0.61,-0.5 1,-1.2 1,-2 0,-0.8 -0.39,-1.5 -1,-2 0.61,-0.5 1,-1.2 1,-2 0,-0.8 -0.39,-1.5 -1,-2 0.61,-0.5 1,-1.2 1,-2 0,-0.8 -0.39,-1.5 -1,-2 0.61,-0.5 1,-1.2 1,-2 0,-0.8 -0.39,-1.5 -1,-2 0.61,-0.5 1,-1.2 1,-2 0,-0.8 -0.38,-1.5 -0.97,-2 0.62,-0.4 1.03,-1.1 1.03,-2 0,-1.3 -1.1,-2.4 -2.47,-2.4 -0.8,0 -1.51,0.3 -1.96,0.9 -0.46,-0.5 -1.15,-0.9 -1.94,-0.9 -0.79,0 -1.49,0.4 -1.94,0.9 -0.45,-0.6 -1.17,-0.9 -1.97,-0.9 -0.8,0 -1.48,0.3 -1.94,0.9 -0.45,-0.6 -1.17,-0.9 -1.97,-0.9 -0.79,0 -1.48,0.3 -1.93,0.9 -0.46,-0.6 -1.17,-0.9 -1.97,-0.9 -0.8,0 -1.49,0.3 -1.94,0.9 -0.45,-0.6 -1.14,-0.9 -1.94,-0.9 -0.8,0 -1.51,0.3 -1.97,0.9 -0.45,-0.6 -1.14,-0.9 -1.93,-0.9 -0.79,0 -1.49,0.4 -1.94,0.9 -0.45,-0.6 -1.17,-0.9 -1.97,-0.9 -0.8,0 -1.51,0.3 -1.97,0.9 -0.45,-0.5 -1.14,-0.9 -1.93,-0.9 -0.795,0 -1.487,0.4 -1.94,0.9 -0.453,-0.6 -1.17,-0.9 -1.969,-0.9 z M 8.44,263 c 0.453,0.6 1.145,1 1.94,1 0.79,0 1.48,-0.4 1.93,-1 0.46,0.6 1.17,1 1.97,1 0.8,0 1.52,-0.4 1.97,-1 0.45,0.6 1.15,1 1.94,1 0.79,0 1.48,-0.4 1.93,-1 0.46,0.6 1.17,1 1.97,1 0.8,0 1.49,-0.4 1.94,-1 0.45,0.6 1.14,1 1.94,1 0.8,0 1.51,-0.4 1.97,-1 0.45,0.6 1.14,1 1.93,1 0.8,0 1.52,-0.4 1.97,-1 0.46,0.6 1.14,1 1.94,1 0.8,0 1.52,-0.4 1.97,-1 0.45,0.6 1.15,1 1.94,1 0.79,0 1.48,-0.4 1.94,-1 0.12,0.2 0.27,0.3 0.43,0.5 -0.64,0.4 -1.06,1.1 -1.06,2 0,0.8 0.39,1.5 1,2 -0.61,0.5 -1,1.2 -1,2 0,0.8 0.39,1.5 1,2 -0.61,0.5 -1,1.2 -1,2 0,0.8 0.39,1.5 1,2 -0.61,0.5 -1,1.2 -1,2 0,0.8 0.39,1.5 1,2 -0.61,0.5 -1,1.2 -1,2 0,0.8 0.39,1.5 1,2 -0.61,0.5 -1,1.2 -1,2 0,0.4 0.1,0.8 0.25,1.1 -0.42,-0.4 -0.97,-0.6 -1.56,-0.6 -0.79,0 -1.49,0.4 -1.94,1 -0.45,-0.6 -1.17,-1 -1.97,-1 -0.8,0 -1.48,0.4 -1.94,1 -0.45,-0.6 -1.17,-1 -1.97,-1 -0.79,0 -1.48,0.4 -1.93,1 -0.46,-0.6 -1.17,-1 -1.97,-1 -0.8,0 -1.49,0.4 -1.94,1 -0.45,-0.6 -1.14,-1 -1.94,-1 -0.8,0 -1.51,0.4 -1.97,1 -0.45,-0.6 -1.14,-1 -1.93,-1 -0.79,0 -1.49,0.4 -1.94,1 -0.45,-0.6 -1.17,-1 -1.97,-1 -0.8,0 -1.51,0.4 -1.97,1 -0.45,-0.6 -1.14,-1 -1.93,-1 -0.7,0 -1.306,0.3 -1.753,0.8 0.234,-0.4 0.375,-0.8 0.375,-1.3 0,-0.8 -0.388,-1.5 -1,-2 0.612,-0.5 1,-1.2 1,-2 0,-0.8 -0.388,-1.5 -1,-2 0.612,-0.5 1,-1.2 1,-2 0,-0.8 -0.388,-1.5 -1,-2 0.612,-0.5 1,-1.2 1,-2 0,-0.8 -0.388,-1.5 -1,-2 0.612,-0.5 1,-1.2 1,-2 0,-0.8 -0.388,-1.5 -1,-2 0.612,-0.5 1,-1.2 1,-2 0,-0.8 -0.408,-1.5 -1.031,-2 0.178,-0.1 0.333,-0.3 0.469,-0.5 z"
           inkscape:connector-curvature="0" />
        <rect
           style="color:#000000;display:inline;overflow:visible;visibility:visible;fill:url(#linearGradient2894);fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:0.2;marker:none;enable-background:accumulate"
           id="rect5580"
           y="261"
           x="6.0019999"
           ry="0.77630001"
           rx="0.77630001"
           height="28"
           width="36" />
        <path
           style="fill:none;stroke:#204a87;stroke-width:0.2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none"
           id="path1332"
           d="m 11.16,279.7 c 0,0.2 0,0.5 0,0.5 0,0 0.1,-0.5 -0.16,-0.6 -0.44,-0.1 -0.71,0.5 -0.85,1 -0.233,0.6 -0.1,1.3 0.58,1.5 0.74,0.2 1.95,-0.7 1.95,-0.7 0,0 -0.14,0.1 -0.18,0.1 -0.1,0.2 -0.14,0.3 -0.1,0.4 0,0 0.13,0 0.18,0 0.21,0.1 0.48,0 0.53,-0.2 0.1,-0.2 0.18,-0.5 0.18,-0.5 0,0 -0.1,0.2 -0.13,0.2 -0.1,0.2 0.12,0.3 0.3,0.4 0.3,0.1 0.7,-0.4 0.75,-0.4 0,0 0.1,0.3 0.25,0.4 0.22,0 0.49,-0.2 0.58,-0.2 0,0 0.1,0.1 0.13,0.1 0.26,0.1 0.66,-0.1 0.7,-0.1 0,0 0.1,0.6 0.1,1.1"
           inkscape:connector-curvature="0" />
        <path
           style="fill:none;stroke:#204a87;stroke-width:0.2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none"
           id="path1334"
           d="m 17.16,280.1 c -0.19,0.5 -0.15,0.9 -0.29,1.4"
           inkscape:connector-curvature="0" />
        <path
           style="fill:none;stroke:#204a87;stroke-width:0.2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none"
           id="path1336"
           d="m 17.02,280.3 c 0,0.1 -0.1,0.2 -0.18,0.2 0.49,-0.1 1.19,-0.3 1.24,-0.2 0,0 0,0.1 0,0.1 -0.1,0.4 -0.49,0.8 -0.49,0.8 0,0.1 1.32,-0.2 1.44,-0.1 0.1,0 0,0.2 0,0.3 -0.1,0.2 -0.18,0.3 -0.32,0.4 0.1,0 0.1,0 0.14,0"
           inkscape:connector-curvature="0" />
        <path
           style="fill:none;stroke:#204a87;stroke-width:0.2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none"
           id="path1338"
           d="m 19.73,280.7 c 0,0.1 0,0.1 -0.1,0.1 0.35,0.1 0.53,-0.1 0.88,-0.1 -0.1,0.3 -0.14,0.5 -0.27,0.8 0.35,-0.4 0.93,-0.8 1.02,-0.8 0.1,0 0,0.8 0,0.8 0.17,0.1 1.59,-1.1 1.73,-1 0.1,0 -0.33,1 0.15,1.2 0.39,0.1 0.93,-0.5 0.93,-0.5 0,0 -0.18,0.2 -0.27,0.3 0,0 0.1,0 0.13,0 0.35,0 0.53,-0.2 0.88,-0.2 0,0.1 -0.1,0.1 -0.1,0.2 0.1,0 0.1,0 0.17,0.1 0.31,0.1 0.88,-0.2 0.88,-0.2 0,0 -0.1,0.3 0,0.3 0.61,0.2 1.01,-0.1 1.63,-0.2"
           inkscape:connector-curvature="0" />
        <path
           style="fill:#3465a4;fill-rule:evenodd;stroke:#204a87;stroke-width:1;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
           id="path5582"
           d="m 34.38,281.1 c 0.1,2.2 0.76,4.3 1.77,6.3 l 0.8,-3.3 3.27,0.7 c -1.17,-0.7 -3.54,-2.9 -4.51,-4.5 z"
           inkscape:connector-curvature="0" />
        <path
           style="color:#000000;display:inline;overflow:visible;visibility:visible;fill:#f7f999;fill-opacity:1;fill-rule:nonzero;stroke:#fdcd12;stroke-width:48.8927002;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none"
           id="path1342"
           transform="matrix(0.0195,0,0,0.0196,73.96,310.8)"
           d="m -1933,-1441 c -21,19 -47,1 -73,11 -26,11 -32,42 -60,43 -28,0 -37,-30 -63,-39 -27,-8 -52,11 -74,-6 -22,-17 -9,-46 -23,-70 -15,-24 -47,-25 -52,-52 -6,-27 22,-41 26,-69 4,-27 -19,-48 -6,-73 13,-25 44,-17 65,-35 21,-19 16,-50 42,-61 26,-10 44,16 72,15 28,-1 45,-28 71,-19 27,8 24,40 47,57 21,17 51,8 66,31 15,24 -7,47 -1,74 5,27 35,39 31,67 -4,27 -36,31 -49,55 -13,25 2,53 -19,71 z"
           inkscape:connector-curvature="0" />
        <path
           style="color:#000000;display:inline;overflow:visible;visibility:visible;fill:#fdce15;fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:6.25;marker:none"
           id="path1346"
           transform="matrix(0.0858,0,0,0.0863,116.7,373.7)"
           d="m -941.1,-1093 a 29.98,29.07 0 1 1 -59.9,0 29.98,29.07 0 1 1 59.9,0 z"
           inkscape:connector-curvature="0" />
        <path
           style="color:#000000;display:inline;overflow:visible;visibility:visible;fill:none;stroke:#ffffff;stroke-width:0.55790001;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none"
           id="path1348"
           d="m 34.4,278 c -0.1,0 -0.18,-0.1 -0.28,-0.1 -0.26,-0.1 -0.54,-0.2 -0.83,-0.2 0,0 0,0 0,0 -1.16,0 -2.08,1.5 -1.42,2.5 0.1,0.1 0.14,0.1 0.22,0.3"
           inkscape:connector-curvature="0" />
        <rect
           style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:0.69709999;fill:none;stroke:url(#linearGradient2880);stroke-width:1;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none"
           id="rect2231"
           y="258.5"
           x="3.5020001"
           ry="0"
           rx="0"
           height="32.98"
           width="41" />
        <path
           style="fill:none;stroke:#888a85;stroke-width:2;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
           id="path2235"
           d="m 10.15,264 h 28.7"
           inkscape:connector-curvature="0" />
        <path
           style="fill:none;stroke:#888a85;stroke-width:1px;stroke-linecap:round;stroke-linejoin:miter;stroke-opacity:1"
           id="path2237"
           d="M 15.39,267.5 H 33.41"
           inkscape:connector-curvature="0" />
        <path
           style="fill:none;stroke:#888a85;stroke-width:1px;stroke-linecap:round;stroke-linejoin:miter;stroke-opacity:1"
           id="path2239"
           d="M 12.34,273.5 H 24.66"
           inkscape:connector-curvature="0" />
        <path
           style="fill:none;stroke:#888a85;stroke-width:1px;stroke-linecap:round;stroke-linejoin:miter;stroke-opacity:1"
           id="path2241"
           d="M 12.5,276.5 H 24.82"
           inkscape:connector-curvature="0" />
        <path
           style="fill:none;stroke:#888a85;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
           id="path5578"
           d="M 11,283.4 H 26"
           inkscape:connector-curvature="0" />
      </g>
      <g
         style="display:inline"
         id="layer4"
         transform="translate(0,-252)" />
    </g>
    <g
       id="g4624"
       transform="matrix(2.1828125,0,0,2.0607253,-451.41828,56.638936)">
      <g
         style="display:inline"
         inkscape:label="Layer 1"
         id="layer1-7">
        <ellipse
           transform="matrix(0.968245,0,0,0.863797,1.447631,-0.747418)"
           id="path1379"
           style="opacity:0.2;fill:url(#radialGradient4337);fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
           cx="23.9375"
           cy="42.6875"
           rx="23.75956"
           ry="5.8835783" />
        <g
           transform="translate(-25)"
           id="g4339">
          <path
             sodipodi:nodetypes="czz"
             id="use4184"
             d="M 66.13085,41.787912 C 57.847275,31.229854 73.566661,39.498791 71.897463,35.839689 70.26171,32.253903 51.343195,36.513864 53.3325,30.513864"
             style="display:inline;opacity:0.20786516;fill:none;fill-opacity:1;fill-rule:evenodd;stroke:url(#linearGradient4343);stroke-width:1;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
             inkscape:connector-curvature="0" />
          <path
             style="display:inline;fill:none;fill-opacity:1;fill-rule:evenodd;stroke:#729fcf;stroke-width:1.00000012;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
             d="M 65.675044,41.213388 C 57.656634,30.125 72.913568,39.404152 71.161327,35 69.172021,30 52.551603,36.027728 53.480248,30.116116"
             id="path4172"
             sodipodi:nodetypes="czz"
             inkscape:connector-curvature="0" />
        </g>
        <g
           transform="matrix(0.811017,0,0,0.811017,4.536063,4.144784)"
           id="g2302">
          <path
             sodipodi:nodetypes="csccscc"
             id="path1359"
             d="m 14.375479,32.558794 c 0,0 1.216876,4.898976 -3.856329,4.944966 -2.4302757,0.02175 -1.9324777,4.006021 -1.9324777,4.006021 l 30.8464667,-0.03115 c 0,0 0.418438,-3.867241 -2.022217,-3.912581 -4.987467,-0.09147 -3.810529,-5.06955 -3.810529,-5.06955 z"
             style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#linearGradient2308);fill-opacity:1;fill-rule:evenodd;stroke:#888a85;stroke-width:1.23301888;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
             inkscape:connector-curvature="0" />
          <path
             sodipodi:nodetypes="ccccc"
             id="path1367"
             d="m 13.926195,33.027451 c 0.08401,2.172549 -0.28454,2.911443 -1.640464,3.675231 L 36,38 c -0.952992,-1.168628 -2.339163,-2.933334 -1.961117,-4.988235 z"
             style="opacity:0.5;fill:url(#linearGradient2310);fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
             inkscape:connector-curvature="0" />
          <path
             style="fill:none;fill-opacity:1;fill-rule:evenodd;stroke:#8d8d8f;stroke-width:1.23301923px;stroke-linecap:round;stroke-linejoin:miter;stroke-opacity:0.43902438"
             d="m 10.436202,38.661601 c 6.564263,0.002 27.126435,0 27.126435,0"
             id="path1393"
             sodipodi:nodetypes="cc"
             inkscape:connector-curvature="0" />
          <path
             sodipodi:nodetypes="cc"
             id="path1395"
             d="m 9.647928,39.660187 c 6.706693,0.002 28.719861,0 28.719861,0"
             style="fill:none;fill-opacity:1;fill-rule:evenodd;stroke:#ffffff;stroke-width:1.23301911px;stroke-linecap:round;stroke-linejoin:miter;stroke-opacity:1"
             inkscape:connector-curvature="0" />
        </g>
        <rect
           y="-2.84375"
           x="0"
           height="48"
           width="48"
           id="rect4784"
           style="fill:none;fill-opacity:0.75;fill-rule:evenodd;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" />
        <path
           sodipodi:nodetypes="ccccccccc"
           id="rect5040"
           d="M 4.8882799,0.5019965 H 42.990539 c 2.023707,0 3.498537,1.4255519 3.498537,3.6208005 l 0.01094,25.165237 C 46.500019,30.977609 45.97204,31.5 44.466781,31.5 L 3.5326624,31.481093 C 2.3542134,31.452343 1.5154744,30.987161 1.4996519,29.464764 L 1.5148181,3.935329 c 0,-1.7712136 1.5383348,-3.4333325 3.3734618,-3.4333325 z"
           style="fill:url(#linearGradient5147);fill-opacity:1;fill-rule:evenodd;stroke:#888a85;stroke-width:1.00000036;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:1"
           inkscape:connector-curvature="0" />
        <rect
           y="4.5"
           x="5.5"
           height="23"
           width="37"
           id="rect9208"
           style="fill:url(#radialGradient5239);fill-opacity:1;fill-rule:evenodd;stroke:#204a87;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" />
        <path
           sodipodi:nodetypes="ccccc"
           id="path4073"
           d="M 6,4.9921565 V 23 C 22.444445,21.645751 28.598693,12.887581 41.968627,11.972548 l 1e-6,-7.011764 z"
           style="opacity:0.75;fill:url(#linearGradient6246);fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
           inkscape:connector-curvature="0" />
        <rect
           style="fill:none;fill-opacity:0.75;fill-rule:evenodd;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
           id="rect1355"
           width="48"
           height="48"
           x="0"
           y="-2.84375" />
        <rect
           y="-2.84375"
           x="0"
           height="48"
           width="48"
           id="rect1377"
           style="fill:none;fill-opacity:0.75;fill-rule:evenodd;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1" />
        <path
           sodipodi:nodetypes="cccsccscccc"
           id="path1387"
           d="m 4.9838392,1.4949272 c -1.2994875,0 -2.4838385,1.2815188 -2.4838385,2.5365102 L 2.5,29.355478 c 0.00622,0.597159 0.1447546,0.782572 0.2794318,0.897058 0.1346773,0.114486 0.419191,0.204825 0.9003915,0.216531 L 44.352677,30.5 c 0.646964,0 0.851442,-0.1 0.931439,-0.185598 0.08,-0.0856 0.217336,-0.396637 0.217336,-1.144523 l 10e-7,-24.9528433 c 0,-1.7888213 -1.004252,-2.7221085 -2.60803,-2.7221085 z"
           style="fill:none;fill-opacity:1;fill-rule:evenodd;stroke:#ffffff;stroke-width:0.99999958;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:1"
           inkscape:connector-curvature="0" />
        <g
           transform="translate(0,13)"
           id="g3052"
           style="opacity:0.4">
          <rect
             y="30.5"
             x="0"
             height="4.5"
             width="4"
             id="rect3026"
             style="opacity:1;fill:url(#radialGradient2517);fill-opacity:1;stroke:none;stroke-width:1;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
          <rect
             transform="scale(-1)"
             y="-35"
             x="-48"
             height="4.5"
             width="4"
             id="rect3036"
             style="display:inline;opacity:1;fill:url(#radialGradient2519);fill-opacity:1;stroke:none;stroke-width:1;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
          <rect
             y="30.5"
             x="4"
             height="4.5"
             width="40"
             id="rect3040"
             style="opacity:1;fill:url(#linearGradient2521);fill-opacity:1;stroke:none;stroke-width:1;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
        </g>
        <g
           transform="translate(48)"
           id="g3316">
          <path
             sodipodi:nodetypes="ccccccccc"
             id="rect2024"
             d="m -42.6875,40.49166 c -0.967886,0 -1.847763,0.285348 -2.4375,1.247229 l -1.8125,2.918046 c -0.808653,1.054721 -0.156385,1.859077 2.59375,1.859077 h 40.4375 c 2.750135,0 3.402403,-0.804356 2.59375,-1.859077 L -3.125,41.738889 C -3.714737,40.777008 -4.594614,40.49166 -5.5625,40.49166 Z"
             style="fill:url(#linearGradient3442);fill-opacity:1;stroke:#888a85;stroke-width:1.00000417;stroke-linejoin:round;stroke-miterlimit:4;stroke-opacity:1"
             inkscape:connector-curvature="0" />
          <path
             d="m 5.3125,38.5 c -0.7068869,0 -1.0666848,0.07419 -1.5292969,1.076172 a 1.0001,1.0001 0 0 1 -0.00195,0.0039 l -1.8125,3.875 a 1.0001,1.0001 0 0 1 -0.041016,0.07617 c -0.1455375,0.252075 -0.1851561,0.437195 -0.1875,0.509766 -0.00234,0.07257 -0.01383,0.03574 0.021484,0.07422 C 1.8323476,44.192192 2.4106476,44.5 3.65625,44.5 h 40.4375 c 1.245602,0 1.823902,-0.307808 1.894531,-0.384766 0.03531,-0.03848 0.02383,-0.0016 0.02148,-0.07422 -0.0023,-0.07257 -0.04196,-0.257691 -0.1875,-0.509766 a 1.0001,1.0001 0 0 1 -0.04102,-0.07617 l -1.8125,-3.875 a 1.0001,1.0001 0 0 1 -0.002,-0.0039 C 43.504185,38.57419 43.144387,38.5 42.4375,38.5 Z"
             id="path2920"
             style="display:inline;fill:none;fill-opacity:0.46875;stroke:#ffffff;stroke-width:1.22474873;stroke-linejoin:round;stroke-miterlimit:4;stroke-opacity:1"
             inkscape:original="M 5.3125 37.5 C 4.344614 37.5 3.4647369 37.878926 2.875 39.15625 L 1.0625 43.03125 C 0.253847 44.43186 0.90611493 45.5 3.65625 45.5 L 44.09375 45.5 C 46.843885 45.5 47.496153 44.43186 46.6875 43.03125 L 44.875 39.15625 C 44.285263 37.878926 43.405386 37.5 42.4375 37.5 L 5.3125 37.5 z "
             inkscape:radius="-1"
             sodipodi:type="inkscape:offset"
             transform="matrix(1,0,0,0.666668,-48,15.83327)" />
        </g>
        <g
           id="g2299"
           inkscape:label="Shadow"
           transform="translate(-102,13.18088)" />
        <g
           transform="translate(-102,28.24691)"
           inkscape:label="Shadow"
           id="g2301" />
        <g
           transform="translate(-102,28.24691)"
           style="display:inline"
           inkscape:label="Lavoro"
           id="g2303" />
        <g
           transform="translate(48)"
           id="g3252">
          <path
             sodipodi:nodetypes="csssssssccssssssssssssccccssscccccccccssssssssssssccsssssssssccccccc"
             id="path2308"
             d="m -41.565234,39.500963 c -0.003,0.0031 0.0028,0.02359 0,0.026 -0.01467,0.0041 -0.05022,0.02021 -0.06523,0.02601 -0.005,0.0021 -0.02762,-0.0023 -0.03262,0 -0.005,0.0025 -0.02769,0.0233 -0.03262,0.026 -0.0097,0.0058 -0.02321,0.01943 -0.03262,0.026 -0.0046,0.0035 -0.02815,0.02233 -0.03262,0.02601 -0.0043,0.0039 -0.02844,0.02193 -0.03262,0.02601 -0.06487,0.075 -0.133186,0.205396 -0.228319,0.364081 l -2.22755,3.952895 c 0,0.0049 -2.28e-4,0.02105 0,0.026 4.53e-4,0.005 -6.74e-4,0.02104 0,0.026 8.91e-4,0.005 -0.0011,0.02106 0,0.02601 0.0026,0.0099 0.02917,0.04222 0.03262,0.05201 0.0019,0.0049 -0.0021,0.02117 0,0.02601 0.0069,0.01439 0.02403,0.03804 0.03262,0.05201 0.0091,0.0138 0.02197,0.03881 0.03262,0.05201 0.0037,0.0043 0.02875,0.02176 0.03262,0.026 0.004,0.0042 0.02844,0.02194 0.03262,0.02601 0.02595,0.02385 0.06703,0.05818 0.09785,0.07802 0.0053,0.0032 0.02724,0.02296 0.03262,0.02601 0.0165,0.0079 0.04736,0.01884 0.06523,0.026 0.120228,0.04402 0.279283,0.07802 0.42402,0.07802 h 23.114643 l -0.09785,-4.993121 -20.789242,-6e-6 c -0.06851,0 -0.143762,-0.0019 -0.195701,0 -0.01215,5.7e-5 -0.05203,5.11e-4 -0.06523,0 -0.004,2.5e-5 -0.02896,-2.14e-4 -0.03262,0 -0.0028,0.0024 -0.02957,-0.0031 -0.03262,0 z m 23.039809,6e-6 0.09785,1.664375 h 5.642726 l -0.260935,-1.664375 z m 7.436655,0 1.17421,4.993121 h 5.218705 c 0.144736,0 0.303792,-0.034 0.42402,-0.07802 0.017877,-0.0072 0.048735,-0.0181 0.065233,-0.026 0.00538,-0.003 0.027357,-0.02283 0.032617,-0.02601 0.030821,-0.01983 0.071897,-0.05416 0.097851,-0.07802 0.00418,-0.0041 0.028592,-0.02185 0.032616,-0.02601 0.00387,-0.0042 0.028906,-0.02168 0.032618,-0.026 0.010648,-0.0132 0.023496,-0.03821 0.032617,-0.05201 0.00859,-0.01397 0.025688,-0.03762 0.032617,-0.05201 0.00212,-0.0048 -0.00192,-0.02114 0,-0.02601 0.00345,-0.0098 0.029987,-0.04214 0.032617,-0.05201 0.0011,-0.005 -8.92e-4,-0.02105 0,-0.02601 6.73e-4,-0.005 -4.54e-4,-0.02104 0,-0.026 2.29e-4,-0.005 0,-0.02106 0,-0.026 l -1.826015,-3.9529 C -5.834197,39.862396 -5.902512,39.732002 -5.967383,39.656999 -5.971557,39.652925 -5.99567,39.634868 -6,39.630993 c -0.00447,-0.0037 -0.028021,-0.02252 -0.032617,-0.02601 -0.00941,-0.0066 -0.022876,-0.02021 -0.032617,-0.026 -0.00493,-0.0027 -0.027643,-0.0235 -0.032616,-0.026 -0.005,-0.0023 -0.027608,0.0021 -0.032617,0 -0.015014,-0.0058 -0.050564,-0.02192 -0.065234,-0.02601 -0.016117,-0.0031 -0.041567,-0.02056 -0.065235,-0.026 -0.011875,-0.0025 -0.054353,0.0014 -0.065233,0 -0.05194,-0.0019 -0.127187,0 -0.195701,0 z m -6.131978,3.328747 -1.043741,1.664374 h 6.001511 l -1.304677,-1.664374 z"
             style="color:#000000;display:inline;overflow:visible;visibility:visible;fill:url(#linearGradient3320);fill-opacity:1;fill-rule:nonzero;stroke:#babdb6;stroke-width:0.99999923;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
             inkscape:connector-curvature="0" />
          <g
             transform="translate(-48,-0.00589)"
             style="fill:url(#radialGradient2433);fill-opacity:1"
             id="g2310">
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3322);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2312"
               width="1"
               height="1"
               x="7"
               y="39"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3324);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2314"
               width="1"
               height="1"
               x="9"
               y="39"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3326);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2316"
               width="1"
               height="1"
               x="11"
               y="39"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3328);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2318"
               width="1"
               height="1"
               x="13"
               y="39"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3330);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2320"
               width="1"
               height="1"
               x="15"
               y="39"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3332);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2322"
               width="1"
               height="1"
               x="17"
               y="39"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3334);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2324"
               width="1"
               height="1"
               x="19"
               y="39"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3336);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2326"
               width="1"
               height="1"
               x="21"
               y="39"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3338);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2328"
               width="1"
               height="1"
               x="6"
               y="40"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3340);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2330"
               width="1"
               height="1"
               x="8"
               y="40"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3342);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2332"
               width="1"
               height="1"
               x="10"
               y="40"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3344);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2334"
               width="1"
               height="1"
               x="12"
               y="40"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3346);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2336"
               width="1"
               height="1"
               x="14"
               y="40"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3348);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2338"
               width="1"
               height="1"
               x="16"
               y="40"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3350);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2341"
               width="1"
               height="1"
               x="18"
               y="40"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3352);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2343"
               width="1"
               height="1"
               x="20"
               y="40"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3354);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2345"
               width="1"
               height="1"
               x="7"
               y="41"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3356);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2347"
               width="1"
               height="1"
               x="9"
               y="41"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3358);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2349"
               width="1"
               height="1"
               x="11"
               y="41"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3360);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2351"
               width="1"
               height="1"
               x="13"
               y="41"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3362);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2353"
               width="1"
               height="1"
               x="15"
               y="41"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3364);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2355"
               width="1"
               height="1"
               x="17"
               y="41"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3366);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2357"
               width="1"
               height="1"
               x="19"
               y="41"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3368);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2359"
               width="1"
               height="1"
               x="21"
               y="41"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3370);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2361"
               width="1"
               height="1"
               x="6"
               y="42"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3372);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2363"
               width="1"
               height="1"
               x="8"
               y="42"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3374);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2365"
               width="1"
               height="1"
               x="10"
               y="42"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3376);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2367"
               width="1"
               height="1"
               x="12"
               y="42"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3378);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2369"
               width="1"
               height="1"
               x="14"
               y="42"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3380);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2371"
               width="1"
               height="1"
               x="16"
               y="42"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3382);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2373"
               width="1"
               height="1"
               x="18"
               y="42"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3384);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2375"
               width="1"
               height="1"
               x="20"
               y="42"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3386);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2377"
               width="1"
               height="1"
               x="5"
               y="43"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3388);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2379"
               width="1"
               height="1"
               x="7"
               y="43"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3390);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2381"
               width="1"
               height="1"
               x="22"
               y="42"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3392);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2383"
               width="1"
               height="1"
               x="23"
               y="41"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3394);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2385"
               width="1"
               height="1"
               x="22"
               y="40"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3396);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2387"
               width="1"
               height="1"
               x="23"
               y="39"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3398);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2389"
               width="1"
               height="1"
               x="24"
               y="40"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3400);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2391"
               width="1"
               height="1"
               x="25"
               y="39"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3402);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2393"
               width="1"
               height="1"
               x="26"
               y="40"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3404);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2395"
               width="1"
               height="1"
               x="25"
               y="41"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3406);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2397"
               width="1"
               height="1"
               x="24"
               y="42"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3408);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2399"
               width="1"
               height="1"
               x="26"
               y="42"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3410);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2401"
               width="1"
               height="1"
               x="25"
               y="43"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3412);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2403"
               width="1"
               height="1"
               x="23"
               y="43"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3414);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2405"
               width="1"
               height="1"
               x="31"
               y="43"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3416);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2407"
               width="1"
               height="1"
               x="33"
               y="43"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3418);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2409"
               width="1"
               height="1"
               x="31"
               y="39"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3420);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2411"
               width="1"
               height="1"
               x="33"
               y="39"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3422);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2413"
               width="1"
               height="1"
               x="37"
               y="39"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3424);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2415"
               width="1"
               height="1"
               x="38"
               y="40"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3426);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2417"
               width="1"
               height="1"
               x="39"
               y="39"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3428);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2419"
               width="1"
               height="1"
               x="41"
               y="41"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3430);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2421"
               width="1"
               height="1"
               x="39"
               y="41"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3432);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2423"
               width="1"
               height="1"
               x="38"
               y="42"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3434);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2425"
               width="1"
               height="1"
               x="40"
               y="42"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3436);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2427"
               width="1"
               height="1"
               x="41"
               y="43"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3438);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2429"
               width="1"
               height="1"
               x="40"
               y="40"
               rx="0.26516503"
               ry="0.26516503" />
            <rect
               style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#radialGradient3440);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
               id="rect2431"
               width="1"
               height="1"
               x="43"
               y="43"
               rx="0.26516503"
               ry="0.26516503" />
          </g>
        </g>
      </g>
      <g
         style="display:inline"
         inkscape:label="tastiera"
         id="layer3">
        <g
           transform="translate(-54,0.18088)"
           inkscape:label="Shadow"
           id="g3270" />
        <g
           id="g2611"
           inkscape:label="Shadow"
           transform="translate(-54,15.24691)" />
        <g
           id="g2613"
           inkscape:label="Lavoro"
           style="display:inline"
           transform="translate(-54,15.24691)" />
      </g>
      <g
         style="display:inline"
         inkscape:label="tasti"
         id="layer2-5" />
    </g>
    <g
       inkscape:label="Layer 1"
       id="layer1-3"
       transform="matrix(2.276996,0,0,2.3809556,594.07096,46.029809)">
      <g
         style="display:inline;opacity:0.6"
         transform="matrix(0.01477517,0,0,0.02195012,36.14156,38.80781)"
         id="g3952-7">
        <rect
           style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:0.40206185;fill:url(#linearGradient5270);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
           id="rect3954-5"
           width="1339.6335"
           height="478.35718"
           x="-1559.2523"
           y="-150.69685" />
        <path
           style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:0.40206185;fill:url(#radialGradient6717-6);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
           d="m -219.61876,-150.68038 c 0,0 0,478.33079 0,478.33079 142.874166,0.90045 345.40022,-107.16966 345.40014,-239.196175 0,-132.026537 -159.436816,-239.134595 -345.40014,-239.134615 z"
           id="path3956-9"
           sodipodi:nodetypes="cccc"
           inkscape:connector-curvature="0" />
        <path
           sodipodi:nodetypes="cccc"
           id="path3958-2"
           d="m -1559.2523,-150.68038 c 0,0 0,478.33079 0,478.33079 -142.8742,0.90045 -345.4002,-107.16966 -345.4002,-239.196175 0,-132.026537 159.4368,-239.134595 345.4002,-239.134615 z"
           style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:0.40206185;fill:url(#radialGradient6719-0);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
           inkscape:connector-curvature="0" />
      </g>
      <path
         sodipodi:nodetypes="cccc"
         id="path6845-2"
         d="M 46.5,25.5 H 36.499996 v 1.999997 H 46.5"
         style="fill:url(#linearGradient5272);fill-opacity:1;stroke:url(#linearGradient5274);stroke-width:1;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
         inkscape:connector-curvature="0" />
      <circle
         transform="matrix(0.761905,0,0,0.761905,7.388392,5.500001)"
         id="path6851-8"
         style="opacity:1;fill:#d0d0ca;fill-opacity:1;stroke:#729fcf;stroke-width:1.31249964;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
         cx="35.625"
         cy="27.5625"
         r="2.625" />
      <path
         sodipodi:nodetypes="cccc"
         id="rect6804-9"
         d="M 1.4999956,25.5 H 11.5 v 1.999997 H 1.4999956"
         style="fill:url(#linearGradient5276);fill-opacity:1;stroke:url(#linearGradient5278);stroke-width:1.00000036;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-opacity:1"
         inkscape:connector-curvature="0" />
      <path
         sodipodi:nodetypes="ccccccccccccc"
         id="path2371-7"
         d="m 18.125,2.5 c -0.744057,0 -1.39865,0.3133048 -1.875,0.8125 l -4.21875,5.25 C 11.703624,9.146009 11.5,9.811876 11.5,10.53125 v 28.9375 c 0,2.233312 1.797934,4.031249 4.03125,4.03125 h 14.9375 C 32.70207,43.499999 34.5,41.702062 34.5,39.46875 V 10.53125 C 34.5,9.811876 34.29638,9.146009 33.96875,8.5625 L 29.75,3.3125 C 29.27365,2.8133048 28.61906,2.5 27.875,2.5 Z"
         style="color:#000000;display:inline;overflow:visible;visibility:visible;fill:url(#linearGradient5280);fill-opacity:1;fill-rule:nonzero;stroke:#888a85;stroke-width:1;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:1.39999998;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
         inkscape:connector-curvature="0" />
      <path
         transform="translate(-40,-1)"
         d="m 58.125,4.5 c -0.459464,0 -0.839082,0.1836208 -1.142578,0.4980469 L 52.892578,10.085938 C 52.647872,10.531969 52.5,11.012564 52.5,11.53125 v 25.9375 c 0,1.69661 1.334636,3.031249 3.03125,3.03125 h 14.9375 C 72.165369,40.499999 73.5,39.165361 73.5,37.46875 v -25.9375 c 0,-0.518689 -0.147872,-0.999286 -0.392578,-1.445312 L 69.017578,4.9980469 C 68.714081,4.6836193 68.334468,4.5 67.875,4.5 Z"
         id="path4329-3"
         style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:0.7;fill:none;fill-opacity:1;fill-rule:nonzero;stroke:#f9f9f9;stroke-width:1;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:1.39999998;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
         inkscape:original="M 58.125 3.5 C 57.380943 3.5 56.72635 3.8133048 56.25 4.3125 L 52.03125 9.5625 C 51.703624 10.146009 51.5 10.811876 51.5 11.53125 L 51.5 37.46875 C 51.5 39.702062 53.297934 41.499999 55.53125 41.5 L 70.46875 41.5 C 72.70207 41.499999 74.5 39.702062 74.5 37.46875 L 74.5 11.53125 C 74.5 10.811876 74.29638 10.146009 73.96875 9.5625 L 69.75 4.3125 C 69.27365 3.8133048 68.61906 3.5 67.875 3.5 L 58.125 3.5 z "
         inkscape:radius="-1"
         sodipodi:type="inkscape:offset" />
      <path
         style="color:#000000;display:inline;overflow:visible;visibility:visible;fill:url(#linearGradient5282);fill-opacity:1;fill-rule:nonzero;stroke:#ffffff;stroke-width:1.00000012;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dashoffset:1.39999998;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
         d="M 15.528487,7.5000019 H 30.47148 c 1.677787,0 3.028495,1.3507087 3.028495,3.0284951 v 28.943041 c 0,1.677786 -1.350708,3.028494 -3.028495,3.028494 H 15.528487 c -1.677786,0 -3.028495,-1.350708 -3.028495,-3.028494 V 10.528497 c 0,-1.6777864 1.350709,-3.0284951 3.028495,-3.0284951 z"
         id="rect2377-61"
         sodipodi:nodetypes="ccccccccc"
         inkscape:connector-curvature="0" />
      <rect
         ry="0"
         rx="0"
         y="10.500004"
         x="15.500004"
         height="5.0000067"
         width="14.999999"
         id="rect4183-2"
         style="color:#000000;display:inline;overflow:visible;visibility:visible;fill:url(#linearGradient5284);fill-opacity:1;fill-rule:nonzero;stroke:#babdb6;stroke-width:1.00000048;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none" />
      <rect
         ry="0"
         rx="0"
         y="17.500006"
         x="15.500004"
         height="5.0000067"
         width="14.999999"
         id="rect4187-9"
         style="fill:url(#linearGradient5286);fill-opacity:1;stroke:#babdb6;stroke-width:1.00000048;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
      <circle
         transform="matrix(0.954546,0,0,0.954547,0.77272,3.181783)"
         id="path4191-3"
         style="fill:#d0d0ca;fill-opacity:1;stroke:#555753;stroke-width:1.04761803;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
         cx="17"
         cy="26"
         r="1.5714285" />
      <rect
         ry="2.4999907"
         rx="2.4999905"
         y="25.49999"
         x="14.500004"
         height="4.9999981"
         width="5.000001"
         id="rect4193-1"
         style="fill:none;fill-opacity:1;stroke:url(#linearGradient5288);stroke-width:1.00000036;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:1.20000052;stroke-opacity:1" />
      <rect
         ry="0"
         rx="0"
         y="26.500006"
         x="21.5"
         height="3.0000033"
         width="10.000003"
         id="rect4195-9"
         style="fill:#eeeeec;fill-opacity:1;stroke:#babdb6;stroke-width:1.00000036;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
      <ellipse
         transform="matrix(0.942803,0,0,1.077485,0.8126,-0.100478)"
         id="path4197-4"
         style="fill:#f7f7f7;fill-opacity:1;stroke:none;stroke-width:1;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
         cx="16.639107"
         cy="25.61565"
         rx="0.53033012"
         ry="0.46403882" />
      <ellipse
         transform="matrix(0.942803,0,0,1.008364,0.8126,1.670102)"
         id="path4213-7"
         style="fill:#f7f7f7;fill-opacity:1;stroke:none;stroke-width:1;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
         cx="16.639107"
         cy="25.61565"
         rx="0.53033012"
         ry="0.46403882" />
      <rect
         ry="2.0110888"
         rx="2.0110888"
         y="8.499999"
         x="13.499996"
         height="33"
         width="18.999966"
         id="rect2379-8"
         style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:0.8;fill:none;fill-opacity:1;fill-rule:nonzero;stroke:url(#linearGradient5290);stroke-width:0.99999988;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:1.39999998;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none" />
      <g
         transform="translate(-40,2)"
         style="opacity:0.19699246"
         id="g4307-4">
        <rect
           style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#linearGradient5292);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:1.39999998;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
           id="rect4239-5"
           width="1.0130863"
           height="8.000001"
           x="57"
           y="31" />
        <rect
           style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#linearGradient5294);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:1.39999998;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
           id="rect4249-0"
           width="1.0130863"
           height="8.000001"
           x="59"
           y="31" />
        <rect
           style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#linearGradient5296);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:1.39999998;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
           id="rect4253-3"
           width="1.0130863"
           height="8.000001"
           x="61"
           y="31" />
        <rect
           style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#linearGradient5298);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:1.39999998;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
           id="rect4257-6"
           width="1.0130863"
           height="8.000001"
           x="63"
           y="31" />
        <rect
           style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#linearGradient5300);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:1.39999998;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
           id="rect4261-1"
           width="1.0130863"
           height="8.000001"
           x="65"
           y="31" />
        <rect
           style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:1;fill:url(#linearGradient5302);fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:1;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:1.39999998;stroke-opacity:1;marker:none;marker-start:none;marker-mid:none;marker-end:none"
           id="rect4265-0"
           width="1.0130863"
           height="8.000001"
           x="67"
           y="31" />
      </g>
    </g>
    <g
       style="display:inline;enable-background:new"
       id="g4229-5"
       transform="matrix(1.7452102,0,0,1.8748228,334.42669,200.66637)">
      <g
         style="display:none"
         id="layer6-5"
         transform="translate(0,-252)">
        <rect
           style="display:inline;overflow:visible;visibility:visible;fill:#eeeeec;fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:2;marker:none;enable-background:accumulate"
           id="rect6284-4"
           y="50"
           x="296"
           height="48"
           width="48" />
        <rect
           style="display:inline;overflow:visible;visibility:visible;fill:#eeeeec;fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:2;marker:none;enable-background:accumulate"
           id="rect6592-7"
           y="126"
           x="303"
           height="32"
           width="32" />
        <rect
           style="display:inline;overflow:visible;visibility:visible;fill:#eeeeec;fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:2;marker:none;enable-background:accumulate"
           id="rect6749-6"
           y="177"
           x="303"
           height="22"
           width="22" />
        <rect
           style="display:inline;overflow:visible;visibility:visible;fill:#eeeeec;fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:2;marker:none;enable-background:accumulate"
           id="rect6833-5"
           y="219"
           x="303"
           height="16"
           width="16" />
        <text
           style="font-style:normal;font-weight:normal;font-size:18.30069923px;font-family:'Bitstream Vera Sans';display:inline;fill:#000000;fill-opacity:1;stroke:none;enable-background:new"
           xml:space="preserve"
           id="context-6"
           y="21.513618"
           x="20.970737"><tspan
             id="tspan2716-9"
             y="21.513618"
             x="20.970737">mimetypes</tspan></text>
        <text
           style="font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;font-size:18.30069923px;line-height:125%;font-family:'Droid Sans';-inkscape-font-specification:'Droid Sans Bold';text-align:start;writing-mode:lr-tb;text-anchor:start;display:inline;fill:#000000;fill-opacity:1;stroke:none;enable-background:new"
           xml:space="preserve"
           id="icon-name-3"
           y="21.513618"
           x="191.97073"><tspan
             id="tspan3023-7"
             y="21.513618"
             x="191.97073">application-certificate</tspan></text>
      </g>
      <g
         style="display:inline"
         id="layer2-3-4"
         transform="translate(0,-252)">
        <path
           style="opacity:0.03420003;fill-rule:evenodd;stroke-width:3pt"
           id="path1196-5"
           d="m 4.319,289 c -2.433,0 -4.3376,1.4 -4.3376,3 0,1.7 1.9046,3 4.3376,3 H 43.68 c 2.43,0 4.33,-1.3 4.33,-3 0,-1.6 -1.9,-3 -4.33,-3 z"
           inkscape:connector-curvature="0" />
        <path
           style="opacity:0.03420003;fill-rule:evenodd;stroke-width:3pt"
           id="path1195-2"
           d="m 4.319,289.2 c -2.222,0 -4.0202,1.3 -4.0202,2.8 0,1.6 1.7982,2.8 4.0202,2.8 H 43.68 c 2.22,0 4.02,-1.2 4.02,-2.8 0,-1.5 -1.8,-2.8 -4.02,-2.8 z"
           inkscape:connector-curvature="0" />
        <path
           style="opacity:0.03420003;fill-rule:evenodd;stroke-width:3pt"
           id="path1194-5"
           d="m 4.319,289.4 c -2.116,0 -3.7028,1.2 -3.7028,2.6 0,1.5 1.5868,2.6 3.7028,2.6 H 43.68 c 2.11,0 3.7,-1.1 3.7,-2.6 0,-1.4 -1.59,-2.6 -3.7,-2.6 z"
           inkscape:connector-curvature="0" />
        <path
           style="opacity:0.03420003;fill-rule:evenodd;stroke-width:3pt"
           id="path1193-4"
           d="m 4.319,289.6 c -1.904,0 -3.4912,1.1 -3.4912,2.4 0,1.4 1.5872,2.4 3.4912,2.4 H 43.68 c 1.9,0 3.49,-1 3.49,-2.4 0,-1.3 -1.59,-2.4 -3.49,-2.4 z"
           inkscape:connector-curvature="0" />
        <path
           style="opacity:0.03420003;fill-rule:evenodd;stroke-width:3pt"
           id="path1192-7"
           d="m 4.319,289.8 c -1.798,0 -3.174,1 -3.174,2.2 0,1.3 1.376,2.2 3.174,2.2 H 43.68 c 1.8,0 3.17,-0.9 3.17,-2.2 0,-1.2 -1.37,-2.2 -3.17,-2.2 z"
           inkscape:connector-curvature="0" />
        <path
           style="opacity:0.03420003;fill-rule:evenodd;stroke-width:3pt"
           id="path1191-4"
           d="m 4.319,290 c -1.587,0 -2.856,0.9 -2.856,2 0,1.1 1.269,2 2.856,2 H 43.68 c 1.58,0 2.85,-0.9 2.85,-2 0,-1.1 -1.27,-2 -2.85,-2 z"
           inkscape:connector-curvature="0" />
        <path
           style="opacity:0.03420003;fill-rule:evenodd;stroke-width:3pt"
           id="path1190-4"
           d="m 4.319,290.2 c -1.481,0 -2.645,0.8 -2.645,1.8 0,1 1.164,1.8 2.645,1.8 H 43.68 c 1.48,0 2.64,-0.8 2.64,-1.8 0,-1 -1.16,-1.8 -2.64,-1.8 z"
           inkscape:connector-curvature="0" />
        <path
           style="opacity:0.03420003;fill-rule:evenodd;stroke-width:3pt"
           id="path1189-3"
           d="m 4.319,290.4 c -1.269,0 -2.327,0.7 -2.327,1.6 0,0.9 1.058,1.6 2.327,1.6 H 43.68 c 1.27,0 2.32,-0.7 2.32,-1.6 0,-0.9 -1.05,-1.6 -2.32,-1.6 z"
           inkscape:connector-curvature="0" />
        <rect
           style="color:#000000;display:inline;overflow:visible;visibility:visible;fill:url(#linearGradient5451);fill-opacity:1;fill-rule:nonzero;stroke:#888a85;stroke-width:1;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none"
           id="rect1324-0"
           y="257.5"
           x="2.507"
           ry="0.77630001"
           rx="0.77630001"
           height="35.009998"
           width="42.990002" />
        <path
           style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:0.30290001;fill:#555753;fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:1;marker:none;enable-background:accumulate"
           id="path5613-7"
           d="m 6.471,259.1 c -1.366,0 -2.469,1.1 -2.469,2.4 0,0.8 0.403,1.6 1,2 -0.612,0.5 -1,1.2 -1,2 0,0.8 0.388,1.5 1,2 -0.612,0.5 -1,1.2 -1,2 0,0.8 0.388,1.5 1,2 -0.612,0.5 -1,1.2 -1,2 0,0.8 0.388,1.5 1,2 -0.612,0.5 -1,1.2 -1,2 0,0.8 0.388,1.5 1,2 -0.612,0.5 -1,1.2 -1,2 0,0.8 0.388,1.5 1,2 -0.612,0.5 -1,1.2 -1,2 0,0.6 0.19,1.1 0.5,1.5 -0.32,0.4 -0.5,0.9 -0.5,1.5 0,1.4 1.103,2.5 2.469,2.5 0.799,0 1.516,-0.4 1.969,-1 0.453,0.6 1.145,1 1.94,1 0.79,0 1.48,-0.4 1.93,-1 0.46,0.6 1.17,1 1.97,1 0.8,0 1.52,-0.4 1.97,-1 0.45,0.6 1.15,1 1.94,1 0.79,0 1.48,-0.4 1.93,-1 0.46,0.6 1.17,1 1.97,1 0.8,0 1.49,-0.4 1.94,-1 0.45,0.6 1.14,1 1.94,1 0.8,0 1.51,-0.4 1.97,-1 0.45,0.6 1.14,1 1.93,1 0.8,0 1.52,-0.4 1.97,-1 0.46,0.6 1.14,1 1.94,1 0.8,0 1.52,-0.4 1.97,-1 0.45,0.6 1.15,1 1.94,1 0.79,0 1.48,-0.4 1.94,-1 0.45,0.6 1.16,1 1.96,1 1.37,0 2.47,-1.1 2.47,-2.5 0,-0.6 -0.19,-1.1 -0.53,-1.5 0.3,-0.4 0.47,-1 0.47,-1.5 0,-0.8 -0.39,-1.5 -1,-2 0.61,-0.5 1,-1.2 1,-2 0,-0.8 -0.39,-1.5 -1,-2 0.61,-0.5 1,-1.2 1,-2 0,-0.8 -0.39,-1.5 -1,-2 0.61,-0.5 1,-1.2 1,-2 0,-0.8 -0.39,-1.5 -1,-2 0.61,-0.5 1,-1.2 1,-2 0,-0.8 -0.39,-1.5 -1,-2 0.61,-0.5 1,-1.2 1,-2 0,-0.8 -0.38,-1.5 -0.97,-2 0.62,-0.4 1.03,-1.1 1.03,-2 0,-1.3 -1.1,-2.4 -2.47,-2.4 -0.8,0 -1.51,0.3 -1.96,0.9 -0.46,-0.5 -1.15,-0.9 -1.94,-0.9 -0.79,0 -1.49,0.4 -1.94,0.9 -0.45,-0.6 -1.17,-0.9 -1.97,-0.9 -0.8,0 -1.48,0.3 -1.94,0.9 -0.45,-0.6 -1.17,-0.9 -1.97,-0.9 -0.79,0 -1.48,0.3 -1.93,0.9 -0.46,-0.6 -1.17,-0.9 -1.97,-0.9 -0.8,0 -1.49,0.3 -1.94,0.9 -0.45,-0.6 -1.14,-0.9 -1.94,-0.9 -0.8,0 -1.51,0.3 -1.97,0.9 -0.45,-0.6 -1.14,-0.9 -1.93,-0.9 -0.79,0 -1.49,0.4 -1.94,0.9 -0.45,-0.6 -1.17,-0.9 -1.97,-0.9 -0.8,0 -1.51,0.3 -1.97,0.9 -0.45,-0.5 -1.14,-0.9 -1.93,-0.9 -0.795,0 -1.487,0.4 -1.94,0.9 -0.453,-0.6 -1.17,-0.9 -1.969,-0.9 z M 8.44,263 c 0.453,0.6 1.145,1 1.94,1 0.79,0 1.48,-0.4 1.93,-1 0.46,0.6 1.17,1 1.97,1 0.8,0 1.52,-0.4 1.97,-1 0.45,0.6 1.15,1 1.94,1 0.79,0 1.48,-0.4 1.93,-1 0.46,0.6 1.17,1 1.97,1 0.8,0 1.49,-0.4 1.94,-1 0.45,0.6 1.14,1 1.94,1 0.8,0 1.51,-0.4 1.97,-1 0.45,0.6 1.14,1 1.93,1 0.8,0 1.52,-0.4 1.97,-1 0.46,0.6 1.14,1 1.94,1 0.8,0 1.52,-0.4 1.97,-1 0.45,0.6 1.15,1 1.94,1 0.79,0 1.48,-0.4 1.94,-1 0.12,0.2 0.27,0.3 0.43,0.5 -0.64,0.4 -1.06,1.1 -1.06,2 0,0.8 0.39,1.5 1,2 -0.61,0.5 -1,1.2 -1,2 0,0.8 0.39,1.5 1,2 -0.61,0.5 -1,1.2 -1,2 0,0.8 0.39,1.5 1,2 -0.61,0.5 -1,1.2 -1,2 0,0.8 0.39,1.5 1,2 -0.61,0.5 -1,1.2 -1,2 0,0.8 0.39,1.5 1,2 -0.61,0.5 -1,1.2 -1,2 0,0.4 0.1,0.8 0.25,1.1 -0.42,-0.4 -0.97,-0.6 -1.56,-0.6 -0.79,0 -1.49,0.4 -1.94,1 -0.45,-0.6 -1.17,-1 -1.97,-1 -0.8,0 -1.48,0.4 -1.94,1 -0.45,-0.6 -1.17,-1 -1.97,-1 -0.79,0 -1.48,0.4 -1.93,1 -0.46,-0.6 -1.17,-1 -1.97,-1 -0.8,0 -1.49,0.4 -1.94,1 -0.45,-0.6 -1.14,-1 -1.94,-1 -0.8,0 -1.51,0.4 -1.97,1 -0.45,-0.6 -1.14,-1 -1.93,-1 -0.79,0 -1.49,0.4 -1.94,1 -0.45,-0.6 -1.17,-1 -1.97,-1 -0.8,0 -1.51,0.4 -1.97,1 -0.45,-0.6 -1.14,-1 -1.93,-1 -0.7,0 -1.306,0.3 -1.753,0.8 0.234,-0.4 0.375,-0.8 0.375,-1.3 0,-0.8 -0.388,-1.5 -1,-2 0.612,-0.5 1,-1.2 1,-2 0,-0.8 -0.388,-1.5 -1,-2 0.612,-0.5 1,-1.2 1,-2 0,-0.8 -0.388,-1.5 -1,-2 0.612,-0.5 1,-1.2 1,-2 0,-0.8 -0.388,-1.5 -1,-2 0.612,-0.5 1,-1.2 1,-2 0,-0.8 -0.388,-1.5 -1,-2 0.612,-0.5 1,-1.2 1,-2 0,-0.8 -0.408,-1.5 -1.031,-2 0.178,-0.1 0.333,-0.3 0.469,-0.5 z"
           inkscape:connector-curvature="0" />
        <rect
           style="color:#000000;display:inline;overflow:visible;visibility:visible;fill:url(#linearGradient5453);fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:0.2;marker:none;enable-background:accumulate"
           id="rect5580-8"
           y="261"
           x="6.0019999"
           ry="0.77630001"
           rx="0.77630001"
           height="28"
           width="36" />
        <path
           style="fill:none;stroke:#204a87;stroke-width:0.2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none"
           id="path1332-6"
           d="m 11.16,279.7 c 0,0.2 0,0.5 0,0.5 0,0 0.1,-0.5 -0.16,-0.6 -0.44,-0.1 -0.71,0.5 -0.85,1 -0.233,0.6 -0.1,1.3 0.58,1.5 0.74,0.2 1.95,-0.7 1.95,-0.7 0,0 -0.14,0.1 -0.18,0.1 -0.1,0.2 -0.14,0.3 -0.1,0.4 0,0 0.13,0 0.18,0 0.21,0.1 0.48,0 0.53,-0.2 0.1,-0.2 0.18,-0.5 0.18,-0.5 0,0 -0.1,0.2 -0.13,0.2 -0.1,0.2 0.12,0.3 0.3,0.4 0.3,0.1 0.7,-0.4 0.75,-0.4 0,0 0.1,0.3 0.25,0.4 0.22,0 0.49,-0.2 0.58,-0.2 0,0 0.1,0.1 0.13,0.1 0.26,0.1 0.66,-0.1 0.7,-0.1 0,0 0.1,0.6 0.1,1.1"
           inkscape:connector-curvature="0" />
        <path
           style="fill:none;stroke:#204a87;stroke-width:0.2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none"
           id="path1334-8"
           d="m 17.16,280.1 c -0.19,0.5 -0.15,0.9 -0.29,1.4"
           inkscape:connector-curvature="0" />
        <path
           style="fill:none;stroke:#204a87;stroke-width:0.2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none"
           id="path1336-8"
           d="m 17.02,280.3 c 0,0.1 -0.1,0.2 -0.18,0.2 0.49,-0.1 1.19,-0.3 1.24,-0.2 0,0 0,0.1 0,0.1 -0.1,0.4 -0.49,0.8 -0.49,0.8 0,0.1 1.32,-0.2 1.44,-0.1 0.1,0 0,0.2 0,0.3 -0.1,0.2 -0.18,0.3 -0.32,0.4 0.1,0 0.1,0 0.14,0"
           inkscape:connector-curvature="0" />
        <path
           style="fill:none;stroke:#204a87;stroke-width:0.2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none"
           id="path1338-4"
           d="m 19.73,280.7 c 0,0.1 0,0.1 -0.1,0.1 0.35,0.1 0.53,-0.1 0.88,-0.1 -0.1,0.3 -0.14,0.5 -0.27,0.8 0.35,-0.4 0.93,-0.8 1.02,-0.8 0.1,0 0,0.8 0,0.8 0.17,0.1 1.59,-1.1 1.73,-1 0.1,0 -0.33,1 0.15,1.2 0.39,0.1 0.93,-0.5 0.93,-0.5 0,0 -0.18,0.2 -0.27,0.3 0,0 0.1,0 0.13,0 0.35,0 0.53,-0.2 0.88,-0.2 0,0.1 -0.1,0.1 -0.1,0.2 0.1,0 0.1,0 0.17,0.1 0.31,0.1 0.88,-0.2 0.88,-0.2 0,0 -0.1,0.3 0,0.3 0.61,0.2 1.01,-0.1 1.63,-0.2"
           inkscape:connector-curvature="0" />
        <path
           style="fill:#3465a4;fill-rule:evenodd;stroke:#204a87;stroke-width:1;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
           id="path5582-3"
           d="m 34.38,281.1 c 0.1,2.2 0.76,4.3 1.77,6.3 l 0.8,-3.3 3.27,0.7 c -1.17,-0.7 -3.54,-2.9 -4.51,-4.5 z"
           inkscape:connector-curvature="0" />
        <path
           style="color:#000000;display:inline;overflow:visible;visibility:visible;fill:#f7f999;fill-opacity:1;fill-rule:nonzero;stroke:#fdcd12;stroke-width:48.8927002;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none"
           id="path1342-1"
           transform="matrix(0.0195,0,0,0.0196,73.96,310.8)"
           d="m -1933,-1441 c -21,19 -47,1 -73,11 -26,11 -32,42 -60,43 -28,0 -37,-30 -63,-39 -27,-8 -52,11 -74,-6 -22,-17 -9,-46 -23,-70 -15,-24 -47,-25 -52,-52 -6,-27 22,-41 26,-69 4,-27 -19,-48 -6,-73 13,-25 44,-17 65,-35 21,-19 16,-50 42,-61 26,-10 44,16 72,15 28,-1 45,-28 71,-19 27,8 24,40 47,57 21,17 51,8 66,31 15,24 -7,47 -1,74 5,27 35,39 31,67 -4,27 -36,31 -49,55 -13,25 2,53 -19,71 z"
           inkscape:connector-curvature="0" />
        <path
           style="color:#000000;display:inline;overflow:visible;visibility:visible;fill:#fdce15;fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:6.25;marker:none"
           id="path1346-4"
           transform="matrix(0.0858,0,0,0.0863,116.7,373.7)"
           d="m -941.1,-1093 a 29.98,29.07 0 1 1 -59.9,0 29.98,29.07 0 1 1 59.9,0 z"
           inkscape:connector-curvature="0" />
        <path
           style="color:#000000;display:inline;overflow:visible;visibility:visible;fill:none;stroke:#ffffff;stroke-width:0.55790001;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none"
           id="path1348-9"
           d="m 34.4,278 c -0.1,0 -0.18,-0.1 -0.28,-0.1 -0.26,-0.1 -0.54,-0.2 -0.83,-0.2 0,0 0,0 0,0 -1.16,0 -2.08,1.5 -1.42,2.5 0.1,0.1 0.14,0.1 0.22,0.3"
           inkscape:connector-curvature="0" />
        <rect
           style="color:#000000;display:inline;overflow:visible;visibility:visible;opacity:0.69709999;fill:none;stroke:url(#linearGradient5455);stroke-width:1;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;marker:none"
           id="rect2231-2"
           y="258.5"
           x="3.5020001"
           ry="0"
           rx="0"
           height="32.98"
           width="41" />
        <path
           style="fill:none;stroke:#888a85;stroke-width:2;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
           id="path2235-0"
           d="m 10.15,264 h 28.7"
           inkscape:connector-curvature="0" />
        <path
           style="fill:none;stroke:#888a85;stroke-width:1px;stroke-linecap:round;stroke-linejoin:miter;stroke-opacity:1"
           id="path2237-6"
           d="M 15.39,267.5 H 33.41"
           inkscape:connector-curvature="0" />
        <path
           style="fill:none;stroke:#888a85;stroke-width:1px;stroke-linecap:round;stroke-linejoin:miter;stroke-opacity:1"
           id="path2239-8"
           d="M 12.34,273.5 H 24.66"
           inkscape:connector-curvature="0" />
        <path
           style="fill:none;stroke:#888a85;stroke-width:1px;stroke-linecap:round;stroke-linejoin:miter;stroke-opacity:1"
           id="path2241-9"
           d="M 12.5,276.5 H 24.82"
           inkscape:connector-curvature="0" />
        <path
           style="fill:none;stroke:#888a85;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
           id="path5578-2"
           d="M 11,283.4 H 26"
           inkscape:connector-curvature="0" />
      </g>
      <g
         style="display:inline"
         id="layer4-6"
         transform="translate(0,-252)" />
    </g>
    <g
       id="g5570"
       transform="matrix(1.04775,0,0,1.04775,72.456729,50.778751)">
      <g
         id="g5548">
        <polygon
           id="polygon5546"
           points="100,50 50,100 0,50 50,0 "
           style="fill:#ffffff" />
      </g>
      <g
         id="g5552">
        <polygon
           id="polygon5550"
           points="0,50 35,15 35,50 "
           style="fill:#25aee4" />
      </g>
      <g
         id="g5556">
        <polygon
           id="polygon5554"
           points="0,50 35,85 35,50 "
           style="fill:#73c35c" />
      </g>
      <g
         id="g5560">
        <polygon
           id="polygon5558"
           points="46,34.4 46,4 50,0 65.2,15.2 "
           style="fill:#e75153" />
      </g>
      <g
         id="g5564">
        <polygon
           id="polygon5562"
           points="46,65.6 46,96 50,100 65.2,84.8 "
           style="fill:#fdc35c" />
      </g>
      <g
         id="g5568">
        <polygon
           id="polygon5566"
           points="100,50 73,23 46,50 73,77 "
           style="fill:#fa9426" />
      </g>
    </g>
    <path
       style="fill:none;stroke:#000000;stroke-width:2.62350011;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1;marker-end:url(#marker6606)"
       d="M -320.44953,87.449998 H 46.262975"
       id="path6323"
       inkscape:connector-curvature="0" />
    <path
       style="fill:none;stroke:#000000;stroke-width:2.61937499px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;marker-end:url(#marker6614)"
       d="M 203.42548,87.449998 H 570.13797"
       id="path6325"
       inkscape:connector-curvature="0" />
    <path
       style="fill:none;stroke:#000000;stroke-width:2.61937499px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;marker-end:url(#marker6622)"
       d="M 570.13797,113.64375 H 386.78172 V 192.225"
       id="path6335"
       inkscape:connector-curvature="0" />
    <path
       style="fill:none;stroke:#000000;stroke-width:2.61937499px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;marker-end:url(#marker6630)"
       d="M 360.58797,192.225 V 113.64375 H 203.42548"
       id="path6337"
       inkscape:connector-curvature="0" />
    <path
       style="fill:none;stroke:#000000;stroke-width:2.61937499px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;marker-end:url(#marker6638)"
       d="M 46.262975,113.64375 H -137.09328 V 192.225"
       id="path6339"
       inkscape:connector-curvature="0" />
    <path
       style="fill:none;stroke:#000000;stroke-width:2.61937499px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;marker-end:url(#marker6646)"
       d="m -163.28703,192.225 v -78.58125 h -157.1625"
       id="path6341"
       inkscape:connector-curvature="0" />
  <text x="-340" y="30" style="font-size:20">
    <tspan>1.</tspan>
    <tspan><?= escape(-340, __("Your computer request a webpage.")) ?></tspan>
  </text>
  <text x="120" y="30" style="font-size:20">
    <tspan>2.</tspan>
    <tspan><?= escape(120, __("Your mydatakeeper box request this webpage.")) ?></tspan>
  </text>
  <text x="420" y="180" style="font-size:20">
    <tspan>3.</tspan>
    <tspan><?= escape(420, __("The encrypted webpage\nis sent back.")) ?></tspan>
  </text>
  <text x="120" y="180" style="font-size:20">
    <tspan>4.</tspan>
    <tspan><?= escape(120, __("Your mydatakeeper box\ndecrypt the webpage\nand cleans it up.")) ?></tspan>
  </text>
  <text x="-120" y="180" style="font-size:20">
    <tspan>5.</tspan>
    <tspan><?= escape(-120, __("Your mydatakeeper box\nencrypt the webpage\nwith its certificate.")) ?></tspan>
  </text>
  <text x="-480" y="180" style="font-size:20">
    <tspan>6.</tspan>
    <tspan><?= escape(-480, __("Your computer decrypt the\ncleaned webpage since it trusts\nthe mydatakeeper certificate.")) ?></tspan>
  </text>
  </g>
</svg>
