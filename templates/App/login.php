<?php
    $this->assign('title', __("Connection"));
?>
<div class="container align-middle">
    <div class="row justify-content-center">
        <div class="col-12 col-md-9 col-xl-6">
            <h4 class="text-center px-5"><?= __("Welcome!") ?></h4>
            <div class="form px-5">
                <?= $this->Form->create(null, array('type' => 'password')) ?>
                    <fieldset class="form-group">
                        <?= $this->Form->password('password', [
                            'label' => false,
                            'placeholder' => __("Password"),
                            'class' => 'form-control text-center my-1']) ?>
                    </fieldset>
                    <?= $this->Form->button(__("Connection"), [
                        'class' => 'btn btn-primary btn-block text-center']); ?>
                    <div class="float-right" style="margin-top: -2.5rem"> <!-- TODO: add w-[0-5] & h-[0-5] -->
                        <div data-toggle="tooltip" class="btn btn-info position-absolute helptip" title="<?= __("The local interface allows you to modify your box settings. You will find the password at the back of your box") ?>">
                        </div>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>