<?php
    $this->assign('title', __("Activate"));

    $this->Form->setTemplates([
        'inputContainer' =>
            '<div class="input row {{spacing}} {{type}}{{required}}{{advanced}}{{dependencies}}">'.
                '{{content}}'.
            '</div>',
        'formGroup' =>
            '<div class="col-12" data-toggle="tooltip" title="{{description}}">'.
                '{{input}}'.
            '</div>',
        'checkboxFormGroup' =>
            '<div class="col">'.
                '{{label}}'.
            '</div>'.
            '{{input}}',
        'checkbox' =>
            '<div class="col">'.
                '<label class="switch">'.
                    '<input type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}/>'.
                    '<span class="btn-primary"></span>'.
                '</label>'.
            '</div>'.
            '<div class="w-100"></div>',
    ]);

    $config = $application->config();
?>
<div class="container align-middle">
    <div class="row justify-content-center">
        <div class="col-12 col-md-9 col-xl-6">
            <?php if ($waiting_status && $waiting_status !== $config['status']) { ?>
                <h4 class="text-center px-5"><?= __("Please wait...") ?></h4>
                <p class="p-5 text-center"><?= __("Your mydatakeeper box is activating. Reload this page after a few seconds if it doesn't refresh itself") ?></p>
                <meta http-equiv="refresh" content="3; url=<?= $this->Url->build(['?' => compact('waiting_status', 'timeout', 'redirect')]) ?>" />
            <?php } else { ?>
                <h4 class="text-center px-5"><?= __("Activate your mydatakeeper box") ?></h4>
                <div class="form px-5">
                    <?= $this->Form->create($config, ['idPrefix' => $application->id]) ?>
                        <fieldset class="form-group">
                            <?= $this->Form->control('account_number', [
                                "class" => "account-number form-control text-center",
                                "required" => true,
                            ] + $config->options('account_number')) ?>
                            <?php if ($advanced) { ?>
                                <?= $this->Form->control('associate', [
                                "templateVars" => ["spacing" => "mt-3 mx-2"],
                            ] + $config->options('associate')) ?>
                                <?= $this->Form->control('enable', [
                                "templateVars" => ["spacing" => "mx-2"],
                            ] + $config->options('enable')) ?>
                                <?= $this->Form->control('status', [
                                "templateVars" => ["spacing" => "mx-2"],
                            ] + $config->options('status')) ?>
                            <?php } ?>
                        </fieldset>
                        <?= $this->Form->button(__("Activate"), [
                            'class' => 'btn btn-primary btn-block text-center']); ?>
                        <div class="float-right" style="margin-top: -2.5rem"> <!-- TODO: add w-[0-5] & h-[0-5] -->
                            <div data-toggle="tooltip" class="btn btn-info position-absolute helptip" title="<?= __("Please enter your activation code received by e-mail after you passed your order. It is composed of four groups of five digits and letters separated by dashes.") ?>">
                            </div>
                        </div>
                    <?= $this->Form->end() ?>
                    <?php if ($advanced) { ?>
                        <?= $this->Html->link(__("Simplified parameters"),
                            [ '?' => [ 'advanced' => false, 'redirect' => $redirect ]],
                            [ 'class' => 'btn btn-light btn-block text-center my-2']); ?>
                    <?php } else { ?>
                        <?= $this->Html->link(__("Advanced parameters"),
                            [ '?' => [ 'advanced' => true, 'redirect' => $redirect ]],
                            [ 'class' => 'btn btn-light btn-block text-center my-2']); ?>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
