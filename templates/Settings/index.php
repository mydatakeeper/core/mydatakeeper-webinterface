<?php
$this->assign('title', __("Settings"));
?>
<div class="container-fluid px-3 pb-5">
    <div class="card-columns">
        <?php foreach ($applications as $application) {
            if ($application->hasConfig)
                echo $this->element('Settings/card', compact('application'));
        } ?>
    </div>
</div>
