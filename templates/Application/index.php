<?php
$this->assign('title', __("Applications"));
?>
<div class="d-flex flex-wrap justify-content-center">
    <?php foreach ($applications as $application) {
        if ($application->hasInterface) {
            echo $this->element('Application/card', compact('application'));
        }
    } ?>
</div>
