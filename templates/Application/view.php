<?php
$url = $this->Url->build([
    'action' => 'file',
    'id' => $interface->id,
    'name' => $interface->index,
]);
$csrfToken = $this->request->getCookie('csrfToken');
?>
<script type="text/javascript">
    var csrfToken = "<?= $csrfToken; ?>";
</script>
<div class="border-top" style="position: absolute; top: 70px; bottom: 96px; width: 100%">
    <iframe sandbox="allow-scripts" style="border: none; width: 100%; height: 100%" src="<?= $url ?>"></iframe>
</div>
