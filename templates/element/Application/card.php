<div class="card app m-2" >
    <a class="badge badge-light" href="<?= $application->interfaceLink() ?>">
        <?= $this->Html->image(
            $application->iconData('160x160') ?: $this->Url->image('mydatakeeper-logo.svg'),
            [
                'alt' => __("Application ''{0}' icon", $application->name()),
                'class' => 'w-100',
            ]
        ); ?>
        <div class="card-body text-center p-1">
            <h5 class="card-title"><?= $application->name() ?></h5>
        </div>
    </a>
</div>
