<div class="col-md-4">
    <h3 class="text-center"><?= __("How to install on Windows") ?></h3>
    <ul>
        <li><?= __("Double-click the P12 file") ?></li>
        <li><?= __("Select Store Location for Current User and click Next") ?></li>
        <li><?= __("Click Next") ?></li>
        <li><?= __("Leave the Password column blank and click Next") ?></li>
        <li><?= __("Select Place all certificates in the following store") ?></li>
        <li><?= __("Click Browse and select Trusted Root Certification Authorities") ?></li>
        <li><?= __("Click Next and then click Finish") ?></li>
        <li><?= __("Click Yes if prompted for confirmation") ?></li>
        <li><?= __("Done!") ?></li>
    </ul>
</div>
<div class="col-md-4">
    <h3 class="text-center"><?= __("How to install on browsers") ?></h3>
    <ul>
        <li><?= __("Edge and IE use the Windows CA store. So installing our CA in the system is enough.") ?></li>
        <li><?= __("Chrome on Windows uses the Windows CA store. So installing our CA in the system is enough.") ?></li>
        <li><?= __("Firefox on Windows has its own CA store and needs to be installed with Firefox-specific instructions that can be found  <a href=\"https://wiki.mozilla.org/MozillaRootCertificate#Mozilla_Firefox\">HERE</a> .") ?></li>
    </ul>
</div>
<div class="col-md-4">
    <h3 class="text-center"><?= __("How to install on Windows (Automated)") ?></h3>
    <ul>
        <li><?= __(" >>> certutil.exe -importpfx Root mitmproxy-ca-cert.p12 ") ?></li>
        <li><?= __(" To know more click <a href=\"https://technet.microsoft.com/en-us/library/cc732443.aspx\">HERE</a> ") ?></li>
    </ul>
</div>
