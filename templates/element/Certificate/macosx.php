<div class="col-md-4">
    <h3 class="text-center"><?= __("How to install on macOS") ?></h3>
    <ul class="left">
        <li><?= __("Double-click the PEM file") ?></li>
        <li><?= __("The \"Keychain Access\" applications opens") ?></li>
        <li><?= __("Find the new certificate \"mitmproxy\" in the list") ?></li>
        <li><?= __("Double-click the \"mitmproxy\" entry") ?></li>
        <li><?= __("A dialog window openes up") ?></li>
        <li><?= __("Change \"Secure Socket Layer (SSL)\" to \"Always Trust\"") ?></li>
        <li><?= __("Close the dialog window (and enter your password if prompted)") ?></li>
        <li><?= __("Done!") ?></li>
    </ul>
</div>
<div class="col-md-4">
    <h3 class="text-center"><?= __("How to install on browsers") ?></h3>
    <ul>
        <li><?= __("Safari on macOS uses the macOS keychain. So installing our CA in the system is enough.") ?></li>
        <li><?= __("Chrome on macOS uses the macOS keychain. So installing our CA in the system is enough.") ?></li>
        <li><?= __("Firefox on macOS has its own CA store and needs to be installed with Firefox-specific instructions that can be found  <a href=\"https://wiki.mozilla.org/MozillaRootCertificate#Mozilla_Firefox\">HERE</a> .") ?></li>
    </ul>
</div>
