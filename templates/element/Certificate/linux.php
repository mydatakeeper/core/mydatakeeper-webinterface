<div class="col-md-4">
  <h3 class="text-center"><?= __("How to install on Chrome on Debian/Ubuntu") ?></h3>
  <ul>
    <li><?= __("Using Chrome, hit a page on your server via HTTPS and continue past the red warning page (assuming you haven't done this already)") ?></li>
    <li><?= __("Open up Chrome Settings > Show advanced settings > HTTPS/SSL > Manage Certificates") ?></li>
    <li><?= __("Click the Authorities tab and scroll down to find your certificate under the Organization Name that you gave to the certificate") ?></li>
    <li><?= __("Select it, click Edit (NOTE: in recent versions of Chrome, the button is now \"Advanced\" instead of \"Edit\"), check all the boxes and click OK. You may have to restart Chrome") ?></li>
  </ul>
</div>
<div class="col-md-4">
  <h3 class="text-center"><?= __("How to install on Chrome on Linux") ?></h3>
  <ul>
    <li><?= __("Open Developer Tools > Security, and select View certificate") ?></li>
    <li><?= __("Click the Details tab > Export. Choose PKCS #7, single certificate as the file format") ?></li>
    <li><?= __("Then follow my original instructions to get to the Manage Certificates page. Click the Authorities tab > Import and choose the file to which you exported the certificate, and make sure to choose PKCS  #7, single certificate as the file type") ?></li>
    <li><?= __("If prompted certification store, choose Trusted Root Certificate Authorities") ?></li>
    <li><?= __("Check all boxes and click OK. Restart Chrome") ?></li>
</ul>
</div>
<div class="col-md-4">
  <h3 class="text-center"><?= __("How to install on Ubuntu (Manually)") ?></h3>
  <ul>
    <li><?= __("Create a directory for extra CA certificates in /usr/share/ca-certificates: <div class=\"text-muted\">$ sudo mkdir /usr/share/ca-certificates/extra<div>") ?></li>
    <li><?= __("Copy the CA mitmproxy.crt file to this directory: <div class=\"text-muted\">$ sudo cp mitmproxy.crt /usr/share/ca-certificates/extra/mitmproxy.crt<div>") ?></li>
    <li><?= __("Let Ubuntu add the mitmproxy.crt file's path relative to /usr/share/ca-certificates to /etc/ca-certificates.conf: <div class=\"text-muted\">$ sudo dpkg-reconfigure ca-certificates</div>") ?></li>
    <li><?= __("In case of a .pem file on Ubuntu, it must first be converted to a .crt file: <div class=\"text-muted\">$ openssl x509 -in foo.pem -inform PEM -out foo.crt</div>") ?></li>
  </ul>
</div>
