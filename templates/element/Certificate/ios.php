<div class="col-md-4">
    <h3 class="text-center"><?= __("How to install on iOS") ?></h3>
    <ul>
        <li><?= __("After certificate installation, open Settings") ?></li>
        <li><?= __("Navigate to General and then About") ?></li>
        <li><?= __("Select Certificate Trust Settings") ?></li>
        <li><?= __("Each root that has been installed via a profile will be listed below the heading Enable Full Trust For Root Certificates. Toggle mitmproxy on") ?></li>
        <li><?= __("Done!") ?></li>
    </ul>
</div>
