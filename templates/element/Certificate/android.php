<div class="col-md-4">
    <h3 class="text-center"><?= __("How to install on Android") ?></h3>
    <ul>
        <li><?= __("Open your device's Settings app") ?></li>
        <li><?= __("Under \"Credential storage,\" tap Install from storage") ?></li>
        <li><?= __("Under \"Open from,\" tap where you saved the certificate") ?></li>
        <li><?= __("Tap the file") ?></li>
        <li><?= __("If prompted, enter the key store password and tap OK") ?></li>
        <li><?= __("Type a name for the certificate") ?></li>
        <li><?= __("Pick VPN and apps") ?></li>
        <li><?= __("Tap OK") ?></li>
        <li><?= __("Done!") ?></li>
    </ul>
</div>
