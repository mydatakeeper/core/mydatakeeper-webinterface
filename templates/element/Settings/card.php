<?php
    use Cake\I18n\I18n;

    $this->Form->setTemplates([
        'inputContainer' =>
            '<div class="input row {{type}}{{required}}{{advanced}}{{dependencies}}">'.
                '{{content}}'.
            '</div>',
        'formGroup' =>
            '<div class="col-12 col-sm">'.
                '{{label}}'.
            '</div>'.
            '<div class="col d-flex" data-toggle="tooltip" title="{{description}}">'.
                '{{input}}'.
            '</div>'.
            '<div class="w-100"></div>',
        'checkbox' =>
            '<label class="switch switch-primary">'.
                '<input type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}/>'.
                '<span class="slider slider-round"></span>'.
            '</label>',
        'checkboxFormGroup' => null,
        'dateWidget' => '{{year}}-{{month}}-{{day}}  {{hour}}:{{minute}}:{{second}}',
    ]);
    $query = $this->request->getQuery();

    // Has an advanced field
    $hasAdvanced = array_reduce($application->fields, function($acc, $field) {
        return $acc || (isset($field['advanced']) ? !!$field['advanced'] : false);
    }, false);
    // The original advanced id array
    $advanced = isset($query['advanced']) && is_array($query['advanced']) ? $query['advanced'] : [];
    // Whether this application should be displayed as advanced
    $isAdvanced = in_array($application->id, $advanced);
    // The new advanced array minus this application
    $advanced = array_values(
        array_filter($advanced, function($id) use ($application) {
            return $application->id !== $id;
        })
    );

    $config = $application->config();
?>
<div id="<?= $application->id ?>" class="card">
    <div class="card-header">
        <?php if (isset($application->icons)) :?>
        <?= $this->Html->image(
            $application->iconData('40x40') ?: $this->Url->image('mydatakeeper-logo.svg'),
            [
                'alt' => __("Application ''{0}' icon", $application->name()),
                'class' => 'card-img-top',
            ]
        ); ?>
        <?php endif ?>
        <h5 class="align-middle m-2"><?= $application->name() ?></h5>
    </div>
    <div class="card-body">
        <div class="card-error"><?= $this->Flash->render($application->id) ?></div>
        <p class="card-text"><?= $application->description() ?></p>
        <?= $this->Form->create($config, [
            'idPrefix' => $application->id,
            'class' => ($isAdvanced ? 'advanced' : null),
        ]); ?>
            <fieldset class="form-group container border rounded p-3">
            <?= $this->Form->hidden('id') ?>
            <?php foreach ($config->getVisible() as $property) { ?>
                <?= $this->Form->control($property, $config->options($property)) ?>
            <?php } ?>
            </fieldset>
            <div class="inputs">
                <div class="d-inline-block">
                    <?= $this->Form->button(__("Save"), [
                        'class' => 'btn btn-primary text-center'
                    ]); ?>
                </div>
                <?php if ($hasAdvanced) { ?>
                    <div class="float-right border rounded">
                        <?php
                            $url = [
                                'controller' => 'Settings',
                                'action' => 'index',
                                'locale' => I18n::getLocale(),
                            ];
                            if ($isAdvanced) {
                                $text = __("Simplified parameters");
                                if (!empty($advanced))
                                    $url['?'] = ['advanced' => $advanced];
                            } else {
                                $text = __("Advanced parameters");
                                $advanced[] = $application->id;
                                $url['?'] = ['advanced' => $advanced];
                            }
                            echo $this->Html->link($text, $url, [
                                'class' => 'btn btn-light text-center',
                            ]);
                        ?>
                    </div>
                <?php } ?>
            </div>
        <?= $this->Form->end() ?>
    </div>
    <div class="card-footer">
        <?php if (false && isset($application->tags)) : // Disabeld for now?>
        <div class="text-left">
            <small class="text-muted">
                <?= __("Tags:") ?>
                <?= implode(', ', array_map(function ($tag) {
                    return $this->Html->link(
                        $tag,
                        "https://store.mydatakeeper.fr/tags/$tag"
                    );
                }, $application->tags)); ?>
            </small>
        </div>
        <?php endif ?>
        <?php if (false && isset($application->categories)) : // Disabled for now?>
        <div class="text-left">
            <small class="text-muted">
                <?= __("Categories:") ?>
                <?= implode(', ', array_map(function ($category) {
                    return $this->Html->link(
                        $category,
                        "https://store.mydatakeeper.fr/categories/$category"
                    );
                }, $application->categories)); ?>
            </small>
        </div>
        <?php endif ?>
        <div class="text-right">
            <?php if (isset($application->contact)) :?>
            <small class="text-muted">
                <?= $this->Html->link(
                    __("Contact"),
                    'mailto:' . $application->contact
                ); ?>
            </small>
            <?php endif ?>
            <?php if (isset($application->website)) :?>
            <small class="text-muted">
                <?= $this->Html->link(
                    __("Website"),
                    $application->website
                ); ?>
            </small>
            <?php endif ?>
            <small class="text-muted">
                <?= __("Version") ?> <?= $application->version ?>
            </small>
        </div>
    </div>
</div>
