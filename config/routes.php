<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

use App\Middleware\LocaleSelectorMiddleware;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

$locales = array_keys(Configure::read('App.availableLocales'));
$builder = function (RouteBuilder $routes) use ($locales) {
    // Register scoped middleware for in scopes.
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true
    ]));

    // Add internationalization middleware.
    $localeSelector = new LocaleSelectorMiddleware($locales);
    $routes->registerMiddleware('locale', $localeSelector);

    /**
     * Apply a middleware to the current route scope.
     * Requires middleware to be registered via `Application::routes()` with `registerMiddleware()`
     */
    $routes->applyMiddleware('csrf');
    $routes->applyMiddleware('locale');

    /**
     * Connect all routes.
     */

    /* Base Controller */
    $routes->connect('/', [
        'controller' => 'App',
        'action' => 'index',
    ], [
        'persist' => ['locale'],
    ]);
    $routes->connect('/activate', [
        'controller' => 'App',
        'action' => 'activate',
    ], [
        'persist' => ['locale'],
    ]);
    $routes->connect('/login', [
        'controller' => 'App',
        'action' => 'login',
    ], [
        'persist' => ['locale'],
    ]);

    /* Application Controller */
    $routes->connect('/app', [
        'controller' => 'Application',
        'action' => 'index',
    ], [
        'persist' => ['locale'],
    ]);
    $routes->connect('/app/:id', [
        'controller' => 'Application',
        'action' => 'view',
    ], [
        'persist' => ['locale'],
    ])->setPass(['id']);
    $routes->connect('/app/:id/file/:name', [
        'controller' => 'Application',
        'action' => 'file',
    ], [
        'persist' => ['locale'],
    ])->setPass(['id', 'name']);
    $routes->connect('/app/:id/method', [
        'controller' => 'Application',
        'action' => 'method',
    ], [
        'persist' => ['locale'],
    ])->setPass(['id']);

    /* Settings Controller */
    $routes->connect('/settings', [
        'controller' => 'Settings',
        'action' => 'index',
    ], [
        'persist' => ['locale'],
    ]);

    /* Certificate Controller */
    $routes->connect('/certificate/*', [
        'controller' => 'Certificate',
        'action' => 'index',
    ], [
        'persist' => ['locale'],
    ]);
    $routes->connect('/certificate/download/*', [
        'controller' => 'Certificate',
        'action' => 'download',
    ], [
        'persist' => ['locale'],
    ]);

    /* License Controller */
    $routes->connect('/license', [
        'controller' => 'License',
        'action' => 'index',
    ], [
        'persist' => ['locale'],
    ]);
    $routes->connect('/license/:package', [
        'controller' => 'License',
        'action' => 'package',
    ], [
        'persist' => ['locale'],
    ])->setPass(['package']);
    $routes->connect('/license/:package/:license', [
        'controller' => 'License',
        'action' => 'license',
    ], [
        'persist' => ['locale'],
    ])->setPass(['package', 'license']);
    $routes->connect('/license/:package/custom/:id', [
        'controller' => 'License',
        'action' => 'custom',
    ], [
        'persist' => ['locale'],
    ])->setPass(['package', 'id']);

    /* Page Controller */
    $routes->connect('/thanks', [
        'controller' => 'Page',
        'action' => 'display', 'thanks',
    ], [
        'persist' => ['locale'],
    ]);
    $routes->connect('/gen-img/*', [
        'controller' => 'Page',
        'action' => 'image'
    ], [
        'persist' => ['locale'],
    ]);
    $routes->connect('/gen-js/*', [
        'controller' => 'Page',
        'action' => 'javascript'
    ], [
        'persist' => ['locale'],
    ]);

    /* Pretty error handler */
    $routes->connect('/*', [], [
        'routeClass' => 'LocaleSelectorFailingRoute',
        'selector' => $localeSelector,
    ]);
};

// Set routes per locale
$defaultLocale = Configure::read('App.defaultLocale');
foreach ($locales as $locale) {
    if ($locale !== $defaultLocale)
        Router::scope('/' . $locale, ['locale' => $locale], $builder);
}

// Set default locale routes
Router::scope('/', ['locale' => $defaultLocale], $builder);

// Set API routes
Router::scope('/api', function ($routes) {
    $routes->connect('/:action', ['controller' => 'Api']);
});
