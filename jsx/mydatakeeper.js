/*!
 * --------------------------------------------------------------------------
 * Mydatakeeper JS
 * Copyright 2020 Mydatakeeper S.A.S.
 * Licensed under GPL3
 * --------------------------------------------------------------------------
 */

$(function () {
    // Use template literals to generate string
    // First parameter is a format
    // The other parameters are accessible int the format
    // as '${_0}', '${_1}', ... '${_n}' variables
    function printf() {
        if (arguments.length === 0) return;
        format=arguments[0];
        for (i=1; i < arguments.length; ++i)
            eval('_'+(i-1)+'="'+arguments[i]+'"');
        return eval('`'+format+'`');
    }

    // Enable tooltips
    $('[data-toggle="tooltip"]').tooltip();

    // Enable Cleave formatting for account number
    $('.account-number').map(function (idx, obj) {
        new Cleave(obj, {
            blocks: [5, 5, 5, 5],
            delimiter: '-',
            uppercase: true,
        });
    });

    // Current locale prefix
    locale_prefix = lang.locale === lang.default_locale ? '' : '/'+lang.locale;

    // Display flash message about HTTPS certificate install when needed
    if (window.location.hostname === "mydatakeeper.local") {
        test_url = window.location.protocol + '//local.mydatakeeper.fr/api/test-connection';
        base_url = window.location.protocol + '//local.mydatakeeper.fr' + window.location.pathname + window.location.search;
        query = {
            url: test_url,
            success: function(result, status, xhr) {
                window.location = base_url;
            },
        }
        $.ajax(query);
        setInterval(function() {
            $.ajax(query);
        }, 5000);
    } else if (window.location.protocol === "http:") {
        test_url = "https://" + window.location.hostname + '/api/test-connection';
        cert_url = "http://" + window.location.hostname + locale_prefix + '/certificate';
        safe_url = "https://" + window.location.hostname + window.location.pathname + window.location.search;

        display_flash = true;
        query = {
            url: test_url,
            error: function(xhr, status, error) {
                // Don't display certificate installation message on the certificate page
                if (window.location.href.startsWith(cert_url)) return;
                if (display_flash) {
                    $(".flash-message").append(
                        '<div class="alert alert-warning" onclick="$(this).hide();">' +
                            printf(lang.install_certificate, cert_url, cert_url + '#why-install-certificate') +
                        '</div>'
                    );
                    display_flash = false;
                }
            },
            success: function(result, status, xhr) {
                window.location = safe_url;
            },
        };
        $.ajax(query);
        setInterval(function() {
            $.ajax(query);
        }, 5000);
    }

    // Enable method calls forwarding from application iframe
    iframe = window.frames[0];
    origin = window.location.origin;
    pathname = window.location.pathname;
    if (pathname.startsWith(locale_prefix))
        pathname = pathname.substr(locale_prefix.length);

    if (pathname.startsWith('/app/')) {
        appname = pathname.substring(5);

        console.log("Registering message listener for application " + appname);
        $(window).on("message", function(e) {
            var event = e.originalEvent;

            // Verify who is sending the message
            var event_origin = event.origin;
            if (event_origin === "null") {
                if (event.source !== iframe) {
                    console.warn("Received message from untrusted iframe. Ignoring");
                    return;
                }
                event_origin = '*';
            } else if (event_origin !== origin) {
                console.warn("Received message from untrusted origin. Ignoring");
                return;
            }
            var src_pathname = event.srcElement.location.pathname;
            if (src_pathname.startsWith(locale_prefix))
                src_pathname = src_pathname.substr(locale_prefix.length);
            if (!src_pathname.startsWith('/app/')) {
                console.warn("Received message from unauthorized location. Ignoring");
                return;
            }
            var src_appname = src_pathname.substring(5);
            if (appname !== src_appname) {
                console.warn("Received message from unknown application. Ignoring");
                return;
            }

            // Verify the message itself
            var data = event.data;
            if (!data.id || !data.method || !data.args) {
                console.error("Malformed message. Ignoring");
                return;
            }

            // Generate closure for each method call
            var call_method = function (source, appname, data)
            {
                // Send ajax request
                var id = data.id;
                var target_url = window.location.origin + "/app/" + appname + "/method";
                $.ajax({
                    url: target_url,
                    headers: {
                        'X-CSRF-Token': csrfToken,
                    },
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: JSON.stringify({
                        method: data.method,
                        args: data.args,
                    }),
                    error: function(xhr, status, error) {
                        source.postMessage({
                            id: id,
                            status: status,
                            error: error,
                        }, event_origin);
                    },
                    success: function(result, status, xhr) {
                        source.postMessage({
                            id: id,
                            status: status,
                            result: result,
                        }, event_origin);
                    },
                });
            }
            call_method(event.source, appname, data);
        });
    }
});
