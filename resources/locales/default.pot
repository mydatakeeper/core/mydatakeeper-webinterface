# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2020-01-30 15:33+0100\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: Controller/AppController.php:59
msgid "Please connect in order to reach this page"
msgstr ""

#: Controller/AppController.php:170
#: Controller/SettingsController.php:56
#: Model/Entity/Config.php:287
msgid "Internal error"
msgstr ""

#: Controller/AppController.php:191
msgid "Unable to activate your box at the moment. Please try again in a few minutes or contact our support."
msgstr ""

#: Controller/AppController.php:235
msgid "Wrong password"
msgstr ""

#: Controller/ApplicationController.php:56
#: Settings/index.php:2
msgid "Settings"
msgstr ""

#: Controller/CertificateController.php:131
msgid "Unknown platform"
msgstr ""

#: Middleware/LocaleSelectorMiddleware.php:46
msgid "Requested locale is not supported yet"
msgstr ""

#: Model/Entity/Config.php:220
msgid "Please select a value"
msgstr ""

#: Model/Entity/Config.php:298
msgid "Value of ''{0}' is too long"
msgstr ""

#: Model/Entity/Config.php:304
msgid "Value of ''{0}' is too short"
msgstr ""

#: Model/Entity/Config.php:310
msgid "Value of ''{0}' is invalid"
msgstr ""

#: Model/Entity/Config.php:320
msgid "Value of ''{0}' is not a valid string"
msgstr ""

#: Model/Entity/Config.php:329
msgid "Value of ''{0}' is not a valid UTF-8 string"
msgstr ""

#: Model/Entity/Config.php:338
msgid "Value of ''{0}' is not a valid extended UTF-8 string"
msgstr ""

#: Model/Entity/Config.php:345
msgid "Value of ''{0}' is not a valid ASCII string"
msgstr ""

#: Model/Entity/Config.php:351
msgid "Value of ''{0}' is not a valid IP address"
msgstr ""

#: Model/Entity/Config.php:357
msgid "Value of ''{0}' is not a valid IPv4 address"
msgstr ""

#: Model/Entity/Config.php:363
msgid "Value of ''{0}' is not a valid IPv6 address"
msgstr ""

#: Model/Entity/Config.php:373
msgid "Value of ''{0}' is not one of the options"
msgstr ""

#: Model/Entity/Config.php:389
msgid "At least one value of ''{0}' is not one of the options"
msgstr ""

#: Model/Entity/Config.php:396
msgid "Value of ''{0}' is not valid"
msgstr ""

#: App/activate.php:2
#: App/activate.php:58
msgid "Activate"
msgstr ""

#: App/activate.php:34
msgid "Please wait..."
msgstr ""

#: App/activate.php:35
msgid "Your mydatakeeper box is activating. Reload this page after a few seconds if it doesn't refresh itself"
msgstr ""

#: App/activate.php:38
msgid "Activate your mydatakeeper box"
msgstr ""

#: App/activate.php:61
msgid "Please enter your activation code received by e-mail after you passed your order. It is composed of four groups of five digits and letters separated by dashes."
msgstr ""

#: App/activate.php:66
#: element/Settings/card.php:85
msgid "Simplified parameters"
msgstr ""

#: App/activate.php:70
#: element/Settings/card.php:89
msgid "Advanced parameters"
msgstr ""

#: App/login.php:2
#: App/login.php:16
msgid "Connection"
msgstr ""

#: App/login.php:7
msgid "Welcome!"
msgstr ""

#: App/login.php:13
msgid "Password"
msgstr ""

#: App/login.php:19
msgid "The local interface allows you to modify your box settings. You will find the password at the back of your box"
msgstr ""

#: Application/index.php:2
msgid "Applications"
msgstr ""

#: Certificate/platform.php:3
msgid "Install your security certificate"
msgstr ""

#: Certificate/platform.php:25
#: Certificate/platform.php:109
msgid "Certificate installation instructions for {0}"
msgstr ""

#: Certificate/platform.php:32
msgid "Download your certificate ({0} version)"
msgstr ""

#: Certificate/platform.php:46
msgid "Why install this certificate?"
msgstr ""

#: Certificate/platform.php:52
msgid "When browsing the internet, there are (mainly) two kind of traffic : <a href=\"https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol\">HTTP</a> and <a href=\"https://en.wikipedia.org/wiki/HTTPS\">HTTPS</a>. Your {0} box intercept this traffic to verify what is going out <b>and</b> what is coming in. It then proceed to a sanitization of your traffic."
msgstr ""

#: Certificate/platform.php:55
msgid "When working on HTTP traffic, your {0} box can intercept and sanitize your traffic transparently (as detailed in the following diagram). You don't need to do anything."
msgstr ""

#: Certificate/platform.php:64
msgid "HTTP proxy"
msgstr ""

#: Certificate/platform.php:74
msgid "The \"S\" in HTTPS stands for \"secure\". You can check wether your connection is secured by looking at the address bar of the page you are currently visiting. A secured connection is encrypting all the traffic between you and the website (end-to-end encryption). To make sure you are receiving traffic from the real website, without anyone listening in the middle, your computer will check wether the website certificate is known and is valid."
msgstr ""

#: Certificate/platform.php:77
msgid "In this case, your {0} box cannot intercept yout traffic transparently. You need to tell your computer that you trust {0} to intercept and (potentially) alter your traffic. To do so, you need to <a href=\"{1}\">install your box certificate</a>. Without this certificate, your computer will show you an error since it does not trust the origin of the traffic."
msgstr ""

#: Certificate/platform.php:90
msgid "HTTPS proxy"
msgstr ""

#: Certificate/platform.php:101
msgid "Certificate installation instructions for every platform"
msgstr ""

#: Certificate/platform.php:105
msgid "All platforms"
msgstr ""

#: Certificate/platform.php:120
msgid "All the certificate formats available"
msgstr ""

#: Certificate/platform.php:124
msgid "All formats"
msgstr ""

#: Certificate/platform.php:128
msgid "Certificate in ''{0}' format"
msgstr ""

#: Error/error400.php:34
#: Error/error500.php:39
msgid "Error"
msgstr ""

#: Error/error400.php:37
msgid "The requested address {0} was not found on this server."
msgstr ""

#: Error/error500.php:42
msgid "An Internal Error Has Occurred."
msgstr ""

#: License/index.php:4
#: layout/application.php:87
#: layout/default.php:114
msgid "Licenses"
msgstr ""

#: License/index.php:9
msgid "The Mydatakeeper brand, as well as its logo displayed on this website are trademarks. Any complete or partial reproduction of these brands without the explicit consent of  Mydatakeeper S.A.S. is forbidden, with regards to the L.713-2 and following articles of the French Code de la propriété intellectuelle."
msgstr ""

#: License/index.php:14
msgid "At Mydatakeeper, we believe in the <a href=\"https://en.wikipedia.org/wiki/Open-source_model\">Open-source model</a>. We are using open-source software whose list (and licenses) is provided below. The code used in our boxes is also under open-source licenses and you can retrieve it from our <a href=\"https://gitlab.com/mydatakeeper\">GitLab</a>."
msgstr ""

#: License/index.php:20
msgid "Package"
msgstr ""

#: License/index.php:21
msgid "License(s) kind(s)"
msgstr ""

#: License/package.php:4
msgid "''{0}' license(s)"
msgstr ""

#: License/package.php:11
msgid "Package ''{0}' license(s)"
msgstr ""

#: License/package.php:15
msgid "License(s):"
msgstr ""

#: License/package.php:26
msgid "Custom license(s) file(s):"
msgstr ""

#: Page/img/http-proxy.svg.php:2539
#: Page/img/https-proxy.svg.php:2955
msgid "Your computer request a webpage."
msgstr ""

#: Page/img/http-proxy.svg.php:2543
msgid "Your mydatakeeper box request this webpage.\nIf the request contains trackers or ads, it is blocked."
msgstr ""

#: Page/img/http-proxy.svg.php:2547
msgid "The webpage is sent to your mydatakeeper box.\nIt is cleaned up from all trackers and ads."
msgstr ""

#: Page/img/http-proxy.svg.php:2551
msgid "Your computer receives a clean webpage."
msgstr ""

#: Page/img/https-proxy.svg.php:2959
msgid "Your mydatakeeper box request this webpage."
msgstr ""

#: Page/img/https-proxy.svg.php:2963
msgid "The encrypted webpage\nis sent back."
msgstr ""

#: Page/img/https-proxy.svg.php:2967
msgid "Your mydatakeeper box\ndecrypt the webpage\nand cleans it up."
msgstr ""

#: Page/img/https-proxy.svg.php:2971
msgid "Your mydatakeeper box\nencrypt the webpage\nwith its certificate."
msgstr ""

#: Page/img/https-proxy.svg.php:2975
msgid "Your computer decrypt the\ncleaned webpage since it trusts\nthe mydatakeeper certificate."
msgstr ""

#: Page/js/lang.js.php:10
msgid "Please <a href=\"${_0}\">install the certificate</a> of your box. <a href=\"${_1}\">Why?</a>"
msgstr ""

#: Page/thanks.php:2
#: layout/application.php:88
#: layout/default.php:115
msgid "Thanks"
msgstr ""

#: Page/thanks.php:52
msgid "At the beginning, Mydatakeeper launched a crowdfunding campaign on <a href=\"https://www.ulule.com/mydatakeeper/\">Ulule</a> in order to raise enough money to boot its activity. No less than <b>{0}</b> people answered our call and helped us to try and make our dream come true. Here is a complete list of the people :"
msgstr ""

#: Page/thanks.php:58
msgid "BIGGEST SUPPORTERS:"
msgstr ""

#: Page/thanks.php:66
msgid "SUPPORTER:"
msgstr ""

#: Page/thanks.php:75
msgid "As well as {0} other anonymous supporters that funded our adventure in July 2018."
msgstr ""

#: Page/thanks.php:80
msgid "We are extremely thankful to these people as well as to all the others that support and believe in us daily. From the Mydatakeeper team : <b>a big thank you!</b>"
msgstr ""

#: element/Application/card.php:7
#: element/Settings/card.php:50
msgid "Application ''{0}' icon"
msgstr ""

#: element/Certificate/android.php:2
msgid "How to install on Android"
msgstr ""

#: element/Certificate/android.php:4
msgid "Open your device's Settings app"
msgstr ""

#: element/Certificate/android.php:5
msgid "Under \"Credential storage,\" tap Install from storage"
msgstr ""

#: element/Certificate/android.php:6
msgid "Under \"Open from,\" tap where you saved the certificate"
msgstr ""

#: element/Certificate/android.php:7
msgid "Tap the file"
msgstr ""

#: element/Certificate/android.php:8
msgid "If prompted, enter the key store password and tap OK"
msgstr ""

#: element/Certificate/android.php:9
msgid "Type a name for the certificate"
msgstr ""

#: element/Certificate/android.php:10
msgid "Pick VPN and apps"
msgstr ""

#: element/Certificate/android.php:11
msgid "Tap OK"
msgstr ""

#: element/Certificate/android.php:12
#: element/Certificate/ios.php:8
#: element/Certificate/ipados.php:8
#: element/Certificate/mac.php:11
#: element/Certificate/macosx.php:11
#: element/Certificate/win10.php:12
#: element/Certificate/win32.php:12
#: element/Certificate/win64.php:12
#: element/Certificate/win7.php:12
#: element/Certificate/win8.1.php:12
#: element/Certificate/win8.php:12
#: element/Certificate/windows.php:12
#: element/Certificate/winvista.php:12
msgid "Done!"
msgstr ""

#: element/Certificate/ios.php:2
#: element/Certificate/ipados.php:2
msgid "How to install on iOS"
msgstr ""

#: element/Certificate/ios.php:4
#: element/Certificate/ipados.php:4
msgid "After certificate installation, open Settings"
msgstr ""

#: element/Certificate/ios.php:5
#: element/Certificate/ipados.php:5
msgid "Navigate to General and then About"
msgstr ""

#: element/Certificate/ios.php:6
#: element/Certificate/ipados.php:6
msgid "Select Certificate Trust Settings"
msgstr ""

#: element/Certificate/ios.php:7
#: element/Certificate/ipados.php:7
msgid "Each root that has been installed via a profile will be listed below the heading Enable Full Trust For Root Certificates. Toggle mitmproxy on"
msgstr ""

#: element/Certificate/linux.php:2
#: element/Certificate/unknown.php:2
#: element/Certificate/xbox_360.php:2
#: element/Certificate/xbox_os.php:2
#: element/Certificate/xbox_os_10.php:2
#: element/Certificate/xbox_os_10_mobile_view.php:2
msgid "How to install on Chrome on Debian/Ubuntu"
msgstr ""

#: element/Certificate/linux.php:4
#: element/Certificate/unknown.php:4
#: element/Certificate/xbox_360.php:4
#: element/Certificate/xbox_os.php:4
#: element/Certificate/xbox_os_10.php:4
#: element/Certificate/xbox_os_10_mobile_view.php:4
msgid "Using Chrome, hit a page on your server via HTTPS and continue past the red warning page (assuming you haven't done this already)"
msgstr ""

#: element/Certificate/linux.php:5
#: element/Certificate/unknown.php:5
#: element/Certificate/xbox_360.php:5
#: element/Certificate/xbox_os.php:5
#: element/Certificate/xbox_os_10.php:5
#: element/Certificate/xbox_os_10_mobile_view.php:5
msgid "Open up Chrome Settings > Show advanced settings > HTTPS/SSL > Manage Certificates"
msgstr ""

#: element/Certificate/linux.php:6
#: element/Certificate/unknown.php:6
#: element/Certificate/xbox_360.php:6
#: element/Certificate/xbox_os.php:6
#: element/Certificate/xbox_os_10.php:6
#: element/Certificate/xbox_os_10_mobile_view.php:6
msgid "Click the Authorities tab and scroll down to find your certificate under the Organization Name that you gave to the certificate"
msgstr ""

#: element/Certificate/linux.php:7
#: element/Certificate/unknown.php:7
#: element/Certificate/xbox_360.php:7
#: element/Certificate/xbox_os.php:7
#: element/Certificate/xbox_os_10.php:7
#: element/Certificate/xbox_os_10_mobile_view.php:7
msgid "Select it, click Edit (NOTE: in recent versions of Chrome, the button is now \"Advanced\" instead of \"Edit\"), check all the boxes and click OK. You may have to restart Chrome"
msgstr ""

#: element/Certificate/linux.php:11
#: element/Certificate/unknown.php:11
#: element/Certificate/xbox_360.php:11
#: element/Certificate/xbox_os.php:11
#: element/Certificate/xbox_os_10.php:11
#: element/Certificate/xbox_os_10_mobile_view.php:11
msgid "How to install on Chrome on Linux"
msgstr ""

#: element/Certificate/linux.php:13
#: element/Certificate/unknown.php:13
#: element/Certificate/xbox_360.php:13
#: element/Certificate/xbox_os.php:13
#: element/Certificate/xbox_os_10.php:13
#: element/Certificate/xbox_os_10_mobile_view.php:13
msgid "Open Developer Tools > Security, and select View certificate"
msgstr ""

#: element/Certificate/linux.php:14
#: element/Certificate/unknown.php:14
#: element/Certificate/xbox_360.php:14
#: element/Certificate/xbox_os.php:14
#: element/Certificate/xbox_os_10.php:14
#: element/Certificate/xbox_os_10_mobile_view.php:14
msgid "Click the Details tab > Export. Choose PKCS #7, single certificate as the file format"
msgstr ""

#: element/Certificate/linux.php:15
#: element/Certificate/unknown.php:15
#: element/Certificate/xbox_360.php:15
#: element/Certificate/xbox_os.php:15
#: element/Certificate/xbox_os_10.php:15
#: element/Certificate/xbox_os_10_mobile_view.php:15
msgid "Then follow my original instructions to get to the Manage Certificates page. Click the Authorities tab > Import and choose the file to which you exported the certificate, and make sure to choose PKCS  #7, single certificate as the file type"
msgstr ""

#: element/Certificate/linux.php:16
#: element/Certificate/unknown.php:16
#: element/Certificate/xbox_360.php:16
#: element/Certificate/xbox_os.php:16
#: element/Certificate/xbox_os_10.php:16
#: element/Certificate/xbox_os_10_mobile_view.php:16
msgid "If prompted certification store, choose Trusted Root Certificate Authorities"
msgstr ""

#: element/Certificate/linux.php:17
#: element/Certificate/unknown.php:17
#: element/Certificate/xbox_360.php:17
#: element/Certificate/xbox_os.php:17
#: element/Certificate/xbox_os_10.php:17
#: element/Certificate/xbox_os_10_mobile_view.php:17
msgid "Check all boxes and click OK. Restart Chrome"
msgstr ""

#: element/Certificate/linux.php:21
#: element/Certificate/unknown.php:21
#: element/Certificate/xbox_360.php:21
#: element/Certificate/xbox_os.php:21
#: element/Certificate/xbox_os_10.php:21
#: element/Certificate/xbox_os_10_mobile_view.php:21
msgid "How to install on Ubuntu (Manually)"
msgstr ""

#: element/Certificate/linux.php:23
#: element/Certificate/unknown.php:23
#: element/Certificate/xbox_360.php:23
#: element/Certificate/xbox_os.php:23
#: element/Certificate/xbox_os_10.php:23
#: element/Certificate/xbox_os_10_mobile_view.php:23
msgid "Create a directory for extra CA certificates in /usr/share/ca-certificates: <div class=\"text-muted\">$ sudo mkdir /usr/share/ca-certificates/extra<div>"
msgstr ""

#: element/Certificate/linux.php:24
#: element/Certificate/unknown.php:24
#: element/Certificate/xbox_360.php:24
#: element/Certificate/xbox_os.php:24
#: element/Certificate/xbox_os_10.php:24
#: element/Certificate/xbox_os_10_mobile_view.php:24
msgid "Copy the CA mitmproxy.crt file to this directory: <div class=\"text-muted\">$ sudo cp mitmproxy.crt /usr/share/ca-certificates/extra/mitmproxy.crt<div>"
msgstr ""

#: element/Certificate/linux.php:25
#: element/Certificate/unknown.php:25
#: element/Certificate/xbox_360.php:25
#: element/Certificate/xbox_os.php:25
#: element/Certificate/xbox_os_10.php:25
#: element/Certificate/xbox_os_10_mobile_view.php:25
msgid "Let Ubuntu add the mitmproxy.crt file's path relative to /usr/share/ca-certificates to /etc/ca-certificates.conf: <div class=\"text-muted\">$ sudo dpkg-reconfigure ca-certificates</div>"
msgstr ""

#: element/Certificate/linux.php:26
#: element/Certificate/unknown.php:26
#: element/Certificate/xbox_360.php:26
#: element/Certificate/xbox_os.php:26
#: element/Certificate/xbox_os_10.php:26
#: element/Certificate/xbox_os_10_mobile_view.php:26
msgid "In case of a .pem file on Ubuntu, it must first be converted to a .crt file: <div class=\"text-muted\">$ openssl x509 -in foo.pem -inform PEM -out foo.crt</div>"
msgstr ""

#: element/Certificate/mac.php:2
#: element/Certificate/macosx.php:2
msgid "How to install on macOS"
msgstr ""

#: element/Certificate/mac.php:4
#: element/Certificate/macosx.php:4
msgid "Double-click the PEM file"
msgstr ""

#: element/Certificate/mac.php:5
#: element/Certificate/macosx.php:5
msgid "The \"Keychain Access\" applications opens"
msgstr ""

#: element/Certificate/mac.php:6
#: element/Certificate/macosx.php:6
msgid "Find the new certificate \"mitmproxy\" in the list"
msgstr ""

#: element/Certificate/mac.php:7
#: element/Certificate/macosx.php:7
msgid "Double-click the \"mitmproxy\" entry"
msgstr ""

#: element/Certificate/mac.php:8
#: element/Certificate/macosx.php:8
msgid "A dialog window openes up"
msgstr ""

#: element/Certificate/mac.php:9
#: element/Certificate/macosx.php:9
msgid "Change \"Secure Socket Layer (SSL)\" to \"Always Trust\""
msgstr ""

#: element/Certificate/mac.php:10
#: element/Certificate/macosx.php:10
msgid "Close the dialog window (and enter your password if prompted)"
msgstr ""

#: element/Certificate/mac.php:15
#: element/Certificate/macosx.php:15
#: element/Certificate/win10.php:16
#: element/Certificate/win32.php:16
#: element/Certificate/win64.php:16
#: element/Certificate/win7.php:16
#: element/Certificate/win8.1.php:16
#: element/Certificate/win8.php:16
#: element/Certificate/windows.php:16
#: element/Certificate/winvista.php:16
msgid "How to install on browsers"
msgstr ""

#: element/Certificate/mac.php:17
#: element/Certificate/macosx.php:17
msgid "Safari on macOS uses the macOS keychain. So installing our CA in the system is enough."
msgstr ""

#: element/Certificate/mac.php:18
#: element/Certificate/macosx.php:18
msgid "Chrome on macOS uses the macOS keychain. So installing our CA in the system is enough."
msgstr ""

#: element/Certificate/mac.php:19
#: element/Certificate/macosx.php:19
msgid "Firefox on macOS has its own CA store and needs to be installed with Firefox-specific instructions that can be found  <a href=\"https://wiki.mozilla.org/MozillaRootCertificate#Mozilla_Firefox\">HERE</a> ."
msgstr ""

#: element/Certificate/win10.php:2
#: element/Certificate/win32.php:2
#: element/Certificate/win64.php:2
#: element/Certificate/win7.php:2
#: element/Certificate/win8.1.php:2
#: element/Certificate/win8.php:2
#: element/Certificate/windows.php:2
#: element/Certificate/winvista.php:2
msgid "How to install on Windows"
msgstr ""

#: element/Certificate/win10.php:4
#: element/Certificate/win32.php:4
#: element/Certificate/win64.php:4
#: element/Certificate/win7.php:4
#: element/Certificate/win8.1.php:4
#: element/Certificate/win8.php:4
#: element/Certificate/windows.php:4
#: element/Certificate/winvista.php:4
msgid "Double-click the P12 file"
msgstr ""

#: element/Certificate/win10.php:5
#: element/Certificate/win32.php:5
#: element/Certificate/win64.php:5
#: element/Certificate/win7.php:5
#: element/Certificate/win8.1.php:5
#: element/Certificate/win8.php:5
#: element/Certificate/windows.php:5
#: element/Certificate/winvista.php:5
msgid "Select Store Location for Current User and click Next"
msgstr ""

#: element/Certificate/win10.php:6
#: element/Certificate/win32.php:6
#: element/Certificate/win64.php:6
#: element/Certificate/win7.php:6
#: element/Certificate/win8.1.php:6
#: element/Certificate/win8.php:6
#: element/Certificate/windows.php:6
#: element/Certificate/winvista.php:6
msgid "Click Next"
msgstr ""

#: element/Certificate/win10.php:7
#: element/Certificate/win32.php:7
#: element/Certificate/win64.php:7
#: element/Certificate/win7.php:7
#: element/Certificate/win8.1.php:7
#: element/Certificate/win8.php:7
#: element/Certificate/windows.php:7
#: element/Certificate/winvista.php:7
msgid "Leave the Password column blank and click Next"
msgstr ""

#: element/Certificate/win10.php:8
#: element/Certificate/win32.php:8
#: element/Certificate/win64.php:8
#: element/Certificate/win7.php:8
#: element/Certificate/win8.1.php:8
#: element/Certificate/win8.php:8
#: element/Certificate/windows.php:8
#: element/Certificate/winvista.php:8
msgid "Select Place all certificates in the following store"
msgstr ""

#: element/Certificate/win10.php:9
#: element/Certificate/win32.php:9
#: element/Certificate/win64.php:9
#: element/Certificate/win7.php:9
#: element/Certificate/win8.1.php:9
#: element/Certificate/win8.php:9
#: element/Certificate/windows.php:9
#: element/Certificate/winvista.php:9
msgid "Click Browse and select Trusted Root Certification Authorities"
msgstr ""

#: element/Certificate/win10.php:10
#: element/Certificate/win32.php:10
#: element/Certificate/win64.php:10
#: element/Certificate/win7.php:10
#: element/Certificate/win8.1.php:10
#: element/Certificate/win8.php:10
#: element/Certificate/windows.php:10
#: element/Certificate/winvista.php:10
msgid "Click Next and then click Finish"
msgstr ""

#: element/Certificate/win10.php:11
#: element/Certificate/win32.php:11
#: element/Certificate/win64.php:11
#: element/Certificate/win7.php:11
#: element/Certificate/win8.1.php:11
#: element/Certificate/win8.php:11
#: element/Certificate/windows.php:11
#: element/Certificate/winvista.php:11
msgid "Click Yes if prompted for confirmation"
msgstr ""

#: element/Certificate/win10.php:18
#: element/Certificate/win32.php:18
#: element/Certificate/win64.php:18
#: element/Certificate/win7.php:18
#: element/Certificate/win8.1.php:18
#: element/Certificate/win8.php:18
#: element/Certificate/windows.php:18
#: element/Certificate/winvista.php:18
msgid "Edge and IE use the Windows CA store. So installing our CA in the system is enough."
msgstr ""

#: element/Certificate/win10.php:19
#: element/Certificate/win32.php:19
#: element/Certificate/win64.php:19
#: element/Certificate/win7.php:19
#: element/Certificate/win8.1.php:19
#: element/Certificate/win8.php:19
#: element/Certificate/windows.php:19
#: element/Certificate/winvista.php:19
msgid "Chrome on Windows uses the Windows CA store. So installing our CA in the system is enough."
msgstr ""

#: element/Certificate/win10.php:20
#: element/Certificate/win32.php:20
#: element/Certificate/win64.php:20
#: element/Certificate/win7.php:20
#: element/Certificate/win8.1.php:20
#: element/Certificate/win8.php:20
#: element/Certificate/windows.php:20
#: element/Certificate/winvista.php:20
msgid "Firefox on Windows has its own CA store and needs to be installed with Firefox-specific instructions that can be found  <a href=\"https://wiki.mozilla.org/MozillaRootCertificate#Mozilla_Firefox\">HERE</a> ."
msgstr ""

#: element/Certificate/win10.php:24
#: element/Certificate/win32.php:24
#: element/Certificate/win64.php:24
#: element/Certificate/win7.php:24
#: element/Certificate/win8.1.php:24
#: element/Certificate/win8.php:24
#: element/Certificate/windows.php:24
#: element/Certificate/winvista.php:24
msgid "How to install on Windows (Automated)"
msgstr ""

#: element/Certificate/win10.php:26
#: element/Certificate/win32.php:26
#: element/Certificate/win64.php:26
#: element/Certificate/win7.php:26
#: element/Certificate/win8.1.php:26
#: element/Certificate/win8.php:26
#: element/Certificate/windows.php:26
#: element/Certificate/winvista.php:26
msgid " >>> certutil.exe -importpfx Root mitmproxy-ca-cert.p12 "
msgstr ""

#: element/Certificate/win10.php:27
#: element/Certificate/win32.php:27
#: element/Certificate/win64.php:27
#: element/Certificate/win7.php:27
#: element/Certificate/win8.1.php:27
#: element/Certificate/win8.php:27
#: element/Certificate/windows.php:27
#: element/Certificate/winvista.php:27
msgid " To know more click <a href=\"https://technet.microsoft.com/en-us/library/cc732443.aspx\">HERE</a> "
msgstr ""

#: element/Settings/card.php:72
msgid "Save"
msgstr ""

#: element/Settings/card.php:106
msgid "Tags:"
msgstr ""

#: element/Settings/card.php:119
msgid "Categories:"
msgstr ""

#: element/Settings/card.php:133
msgid "Contact"
msgstr ""

#: element/Settings/card.php:141
#: layout/application.php:89
#: layout/default.php:116
msgid "Website"
msgstr ""

#: element/Settings/card.php:147
msgid "Version"
msgstr ""

#: layout/application.php:18
#: layout/default.php:18
msgid "Safety first"
msgstr ""

#: layout/application.php:63
#: layout/default.php:83
msgid "Home"
msgstr ""

#: layout/application.php:83
#: layout/default.php:110
msgid "Copyright"
msgstr ""

#: layout/application.php:92
#: layout/default.php:119
msgid "Icons made by <a href='https://www.flaticon.com/authors/freepik' title='Freepik'>Freepik</a> from <a href='https://www.flaticon.com/' title='Flaticon'>www.flaticon.com</a>"
msgstr ""

#: layout/default.php:63
msgid "Install certificate"
msgstr ""

#: layout/default.php:69
msgid "Toggle Dropdown"
msgstr ""

#: layout/default.php:72
msgid "Why install a certificate ?"
msgstr ""

#: layout/default.php:73
msgid "See install instructions"
msgstr ""

#: layout/default.php:74
msgid "See other options"
msgstr ""

#: layout/default.php:75
msgid "Take me to the safety"
msgstr ""

